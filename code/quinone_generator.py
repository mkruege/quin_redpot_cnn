





def generate_exp1(funct_group):
    "Generates synthetic quinones for experiment with trained CNN model."
    templates = {
        #"1,2-BQ": "C1#5=C#6C(=O)C(=O)C#3=C1#4",
        "1,2-BQ": "O=c1c#6c#5c#4c#3c1=O", #ortho
        #"1,4-BQ": "C1#5=C#6C(=O)C#2=C#3C1=O",
        "1,4-BQ": "O=c1c#4c#5c(=O)c#2c1#3", #para
        #"2,3-NQ": "O=C1C#4=C2C#5=C#6C#7=C#8C2=C#1C1=O",
        "2,3-NQ": "O=C2C#4c1c#5c#6c#7c#8c1C#1C2=O", #ortho
        #"1,5-NQ": "C1#7=C#6C(=O)C2=C#4C#3=C#2C(=O)C2=C1#8",
        "1,5-NQ": "O=c1c#6c#7c#8c2c(=O)c#2c#3c#4c12", #multi
        #"1,7-NQ": "C1(=O)C#6=C#5C2=C#4C#3=C#2C(=O)C2=C1#8",
        "1,7-NQ": "O=c2c#6c#5c1c#4c#3c#2c(=O)c1c2#8", #multi
        "1,2-NQ": "C1#6=C#7C#8=C2C(=O)C(=O)C#3=C#4C2=C1#5", #ortho
        "1,4-NQ": "C1#6=C#7C#8=C2C(=O)C#2=C#3C(=O)C2=C1#5", #para
        "2,6-NQ": "C1(=O)C#7=C#8C2=C#1C(=O)C#3=C#4C2=C1#5", #multi
        "9,10-AQ": "C1#3=C#2C#1=C2C(=C1#4)C(=O)C3=C#5C#6=C#7C#8=C3C2=O", #para
        "1,10-AQ": "C1#7=C#6C#5=C2C(=C1#8)C#9=C3C(=O)C#2=C#3C#4=C3C2=O", #multi
        "1,4-AQ": "C1#6=C#7C#8=C2C#9=C3C(=O)C#2=C#3C(=O)C3=C#XC2=C1#5", #para
        "2,3-AQ": "C1#7=C#8C2=C#9C3=C#1C(=O)C(=O)C#4=C3C#X=C2C#5=C1#6", #ortho
        "2,9-AQ": "C1#6=C#7C#8=C2C(=C1#5)C#X=C3C#4=C#3C(=O)C#1=C3C2=O", #multi
        "1,2-AQ": "C1#6=C#7C#8=C2C#9=C3C(=C#XC2=C1#5)C#4=C#3C(=O)C3=O", #ortho
        "1,5-AQ": "O=C1C#2=C#3C#4=C2C#X=C3C(C#9=C12)=C#8C#7=C#6C3=O", #multi
        "2,6-AQ": "O=C1C#3=C#4C2=C#XC3=C#5C(=O)C#7=C#8C3=C#9C2=C1#1", #multi
        "1,7-AQ": "O=C1C#6=C#5C2=C#XC3=C#4C#3=C#2C(=O)C3=C#9C2=C1#8", #multi
        #1, 2 - Aceanthrenequinone
        "1,2-AcaQ": "O=c2c(=O)c4c1c#0c#9c#8c#7c1c#6c3c#5c#4c#3c2c34", #ortho
        #1,2-Acenaphthenequinone
        "1,2-AcnQ": "O=c1c(=O)c3c#8c#7c#6c2c#5c#4c#3c1c23", #ortho
        #1,4-Chrysenequinone
        "1,4-CQ": "O=c1c#2c#3c(=O)c4c1c#5c#6c3c2c#7c#8c#9c#0c2c#Ac#Bc34", #para
        # 5,6-Chrysenequinone
        #"5,6-CQ": "O=c3c(=O)c2c1ccccc1ccc2c4ccccc34", #ortho
        #1,4-Phenanthrenequinone
        "1,4-PhQ": "O=c1c#2c#3c(=O)c3c1c#5c#6c2c#7c#8c#9c#0c23", #para
        # 9,10-Phenanthrenequinone
        "9,10-PhQ": "O=c2c(=O)c1c#1c#2c#3c#4c1c3c#5c#6c#7c#8c23", #ortho
        #Benzo[a]pyrene-1,6-dione
        "Bp-1,6-Q": "O=c2c#2c#3c3c#4c#5c4c(=O)c1c#7c#8c#9c#0c1c5c#Ac#Bc2c3c45", #multi
        #Benzo[a]pyrene-4,5-dione
        "Bp-4,5-Q": "O=c2c(=O)c4c#6c1c#7c#8c#9c#0c1c5c#Ac#Bc3c#1c#2c#3c2c3c45", #ortho
        #Benzo[a]pyrene-6,12-dione
        "Bp-6,12-Q": "O=c2c#5c5c1c#4c#3c#2c#1c1c(=O)c4c#Ac#0c3c#9c#8c#7c2c3c45", #multi
        #7,12-Benz[a]anthraquinone
        "7,12-BaQ": "O=c2c1c#Ac#0c#9c#8c1c(=O)c4c2c#1c#2c3c#3c#4c#5c#6c34", #para
        #4,5-Pyrenequinone
        "4,5-PyQ": "O=c1c(=O)c4c#3c#2c#1c3c#0c#9c2c#8c#7c#6c1c2c34" #ortho

    }
    template_class = [0, 1, 0, 2, 2, 0, 1, 2, 1, 2, 1, 0, 2, 0, 2, 2, 2, 0, 0, 1, 1, 0, 2, 0, 2, 1, 0] #0: ortho, 1: para, 2: multi
    alloriginals = []
    allalternates = []
    allkeys = []
    for key in templates: #iterate over templates
        temp = templates[key]
        splits = temp.split("#")
        poslist = []
        molsplits = [splits[0]]
        for split in splits[1:]: #seperate position labels from SMILES strings
            poslist.append(split[0])
            molsplits.append(split[1:])
        originals = []
        alternates = []
        for i in range(len(poslist)): #make alternate quinone for each position
            new = [molsplits[0]]
            for j in range(i):
                new.append(molsplits[j+1])
            new.append(funct_group)
            for j2 in range(len(molsplits)-(i+1)):
                new.append(molsplits[j2+1])

            alternates.append("".join(new))
            originals.append("".join(molsplits))
        for i in range(len(originals)):
            [new_or, new_alt] = filter_valid_quinones(originals[i], alternates[i])
            try:
                if new_alt == 0:
                    pass
                else:
                    allkeys.append(key)
            except:
                allkeys.append(key)
            if type(new_or) == str and type(new_alt) == str:
                alloriginals.append(new_or)
                allalternates.append(new_alt)


    return [(alloriginals, allalternates), allkeys]



def filter_valid_quinones(quin1, quin2):
    from rdkit import Chem

    try:
        mol = Chem.MolFromSmiles(quin1)
        mol2 = Chem.MolFromSmiles(quin2)
        new_quin1 = Chem.MolToSmiles(mol)
        new_quin2 = Chem.MolToSmiles(mol2)


        return [new_quin1, new_quin2]

    except:
        return [0, 0]