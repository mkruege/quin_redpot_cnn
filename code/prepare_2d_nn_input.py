import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
import random
import altern_smiles
import prepare_2d_smiles_data
from sklearn.preprocessing import OneHotEncoder
from sklearn.utils import shuffle
import plot_results
global rdk
from sklearn.model_selection import KFold
import pickle
try:
    from rdkit import Chem
    rdk =1
except:
    rdk = 0


def prepare_img_nn_input(df_file, images, output_dim=2, outvals=["E", "Gsolv"]):


    df = pd.read_csv(df_file)  # read file

    # remove nans
    #df = df.dropna(axis=0, how="any")

    # define the labels and split them from the rest (features)
    labelsList = []
    for i in outvals:  # only use labels from argument
        labelsList.append(df[i])

    features = images

    # prepare arrays and transposed arrays
    labelst = np.array(labelsList)
    labels = np.transpose(labelst)
    X = np.array(features)

    # normalize labels
    scaler_y = MinMaxScaler()
    scaler_y.fit(labels)
    Y = scaler_y.transform(labels)
    # but not features (only 0 and 1)

    if output_dim == 3:
        X = X.reshape(X.shape[0], X.shape[1], X.shape[2], 1)

    # split training and test data
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)


    return [X_train, X_test, Y_train, Y_test, scaler_y]


def reshape_dimensions(data, specs, output_dim):
    "Turns one dimensional string into 2- or 3-dim tensor."
    new_data = np.zeros((specs[0], specs[1], specs[2]), dtype=np.bool)
    for linnum, line in enumerate(data):
        count = 0
        count1 = 0
        for valnum, val in enumerate(line):
            if count < specs[2]:
                new_data[linnum][count1][count] = val
                count = count + 1
            else:
                count = 1
                count1 = count1 + 1
                new_data[linnum][count1][0] = val


    if output_dim == 3:
        new_data = new_data.reshape(new_data.shape[0], new_data.shape[1], new_data.shape[2], 1)

    return new_data


def split_chars(data):
    "Splits chars in arrays of SMILES."
    try: #sometimes data must be reshaped
        data.shape[1]
        data = data.reshape(data.shape[0],)
    except:
        pass
    new_data = []
    for num, smiles in enumerate(data):
        new_dat = []
        for i in smiles:  # also split strings into single chars
            new_dat.append(i)
        new_data.append(new_dat)

    for num1, dat in enumerate(new_data):  # find chars that are only part of element names
        for num, char in enumerate(dat):
            if char == "r":
                dat[num - 1] = dat[num - 1] + char
                del dat[num]
    return new_data




def prepare_input_from_original_smiles(file, outvals, outvalnames = [], asfile=1, n_random_smiles=0, n_shifts=0, k_random_smiles=20, k_shifts=20, files=1, output_dim=2, extend_frame=10, rands_nums_histo=0, trainingfiles=[], merge_multi_out=0, comparefiles=[], crossval=0, rsmilfile=[], outfilename='inpt_data', make_cmpr=0, cmprfile='ramak_cmpr.csv', vertical_shift=0, nosplit=0, remove_exp_mols=0, remove_by_fgroup=[], spectestset=[], fulltest=0):
    "Obtain NN input data from smiles-csv. Supports randomSmiles and shifts method."


    # define specific string information: 1) number of unique chars 2) length of longest char+extension 3) position of empty char in uniques -> specs now obtained automatically
    # if "tabor" in file[0]:
    #     specs = [18, 84+extend_frame, 9]
    #
    # elif "pdf" in file[0] or "krist" in file[0]:
    #     specs = [28, 115+extend_frame, 8]
    #
    # elif "_er" in file[0]:
    #     specs = [22, 140+extend_frame, 8]
    #
    # elif "_ramak" in file[0]:
    #     specs = [16, 24+extend_frame, 5]
    #
    # try:
    #     specs[0]
    # except:
    #     specs = [30, 150+extend_frame, 9]





    data = prepare_2d_smiles_data.read_input_sheets(file, files)


    if merge_multi_out == 1:
        data_merged = prepare_2d_smiles_data.merge_multi_outputs(data, outvals)

    else:
        data_merged = prepare_2d_smiles_data.merge_data(data, outvals)

    #merge strings for randomSmiles method
    data_merged_smil = []
    for dat in data_merged:
        row = []
        for i in range(len(outvals)):
            row.append(dat[i])
        row.append("".join(dat[len(outvals):]))
        data_merged_smil.append(row)

    if merge_multi_out==0:
        df = pd.DataFrame(data_merged_smil,columns=outvals + ["smiles"])
    else:
        df = pd.DataFrame(data_merged_smil, columns=outvalnames + ["smiles"])
    # shuffle dataset
    df = shuffle(df)

    # remove nans
    df = df.dropna(axis=0, how="any")

    # remove quinones for quinone data experiment from training data
    if remove_exp_mols == 1:
        smiles_exp = ["C1=CC(=O)C=CC1=O", "C1=CC=C2C(=C1)C=CC(=O)C2=O", "C1=CC=C2C(=O)C=CC(=O)C2=C1",
                      "C1=CC=C2C(=C1)C3=CC=CC=C3C(=O)C2=O", "C1=CC=C2C(=O)C=CC(=O)C2=C1", "CC1=CC(=O)C2=CC=CC=C2C1=O", "O=C1c2ccccc2C(=O)c3ccccc13",
                      "C1=CC=C2C(=C1)C(=CC(=O)C2=O)O", "O=c2c1ccccc1c(=O)c4c2ccc3ccccc34", "O=c1c(=O)c3cccc2cccc1c23",
                      "CC1=CC2=C(C=C1)C(=O)C3=CC=CC=C3C2=O"]
        for exp_smil in smiles_exp:
            current = Chem.MolToSmiles(Chem.MolFromSmiles(exp_smil))
            df = df[df.smiles != current]

    #remove quinones based on functional groups
    #print(df.shape)
    for group in remove_by_fgroup:
        for smil in df.smiles:
            if Chem.MolFromSmiles(smil).HasSubstructMatch(Chem.MolFromSmarts(group)):
                df = df[df.smiles != smil]
    #print(df.shape)

    # define the labels and split them from the rest (features)
    features = df
    labelnames = outvals

    labelsList = []
    if merge_multi_out==0:
        for i in outvals:  # only use labels from argument
            labelsList.append(features[i])
        for i in labelnames:
            features = features.drop(i, axis=1)  # remove from features

    else:
        for i in outvalnames:
            labelsList.append(features[i])
        for i in outvalnames:
            features = features.drop(i, axis=1)  # remove from features


    # prepare arrays and transposed arrays
    labelst = np.array(labelsList)
    labels = np.transpose(labelst)
    feature_list = list(features.columns)
    features = np.array(features)


    Y = labels
    # but not features (only 0 and 1)
    X = features
    sets = []

    # if fulltest == 1:
    #     X = np.reshape(X, (X.shape[0], 1))
    #     Y = np.reshape(Y, (Y.shape[0], 1))
    #     filename = outfilename + "_test_set"
    #     outfile = open(filename, 'wb')
    #     pickle.dump([X, Y], outfile)
    #     outfile.close()



    if crossval == 0:
        if nosplit == 0:
            # split training and test data
            X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)
            sets.append([X_train, X_test, Y_train, Y_test])
        else:
            X_train = X
            Y_train = Y
            sets.append([X_train, [], Y_train, []])

    else:

        if len(spectestset) == 0:
            # extract test set and save to extra file
            X, Xtesttest, Y, Ytesttest = train_test_split(X, Y, test_size=50)

        else:
            Xtesttest = np.asarray(spectestset[0]["smiles"])
            Ytesttest = np.asarray(spectestset[0][outvals[0]])
            Xtesttest = np.reshape(Xtesttest, (Xtesttest.shape[0], 1))
            Ytesttest = np.reshape(Ytesttest, (Ytesttest.shape[0], 1))
            for num, i in enumerate(Ytesttest):
                for j in Y:
                    if np.isclose(i, j):
                        X = np.delete(X, num)
                        Y = np.delete(Y, num)
                        break
            X = np.reshape(X, (X.shape[0], 1))
            Y = np.reshape(Y, (Y.shape[0], 1))
        print(str(Ytesttest[0][0]) + ";" + str(Ytesttest[-1][0]))

        # if fulltest != 0:
        #     filename = outfilename + "_test_set"
        #     outfile = open(filename, 'wb')
        #     pickle.dump([Xtesttest, Ytesttest], outfile)
        #     outfile.close()


        kf = KFold(n_splits=crossval)
        kf.get_n_splits(X)
        KFold(n_splits=crossval, random_state=None, shuffle=False)
        for train_index, test_index in kf.split(X):
            X_train, X_test = X[train_index], X[test_index]
            Y_train, Y_test = Y[train_index], Y[test_index]
            sets.append([X_train, X_test, Y_train, Y_test])
        sets[-1][1] = np.concatenate([sets[-1][1], Xtesttest])
        sets[-1][3] = np.concatenate([sets[-1][3], Ytesttest])
    returns = []
    for setnum, set in enumerate(sets):
        [X_train, X_test, Y_train, Y_test] = set

        if setnum == len(sets) - 1:  # last set
            #save last test daza set for comparison
            filename = outfilename + "_last_val_set"
            outfile = open(filename, 'wb')
            pickle.dump([X_test, Y_test], outfile)
            outfile.close()

        for cfile in comparefiles: #add comparison_file only to val_data
            dfc = pd.read_csv(cfile)
            dfcsmil = np.asarray(dfc["smiles"])
            dfcsmil = dfcsmil.reshape((len(dfcsmil), 1))
            X_test = np.append(X_test, dfcsmil, axis=0)
            if merge_multi_out == 0:
                dfcout = np.asarray(dfc[outvals[0]])
            else:
                try:
                    dfcout = np.asarray(dfc[outvals[0][0]])
                except:
                    dfcout = np.asarray(dfc[outvals[0][1]])
            dfcout = dfcout.reshape((len(dfcout), 1))
            if len(outvals) > 1:
                for num, out in enumerate(outvals[1:]):
                    if merge_multi_out == 0:
                        dfcout = np.append(dfcout, np.asarray(dfc[out]), axis=1)
                    else:
                        try:
                            dfcout = np.append(dfcout, np.asarray(dfc[out[0]]).reshape(len(dfc[out[0]]), 1), axis=1)
                        except:
                            dfcout = np.append(dfcout, np.asarray(dfc[out[1]]).reshape(len(dfc[out[1]]), 1), axis=1)


            Y_test = np.append(Y_test, dfcout, axis=0)

        for tfile in trainingfiles: #add trainingfile only to training data
            dft = pd.read_csv(tfile)
            dftsmil = np.asarray(dft["smiles"])
            dftsmil = dftsmil.reshape((len(dftsmil), 1))
            X_train = np.append(X_train, dftsmil, axis=0)
            if merge_multi_out == 0:
                dftout = np.asarray(dft[outvals[0]])
            else:
                try:
                    dftout = np.asarray(dft[outvals[0][0]])
                except:
                    dftout = np.asarray(dft[outvals[0][1]])

            dftout = dftout.reshape((len(dftout), 1))
            if len(outvals) > 1:
                for num, out in enumerate(outvals[1:]):
                    if merge_multi_out == 0:
                        dftout = np.append(dftout, np.asarray(dft[out]), axis=1)
                    else:
                        try:
                            dftout = np.append(dftout, np.asarray(dft[out[0]]).reshape(len(dft[out[0]]), 1), axis=1)
                        except:
                            dftout = np.append(dftout, np.asarray(dft[out[1]]).reshape(len(dft[out[1]]), 1), axis=1)




            Y_train = np.append(Y_train, dftout, axis=0)



        #RANDOM SMILES PARTIAL EXTENSION
        # if 1:
        #     randnums = []
        #     extend_X = []
        #     extend_Y = []
        #     for num, smil in enumerate(X_train):
        #         if rdk == 1:  # package rdkit loaded successfully -> obtain random smiles via package
        #             k_rs = altern_smiles.get_x_random_smiles_alternations(smil[0],
        #                                                                   k_random_smiles)  # obtain k_random_smiles for each molecule in training data
        #         else:
        #             k_rs = altern_smiles.get_x_random_smiles_alternations_from_csv(smil[0],
        #                                                                            k_random_smiles, rsmilfile[
        #                                                                                0])  # obtain k_random_smiles for each molecule in training data
        #         if (Y_train[num][0] < 0.1) and (Y_train[num][0] > -0.35): #only perform augmentation for certain E(0) range
        #             for i in k_rs:
        #                 extend_Y.append(Y_train[num])
        #                 extend_X.append(i)
        #             randnums.append(len(k_rs))
        #     if rands_nums_histo == 1:
        #         plot_results.nums_histo(randnums, 0, k_random_smiles, k_shifts, extend_frame)
        #
        #     try:
        #         for num in random.sample(range(len(extend_X)),
        #                                  n_random_smiles):  # get specified number of random elements
        #             X_train = np.append(X_train, extend_X[num])  # attach them to both X-...
        #             Y_train = np.append(Y_train, extend_Y[num])  # and Y-train
        #     except:
        #         print("WARNING: The number of alternations by random smiles is only " + str(len(extend_X)))
        #         for num in range(len(extend_X)):  # get all alternated stings
        #             X_train = np.append(X_train, extend_X[num])  # attach them to both X-...
        #             Y_train = np.append(Y_train, extend_Y[num])  # and Y-train


        #RANDOM SMILES METHOD FOR EXTENSION
        if n_random_smiles > 0:
            randnums = []
            extend_X = []
            extend_Y = []
            for num, smil in enumerate(X_train):
                if rdk == 1: #package rdkit loaded successfully -> obtain random smiles via package
                    k_rs = altern_smiles.get_x_random_smiles_alternations(smil[0],
                                                                      k_random_smiles)  # obtain k_random_smiles for each molecule in training data
                else:
                    k_rs = altern_smiles.get_x_random_smiles_alternations_from_csv(smil[0],
                                                                          k_random_smiles, rsmilfile[0])  # obtain k_random_smiles for each molecule in training data
                for i in k_rs:
                    extend_Y.append(Y_train[num])
                    extend_X.append(i)
                randnums.append(len(k_rs))

            if rands_nums_histo == 1:
                plot_results.nums_histo(randnums, 0, k_random_smiles, k_shifts, extend_frame)

            try:
                for num in random.sample(range(len(extend_X)), n_random_smiles):  # get specified number of random elements
                    X_train = np.append(X_train, extend_X[num])  # attach them to both X-...
                    Y_train = np.append(Y_train, extend_Y[num])  # and Y-train
            except:
                print("WARNING: The number of alternations by random smiles is only " + str(len(extend_X)))
                for num in range(len(extend_X)):  # get all alternated stings
                    X_train = np.append(X_train, extend_X[num])  # attach them to both X-...
                    Y_train = np.append(Y_train, extend_Y[num])  # and Y-train


        Y_train = np.reshape(Y_train, (len(X_train), len(outvals)), order="C") #unflatten the array

        new_X_train = split_chars(X_train)

        new_X_test = split_chars(X_test)

        split = len(new_X_test)
        data = np.concatenate([new_X_test, new_X_train])
        if nosplit == 0:
            data2 = np.concatenate([Y_test, Y_train])

        else:
            data2 = Y_train

        scaler_y = MinMaxScaler()
        scaler_y.fit(data2)
        data2 = scaler_y.transform(data2)

        Y_test = data2[:split]
        Y_train = data2[split:]

        lengths = []
        for dat in data:  # find max string length
            lengths.append(len(dat))
        maxlength = max(lengths)

        if vertical_shift != 0:
            newdata = []
            for dat in data:
                newdat = ["ö"]*vertical_shift
                for d in dat:
                    newdat.append(d)

                newdata.append(newdat)
            data = np.asarray(newdata)

        for dat in data:  # fill up with a new character to get equal lengths
            for i in range(maxlength + extend_frame):
                try:
                    dat[i]
                except:
                    dat.append("ö")


        uniques = []  # find all unique characters
        for dat in data:
            for char in dat:
                if char not in uniques:
                    uniques.append(char)

        #print(uniques)


        if make_cmpr == 1:
            # prepare molecules for comparison
            cdata = prepare_2d_smiles_data.read_input_sheets([cmprfile], 1)

            cdata_merged = prepare_2d_smiles_data.merge_data(cdata, ["E_HOMO"])

            cmdata = []
            for dat in cdata_merged:
                cmdata.append(dat[1:])
                while len(cmdata[-1]) < (maxlength + extend_frame): #transform cmdata to same frame size
                    cmdata[-1].append("ö")

            for dat in cmdata:
                for char in dat:
                    if char not in uniques:
                        uniques.append(char) #append unique chars for one hot encoder

        header0 = [] #obtain header for df (just numbers for smiles-chars)
        for prop in outvals:
            header0.append(prop)
        for i in range(maxlength + extend_frame):
            header0.append(i)

        dataf = pd.DataFrame(data=[row for row in data]) #transform list to DataFrame for one hot encoding


        #set feature order manually
        uniques = ['5', '+', 'F', 'l', 's', 'i', 'Br', '6', 'c', '1', '2', 'O', '=', '(', 'C', ')', 'P', 'H', '-', 'ö', '3', 'S', '4', 'o', 'N', '#', '7', '[', 'n', ']']
        #get array of unique chars for one hot encoding
        uniquesarray = []
        for i in range(dataf.shape[1]):
            uniquesarray.append(uniques)

        dataf = dataf.replace("\n", "ö")
        #print(dataf[dataf.eq("\n").any(1)])


        ohe = OneHotEncoder(categories=uniquesarray, dtype=bool)  # apply one-hot-encoding on all chars
        all_ohe = ohe.fit_transform(dataf)

        # if save_ohe == 1:
        #     filename = "ohe"
        #     outfile = open(filename, 'wb')
        #     pickle.dump([all_ohe, maxlength+extend_frame], outfile)
        #     outfile.close()

        filename = outfilename + "_OHE"
        outfile = open(filename, 'wb')
        pickle.dump([ohe, uniquesarray], outfile)
        outfile.close()

        if make_cmpr == 1:

            cdataf = pd.DataFrame(data=[row for row in cmdata])  # transform list to DataFrame for one hot encoding

            cmp_ohe = ohe.transform(cdataf)

            cmp_ohe = reshape_dimensions(cmp_ohe.toarray(), [len(cmdata), maxlength + extend_frame, len(uniques)],
                                        output_dim)

            if asfile == 1:
                filename = outfilename + "_cmpr"
                outfile = open(filename, 'wb')
                pickle.dump([cmp_ohe, scaler_y], outfile)
                outfile.close()


        X_test_ohe = all_ohe[:split].toarray()
        X_train_ohe = all_ohe[split:].toarray()

        # reshape to 2- or 3-dim input
        X_test = reshape_dimensions(X_test_ohe, [len(new_X_test), maxlength+extend_frame, len(uniques)], output_dim)
        X_train = reshape_dimensions(X_train_ohe, [len(new_X_train), maxlength+extend_frame, len(uniques)], output_dim)

        #SHIFTS METHOD FOR EXTENSION
        if n_shifts > 0:
            shiftsnum = []
            extensionsx = []
            extensionsy = []
            for quinnum, quin in enumerate(X_train):
                quins = altern_smiles.get_x_shifts(quin, k_shifts, [maxlength, len(uniques), uniques.index("ö")]) #call shifts function
                shiftsnum.append(len(quins))
                for shift in quins:
                    extensionsx.append(shift)  # save the resulting string
                    extensionsy.append(Y_train[quinnum])  # get associated y values

            try:
                for num in random.sample(range(len(extensionsx)), n_shifts):  # get specified number of random elements
                    X_train = np.concatenate([X_train, [extensionsx[num]]])  # attach them to both X-...
                    Y_train = np.concatenate([Y_train, [extensionsy[num]]])  # and Y-train
            except:
                print("WARNING: The number of alternations by shifts is only " + str(len(extensionsx)))
                for num in range(len(extensionsx)):  # get all alternated stings
                    X_train = np.concatenate([X_train, [extensionsx[num]]])  # attach them to both X-...
                    Y_train = np.concatenate([Y_train, [extensionsy[num]]])  # and Y-train

            if rands_nums_histo == 1:
                plot_results.nums_histo(shiftsnum, 1, k_random_smiles, k_shifts, extend_frame)

        if merge_multi_out == 1:
            X_train_val = Y_train[:, 0]
            Y_train = Y_train[:, 1]
            X_test_val = Y_test[:, 0]
            Y_test = Y_test[:, 1]

            returns.append([X_train, X_test, Y_train, Y_test, scaler_y, X_train_val, X_test_val])

        # print(X_train.shape)
        # print(X_test.shape)
        # print(Y_train.shape)
        # print(Y_test.shape)

        #print(len(returns))
        for ret in returns:
            #print(len(ret))
            for retret in ret:
                try:
                    print(len(retret))
                except:
                    pass
        if fulltest == 0 and setnum == len(sets) - 1 and crossval != 0:

            Xtesttest_enc = X_test[-50:]
            Ytesttest_enc = Y_test[-50:]
            X_test = X_test[:-50]
            Y_test = Y_test[:-50]
            filename = outfilename + "_test_set"
            outfile = open(filename, 'wb')
            print(str(Ytesttest[0][0]) + ";" + str(Ytesttest[-1][0]))
            print(str(Ytesttest_enc[0][0]) + ";" + str(Ytesttest_enc[-1][0]))
            pickle.dump([Xtesttest, Ytesttest, Xtesttest_enc, Ytesttest_enc], outfile)
            outfile.close()

        returns.append([X_train, X_test, Y_train, Y_test, scaler_y])

    if fulltest == 1:
        Xtesttest_enc = returns[0][0]
        Ytesttest_enc = returns[0][2]
        Ytesttest = Y
        Xtesttest = X
        filename = outfilename + "_test_set"
        outfile = open(filename, 'wb')
        pickle.dump([Xtesttest, Ytesttest, Xtesttest_enc, Ytesttest_enc], outfile)
        outfile.close()


    if asfile == 1:
        filename = outfilename
        outfile = open(filename, 'wb')
        pickle.dump(returns, outfile)
        outfile.close()
    return returns

def prepare_input(file, output_dim=2, outvals=["E", "Gsolv"], allvals=["E", "Gsolv"], extend_trainingset=0, shifts=20):
    "Simple and fast function to prepare NN input from one-hot-encoding sheet (obtained with prepare_2d_smiles_data). Only shifts_method supported, not randomSmiles."

    #define specific string information: 1) number of unique chars 2) length of longest char+extension 3) position of empty char in uniqes
    if "tabor" in file:
        specs = [18, 94, 9]

    elif "pdf" in file or "krist" in file:
        specs = [28, 125, 8]

    elif "_er" in file:
        specs = [22, 150, 8]

    elif "_ramak" in file:
        specs = [16, 24, 6]

    else:
        specs = [30, 150, 9]

    df = pd.read_csv(file) #read file

    # shuffle dataset
    df = shuffle(df)

    # remove nans
    df = df.dropna(axis=0, how="any")

    # define the labels and split them from the rest (features)
    features = df
    labelnames = allvals

    labelsList = []
    for i in outvals: #only use labels from argument
        labelsList.append(features[i])
    for i in labelnames:
        features = features.drop(i, axis=1)  # remove from features

    # prepare arrays and transposed arrays
    labelst = np.array(labelsList)
    labels = np.transpose(labelst)
    feature_list = list(features.columns)
    features = np.array(features)

    # normalize labels
    scaler_y = MinMaxScaler()
    scaler_y.fit(labels)
    Y = scaler_y.transform(labels)
    # but not features (only 0 and 1)

    # reshape x for 2-dim input
    X = reshape_dimensions(features, [len(features), specs[1], specs[0]], output_dim)

    # split training and test data
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)


    if extend_trainingset != 0:
        extensionsx = []
        extensionsy = []
        for quinnum, quin in enumerate(X_train):
            altern_smiles.get_x_shifts(quin, shifts, specs) #call shifts function
            extensionsx.append(quin)  # save the resulting string
            extensionsy.append(Y_train[quinnum])  # get associated y values

        try:
            for num in random.sample(range(len(extensionsx)), extend_trainingset):  # get specified number of random elements
                X_train = np.concatenate([X_train, [extensionsx[num]]])  # attach them to both X-...
                Y_train = np.concatenate([Y_train, [extensionsy[num]]])  # and Y-train
        except:
            print("WARNING: The number of alternations by shifts is only " + str(len(extensionsx)))
            for num in range(len(extensionsx)):  # get all alternated stings
                X_train = np.concatenate([X_train, [extensionsx[num]]])  # attach them to both X-...
                Y_train = np.concatenate([Y_train, [extensionsy[num]]])  # and Y-train



    return [X_train, X_test, Y_train, Y_test, scaler_y]