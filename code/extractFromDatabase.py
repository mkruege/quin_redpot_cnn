import urllib.request
import pandas as pd
import csv
import pubchempy as pcp
import re


#failed: ['Isocochlioquinone B', 'Coleone U quinone', 'Isocochlioquinone A', 'Julichrome Q1,9', 'Anhydroatrovirin 9,10 quinone', '7 Hydroxy 8 methoxymalbranicin 5,6 quinone', 'g L Glutaminyl 3,4 benzoquinone', 'Scleroquinone', 'Hydroxymethyl 1,4 benzoquinone', 'Methylenediboviquinone 3,3', '5 Bromotoluhydroquinone', '2 Methoxy 3 methylbenzoquinone', '2 Heptaprenyl 1,4 benzoquinone', '2 Octaprenyl 1,4 benzoquinone', 'g,g Dimethylallyl 1,4 benzoquinone', '1,2 Methylenedioxy 6a,7 dehydroaporphine 10,11 quinone', "1 Hydroxy 5',8' dimethoxy 3,3' dimethyl 2,2' binaphthyl 5,8,1',4' diquinone", '(+) Sclerotiorin', 'Julichrome Q1,4', 'Julichrome Q8,8', 'Julichrome Q3,8', 'Julichrome Q1,7', "2 (2',3' Dihydrosorbyl) 3,6 dimethyl 5 hydroxy 1,4 benzoquinone", '8’,9’ Dihydroxysargaquinone', '2 Geranylgeranyl 6 methylbenzohydroquinone', 'Menzohydroquinone', '2 (Geranylgeranyl) 6 methyl 1,4 benzohydroquinone', '2,3 Dimethoxy 6 propyl hydroquinone', "11' Methoxysargaquinone", '2 Geranylgeranyl 6 methylbenzoquinone derivative', '6 Geranyl 2 methyl 1,4 benzoquinone', 'n Propyl 1,4 benzoquinone', 'Ethylbenzoquinone', '( ) Curcuhydroquinone', 'Capillaquinone', '5 Geranyl 2 methyl 1,4 benzoquinone', '5 Farnesyl 2 methyl 1,4 benzoquinone', '2 Hexaprenyl 1,4 benzoquinone', '2 Pentaprenyl 1,4 benzoquinone', 'Geranylgeranylbenzoquinone', 'd Tocopherylquinone', '11 O Methyl epicochlioquinone A', '14 Epidihydrocochlioquinone B', '2 Acetoxycurcuquinone', 'Hydroxy 2 octaprenyl 1,4 benzohydroquinone', 'Asterriquinone A 1', '2,5 Dimethoxy 3,6 dimethylhydroquinone', 'Rubrosinensiquinone A', 'Desoxytridentoquinone', 'Ubiquinone precursor', '7 Methoxy malbranicin hydroquinone', 'Ubiquinone Q12', 'Asterriquinone B 4', 'X Dihydro ubiquinone 10', "2 [(2'E,6'E,10'E,14'Z) 5' hydroxy 15' hydroxymethyl 3',7',11' trimethylhexadeca  2',6',10',14' tetraenyl] 6 methylhydroquinone", 'Asterriquinone A 4', '( ) Pleurotin', '2,3 Didehydro 19a hydroxy 14 epicochlioquinone B', "8',9' Diydroxy 5 methylsargaquinone", 'Ubiquinone precursor', "(2'E, 6'E, 10'E, 14'E) 2 (8' one 15' formyl 3',7',11'  trimethylhexadeca 2',6',10',14' tetraenyl) 6 methyl 1,4 benzoquinone", '(3S) Mucroquinone', 'Ubiquinone precursor', "(2'E,6'E,10'E,14'E) 2 (8',9' Dione 3',7',11',15'  tetramethylhexadeca  2',6',10',14' tetraenyl) 6 methyl 1,4 benzoquinone", '5,8 Dimethoxy 6 methyl 1,2 naphthoquinone', '(3R) Mucroquinone', 'd tocopheryl hydroquinone', 'Asterriquinone SU 5500', '2 Methoxy 6 (1 propyl) 1,4 benzoquinone', "(2'E,6'E,10'E,14'E) 2 (9' Hydroxy 3',7',11',15'  tetramethylhexadeca  2',6',10',14' tetraenyl) 6 methyl 1,4 benzoquinone", '2,3 Dimethoxy 5 methyl heptaprenyl 1,4 benzoquinone', "9' Methoxysargaquinone", '10’,11’ Dihydroxysargaquinone', 'Ubiquinone precursor', '(R) Curcuquinone', 'Rubrosinensiquinone C', 'Tetrahydromethionaquinone', '2,3 Dimethoxy 6 propyl 1,4 benzoquinone', '5 (g,g Dimethylallyl) 2 methylbenzoquinone', '5 Geranylgeranyl 2 methyl 1,4 benzoquinone', 'Rubrocashmeriquinone', '3 Acetyl 5 hydroxy 2 methylnaphthoquinone', '5 Farnesyl 2 methyl 1,4 benzohydroquinone', 'Laurequinone', 'Ubiquinone precursor', 'Ubiquinone precursor', 'Ubiquinone precursor', 'Ubiquinone precursor', 'Cystoquinone', '(S) Lagopodin A', 'Gonyleptidin', 'Ubiquinone precursor', '3 Acetyl 5 hydroxy 7 methoxy 2 methylnaphthoquinone', 'Hyathellaquinone', 'Rotundiquinone dimethyl ether', 'Rubrogliocladin', 'Asterriquinone A 2', "5,8,8' Trihydroxy 6,6' dimethyl 2,2' binaphthoquinone", "3',4' Dehydroviomellein", 'Dihydroxythymoquinone', 'Rubrosinensiquinone B', 'Julichrome Q1,3', 'IFO 8835 10', 'Julichrome Q1,6', 'IFO 8835 8', '5 Hydroxy 6 methoxy 2,3 dimethyl 1,4 benzoquinone', 'Ardisiaquinone C', 'IFO 8835 4', '2,6 Di tert. butylbenzoquinone', 'Methylenediboviquinone 3,4', '2 Methoxy 6 propyl hydroquinone', 'Asterriquinone C 1 quinol', '4,9 Dihydroxy 1,2,6,7,11,12 hexymethylperylene 3,10 quinone', 'Dietchequinone', 'Tocopheryl hydroquinone', '6 Hexanoyl 2,5,7,8 tetrahydroxy 1,4 naphthoquinone', 'Dihydrophytylplastoquinon e', '5 Chloro 3,6 dihydroxy 2 methyl 1,4 benzoquinone', 'Asterriquinone A 1 quinol', '2,3 Dimethoxy 5,6 dimethyl 1,4 benzoquinone', 'IFO 8835 12', 'Ubiquinone precursor', '2,5 Dihydroxy 3,6 diphenyl 1,4 benzoquinone', 'Ubiquinone precursor', 'Ubiquinone precursor', '(3R) Dihydroscabequinone', '3 Hydroxy 5 methoxy 2 methylbenzoquinone', '2,5 Dihydroxy 3 (3,4 dihydroxyphenyl) 6 phenyl 1,4 benzoquinone', 'Psoralenquinone', '2 Hydroxy 5 methoxy 3 pentadecenyl 1,4 benzoquinone', '5,6 Dimethoxy 2 methyl 1,4 naphthoquinone', '6 (1 Acetoxyethyl)2,7 dimethoxyjuglone', 'iso Asterriquinone', 'Maesaquinone monoacetate', '(R) Stenocarpoquinone A', 'IFO 8835 16', 'Leucohymenoquinone', "4' Hydroxyphlebiarubrone", '3 Acetyl 5,7 dimethoxy 2 methylnaphthoquinone', 'Mutaquinone A', 'Dimethoxy p xylohydroquinone', 'Furanonaphthoquinone', 'Endocrocin 6,8 dimethyl ether', '3 Hydroxytoluquinone', 'Betulachrysoquinone', '3’,4’,4’’ Trihydroxyphlebiarubrone', '6 Methyldihydrophytylplasto quinone', "5,5',8 Trihydroxy 7,7'dimethyl 2,2' binaphthoquinone", '3 Libocedroxythymoquinone', '3,6 Dihydroxy 2 methyl 1,4 benzoquinone', '(3S) Abruquinone B', 'Dehydrocyclospongiaquino ne 1', '(3S) Abruquinone A', 'Mutaquinone D', '(3S) Abruquinone C', '2 Ethyl 3,6 dihydroxy 1,4 benzoquinone', 'Mutaquinone B', 'Mutaquinone C', 'Ubiquinone Q8', "2 Hydroxy 5 methoxy 3 (8'Z, 11'Z,14' pentadecatrien)yl 1,4 benzoquinone", 'Dnacin B', '2 Hydroxy 3 methyl 5 methoxy p benzoquinone', '2,3 Dimethoxy 5 methyl 6 IX,X tetrahydrofarnesylfarnesyl geranyl geranyl 1,4  benzoquinone', "3,4,3',4' Bisdehydroxanthomegnin", 'Hydroxybreviquinone', 'neo Asterriquinone', "3,3',8,8' Tetramethoxy 6,6' dimethyl 2,2' binaphthoquinone", '2,5 Dihydroxy 3 phenyl 6 (3,4,5 trihydroxyphenyl) 1,4 benzoquinone', '2,5 Dimethoxy 3,6 dimethyl 1,4 benzoquinone', '3 Hydroxythymoquinone', 'Bryebinalquinone', 'O Methylpulviquinone A', '5 Chlorodermorubin', '3 Hexyl 2 hydroxy 6 pentylbenzoquinone', '2,5,7 Trimethoxy naphthoquinone', 'Cassumunaquinone 2', 'Tanshindol B', '2 O,8 O Dimethyljavanicin', 'Prezwaquinone', 'Tanshindol C', '5,6 Dihydroxy 2,3 dimethyl 1,4 benzoquinone', '(3R) Scabequinone', 'Javanicin 8 methyl ether', '(5S,7S) 5,6,7,8 Tetrahydro 5,7 dihydroxy 2 methoxy 7 methyl 1,4 anthraquinone', 'b Methylpyrano 1,4 naphthoquinone', '1,6 Dimethyl 7 methoxyisoquinoline 5,8 quinone', '1,4 Naphthoquinone 8 hydroxy 3 [(3S) acetoxy]butyric acid', '1,4 Dihydroxy 2,5 dimethoxy 7 methylanthraquinone', 'Granaticin methyl ester', 'Cassumunaquinone 1', 'Dicinnaquinone', '8 Hydroxy 2,5,6 trimethoxy 7 (2 oxopropyl) 1,4 naphthoquinone', 'Hexahydromenaquinone MK 9(II,III,IX H6)', "3,3' Bidiomelquinone", 'Menaquinone MK 9(II,III H4)', '(3R) Hydroxyscabequinone', 'g Naphthocyclinone', 'Chlorobiumquinone', 'Menaquinone MK 8(II,III H4)', '8 Hydroxy 2,5 dimethoxy 6 methyl 7 (2 oxopropyl) 1,4 naphthoquinone', 'Hexahydromenaquinone MK 9(II,III,VIII H6)', '3,4 Dehydroxanthomegnin diacetat', '2 Dodecyl naphthoquinone', '2 Hexylnaphthoquinone', 'K 82 A', 'd Actinorhodin', 'Nepenthon A', 'II Dihydromenaquinone 10, MK 10(II H2)', 'II Dihydromenaquinone 7', 'Methylenediboviquinone 3,4', 'Menaquinone MK 8(II H2)', 'Fusarubin methyl acetal', 'Fusarubin ethyl acetal', 'Deoxybryaquinone', 'Menaquinone MK 5(V H2)', 'Menaquinone', '2 Methyl 3 VI,VII tetrahydroheptaprenyl 1,4 naphthoquinone', 'Menaquinone', 'Menaquinone MK 6(VI H2)', 'Solanoquinone', 'Dimethylhexyl)cyclopentyl] 3,7,11 trimethyl tetradecyl} 3 methyl 1,4  naphthoquinone', 'Menaquinone', '3 Methoxy 2 methyl 9H carbazole 1,4 quinone', 'Dihydromenaquinone MK 9(II H2)', '1,6,8 Trihydroxy 3 methylanthraquinone 2 carboxylic acid', '6 O Methyldermorubin', 'Dicyperaquinone D', 'Rotundiquinone', 'Olivovariin', '2,3 Didodecyl naphthoquinone', 'Dicyperaquinone A', '2 Hexyl 3 methyl naphthoquinone', 'Menaquinone MK 7', 'Menaquinone MK 7(VII H2)', 'Menaquinone MK 8(VIII H2)', '2 Methyl 3 (g,g dimethylallyl) 1,4 naphthoquinone', 'Menaquinone MK 8', '4,7 Dihydro 6 methoxy 2,5 dimethyl 2H isoindole 4,7 dione', 'Dicyperaquinone B', '6,7 Diethoxy 2 methylnaphthoquinone', 'Dicyperaquinone E', 'Dicyperaquinone C', 'Menaquinone 10 (II , III H4)', '8 Hydroxy 5,6 dimethoxy 2 methyl 3 (2 oxopropyl) 1,4 naphthoquinone', "(3'R,P) Anhydropseudophlegmacin 9,10 quinone 1,6',8' tri O methyl ether", '1,6,8 Trimethoxy 3 propanoylanthraquinone', "5,5' Dihydroxy 7,7' dimethyl 6,6' binaphthyl 1,4", 'AM 3867 I', "b' Dihydrodiospyrin", '6 Bromo 5 hydroxy 6 methylnaphthoquinone', '2,5 Di (N ( ) prolyl) p benzoquinone', 'Phthyocol', 'Dimethylmenaquinone', '2 trans Thermoplasmaquinone', 'F8', 'Cadalene 1,4 quinone', 'Methylmenaquinone MMK 8(VIII H2)', '2 cis Thermoplasmaquinone', '2 Hydroxymethyl 1 methoxy anthraquinone', "Anhydroflavomannin 9,10 quinone 6,6',8' tri O methyl ether", 'Pulviquinone A', 'Dimethylmenaquinone DMMK 8(VIII H2)', "5,5',6,6' Tetrahydroxy 2,2' dimethyl 2,2' binaphthoquinone", "5,5',8 Trihydroxy 2,2' dimethyl 6,6' binaphthoquinone", '(+) Chloromycorrhizin A', '2 Butyryl 1,8 dihydroxy 3 methylanthraquinone', '8 O Methyl 2 hydroxyjavanicin', '2 Acetylnaphthol[2,3 b]furan 4,9 quinone', '7 Acetonyl 5 hydroxy 6 methyl 1,4 naphthoquinone', 'Atrovenetin', '(5S,6R,8R,10R) Sarubicin A', '8 Hydroxy l,6 dimethyl 3 methylanthraquinone', '2,3 Diacetoxy 1,4 dihydroxyanthraquinone', '6 Bromo 5 hydroxy 2 methylnaphthoquinone', '(1S,3S,4S) Nanaomycin D', '1 Bromo 4 hydroxy 5,7 dimethoxy 2 methylanthraquinone', '6 Methylxanthopurpurin 3 methyl ether', 'Cinnaquinone', '( ) Kinamycin A', 'Coronatoquinone', '2 (1 Hydroxyethyl)naphthol[2,3 b]furan 4,9 quinone', 'F7', '8 Hydroxy 2,5,6 trimethoxy 3 (2 oxopropyl) 1,4 naphthoquinone', '5 Chloro 6,8 dihydroxy 1 methoxy 3 methylanthraquinone', 'a Ethylfurano 1,4 naphthoquinone', 'Julichrome Q3,4', '2 Isopropenylnaphtho[2,3 b]furan 4,9 quinone', '1 Hydroxy 3 methoxy 6 methyl anthraquinone 1 O  b D gentiobioside', 'Ventiloquinone H', '1,3,7 Trimethoxy 6 methylanthraquinone', '2,5 (or 3,5) Dihydroxy 1,3,4 (or 1,2,4)  trimethoxyanthraquinone', 'O Demethylanhydrofusarubi n', 'Kwanzoquinone A monoacetate', 'Kwanzoquinone B monoacetate', 'Coniothranthraquinone', '4 Deoxyanhydrofusarubin', '4 Hydroxypiloquinone', 'Antibiotic Zgg', 'Desoxyfrenolicin', '2 Hydroxy 6 methoxy 3,5 dimethyl 1,4  benzoquinone', "(3S,3'S,P) Anhydrophlegmacin 9,10  quinone 8' O methyl ether", 'Trichodermaquinone', 'Nanaomycin betaA', '(+) Mycorrhizin A', 'Murayaanthraquinone', 'Anhydrofusarubin lactone', '7 Hydroxy 1,8 dimethoxy 2,3 methylene dioxyanthraquinone', 'A 7884', "4,11 Dihydroxy 5 methoxy 2,9 dimethyldinaphtho[1,2 b:2',3' d]furan 7,12 quinone", '3,4,6,9 Tetrahydro 10 hydroxy 7 methoxy 3 methyl 1,6,9 trioxo 1H naphtho [2,3 c]pyran', 'Ventiloquinone C', '3,5,7 Trihydroxy 2 methoxy 1,4 naphthoquinone', 'Ventiloquinone D', 'Iso aeroplysinin 1', '1,2 Methylenedioxyanthraquin one', '2 Hydroxy 1,6 dimethoxy 3 methylanthraquinone', 'Anhydrophlegmacinquinon e B2', 'Anhydrophlegmacinquinon e A2', 'Lepiotaquinone', "1,3,6,8 Tetrahydroxy 2 (1' hydroxy 3' oxobutyl) anthraquinone", 'MM 061', 'Ventiloquinone O', "Dianhydroflavomannin 9,10 quinone 6,6' di O methyl ether", '9 Chloro 10 hydroxy 1,4 anthraquinone', '( ) Kinamycin C', '1 Hydroxy 5 methoxy anthraquinone', '5 Geranyl 3,6 dihydroxy 2 methyl 1,4 naphthoquinone', '( ) Kinamycin D', '1,3 Dihydroxy 8 methoxy 6 methoxymethylanthraquin one', 'Gonioquinone', 'Coleone U quinone', '8 Hydroxy 2 isopropenylnaphtho[2,3 b]furan 4,9 quinone', '4 Hydroxy 3 hydroxymethyl 5,6 dimethoxyanthraquinone', '2 Hydroxyanthraquinone 3 aldehyde', 'Helicquinone', '2 Hydroxymethylanthraquin one', "7,7' Bi(3 ethyl 2,6 dihydroxynaphthazarin)", "Anhydroflavomannin 5,8 quinone 6,6' di O methyl ether", '5,10 Dihydroxy 1,4 anthraquinone', '5 Hydroxy 2,7 dimethoxy 1,4 naphthoquinone', "4,4' Bis(1,3,8 trihydroxy 3 methyl 6  methoxyanthraquinone)", '3 O,9 O Dimethylfusarubin', '5 Deoxy 3,4 anhydrofusarubin', '3,8 Dihydroxy 1 methoxy 9,10 anthraquinone', 'Naphthoquinone 4', '( ) Kinamycin B', '3 Hydroxy 2 methylanthraquinone', "(2'S) Hydroxy (3S,3'S,P)  anhydrophlegmacin 9,10  quinone 8' O methyl ether", '6 (1 Hydroxyethyl) 2,7 dimethoxyjuglone', '2 Methoxy 3 methylanthraquinone', 'g Actinorhodin', 'Methyl 3,4,8 trihydroxy 1 methylanthraquinone 2 carboxylate 4 methyl ether', '2 (2,3 Dihydro 5 methyl 6 oxopyran 2 yl) 5,8 dihydroxy 1,4 naphthoquinone', 'anthraquinones 9', '6 (1 Ethoxyethyl) 5 hydroxy 2,7 dimethoxy 1,4 naphthoquinone', '7,10 Dihydroxy 5 methoxy 2 methyl 1,4 anthraquinone', '5,6 Dihydroxy 2 methylnaphthoquinone', '2 Amino 3,6 dioxo 4,5 epoxycyclohexane carboxylic acid', "Anhydroflavomannin 9,10 quinone 6,6' dimethyl ether", 'Eleuthraquinone B', 'Draculone', '8 Hydroxy 6 hydroxymethyl 1,3 dimethoxyanthraquinone', 'Y 1005', '6 Ethyl 5,7 dihydroxy 2 methoxy naphthoquinone', '6 Ethyl 2,7 dimethoxyjuglone', '(±) Versicolorin C', '5 Hydroxy 1,3 dimethoxy 7 methylanthraquinone', '4 Deoxyjavanicin', "1,5',8' Trihydroxy 3,3' dimethyl 2,2' binaphthyl 5,8,1',4' diquinone", '3 Bromo 4 hydroxy 5,10 dioxo 1 antrha 91 cenecarboxylic acid', '1 Hydroxy 6,8 dimothoxy 3 methylanthraquinone', "5,5',8,8' Tetrahydroxy 7,7'dimethyl 2,2' binaphthoquinone", "(S) 2,5,7 Trihydroxy 3 (5' hydroxyhexyl) 1,4 naphthoquinone", '5,10 Dihydroxy 7 methoxy 1,4 anthraquinone', '1 Hydroxy 5,7 dimethoxy 2 methylanthraquinone', '1 Hydroxyanthraquinone 3 caboxylic acid', 'Methyl 3,4,8 trihydroxy 1 methylanthraquinone 2 carboxylate 3,4 dimethyl ether', '10 Hydroxy 2,5 dimethoxy 7 methyl 1,4 anthraquinone', '1 Chloro 4 hydroxy 2 methylanthraquinone', 'Cordeaxione', '1 Chloro 4 hydroxy 3 methylanthraquinone', '3 Hydroxy 8 O acetyl 1 methylanthraquinone 2 carboxylic acid', '9 O Methylfusarubin', "trans 4'  Hydroxyanhydroflavoman nin 9,10 quinone 6,6' di O methyl ether", 'Biruloquinone', '3,5,8 Trihydroxy 6 methoxy 2 (5 oxohexa 1,3 dienyl) 1,4  naphthoquinone', '3 Hydroxy 8 methoxy 1 propylanthraquinone', '1 Hydroxy 4,6,8 trimethoxyanthraquinone', '5,10 Dihydroxy 2 methyl 1,4 anthraquinone', "5,8,8' Trihydroxy 3,3' dimethyl 2,2' binaphthoquinone", '6 Hydroxy 1,3 dimethoxy 7 methylanthraquinone', 'b Actinorhodin', "1,1',4,4' Tetrahydroxy 6,6' dimethyl 2,2' binaphthyl 5,8", 'O Demethyljavanicin', 'Anyhdrofusarubin 9 methyl ether', "(1R,1'R,3S,3'S) Actinorhodin", 'Julichrome Q4,5', 'Naphthoquinone 3', '1 Hydroxyanthraquinone 2 carboxylic acid methyl ester', '1,5 Dihydroxy 2,6 dimethoxy anthraquinone', '5,7,10 Trihydroxy 2 methyl 1,4 anthraquinone', '11 Deoxy bisanhydro 13 dihydrodaunomycinone', '5 Hydroxydigitolutein', '9 Ethyl 6 hydroxy 4 methoxynaphthacene 5,12 quinone', 'DK 7814C', '5,10 Dihydroxy 7 methoxy 2 methyl 1,4 anthraquinone', '5,10 Dihydroxy 2 methoxy 7 methyl 1,4 anthraquinone', "5,8,8' Trihydroxy 3',6 dimethyl 2,5' dinaphthoquinone", 'DF 7814A', 'DK 7814B', 'Diazadiphenoquinone', '2 Acetyl 3,8 dihydroxy 6 methoxy anthraquinone (or 3 Acetyl 2,8 dihydroxy 6 methoxyanthraquinone)', '2 Acetoxy 1 hydroxy anthraquinone', "Anhydroflavomannin 1,4 quinone 6,6' di O methyl ether", 'Ventiloquinone I', '2,6 Diacetoxy 1 hydroxyanthraquinone', '(7S) 5,6,7,8 Tetrahydro 7 hydroxy 2 methoxy 7 methyl 1,4 anthraquinone', '1 Hydroxy 7 methylanthraquinone', '1 Hydroxy 6 or 7 hydroxymethylanthraquinone', "4',10 Dihydroxy 4 oxoanhydro flavomannin 9',10' quinone dimethyl ether A", 'b Naphthocyclinone', '1,7 Dihydroxy 3 hydroxymethyl 9,10 anthraquinone', '5,6 Dihydro 4,7,9,12 tetrahydroxy 2 methylbenzo[a]naphthace ne 8,13 quinone', '1 Hydroxy 5 methoxy 2 methylanthraquinone', '5,8 Dihydroxy 2,6 dimethoxy 7 (2 oxopropyl) 1,4 naphthoquinone', 'Naphthoquinone 2', 'Xanthopurpurin 3 methyl ether', '1 Hydroxy 2 hydromethyl dimethoxyanthraquinone', '1 Amino 8 hydroxy anthraquinone', '6 Ethyl 2,5,7,8 tetrahydroxy 1,4 naphthoquinone', '8 Hydroxy 3 methoxy 1  methylanthraquinone 2 carboxylic acid methyl ester', '1 Hydroxy 7 methoxyanthraquinone', '2 (1 Hydroxyethyl) 3,8 dihydroxy 6 methoxyanthraquinone (or dihydroxy 6 methoxyanthraquinone)', 'Methyl 8 hydroxy 3 methoxy 1 propylanthraquinone 2 carboxylate', "5 Hydroxyanhydroflavoman nin 9,10 quinone 6,6' di O methyl ether", '1,3 Dihydroxy 2,5 dimethoxyanthraquinone', '1 Hydroxy 6 methoxyanthraquinone', '1,6,8 Trhydroxy 3 propanoylanthraquinone', 'Deflectin 1b', '1,3 Dihydroxy 6 methyl 7 methoxyanthraquinone', '1,4 Dihydroxy 2,3 dimethoxy anthraquinone', '5,6 Dihydro 4,7,9,12 tetrahydroxy 2 methylbenzo[a]naphthacene 8,13 quinone 10 or carboxylic acid', '3 O Methylfusarubin', '1 Bromo 4,5 dihydroxy 7 methoxy 2 methylanthraquinone', '1,3 Dihydroxy 5,6 dimethoxy 2 methoxymethyl anthraquinone', '1 Hydroxy 2,5 dimethoxy anthraquinone', 'Catenarin 5 methylether', 'dimeric Ventiloquinone', '1 Hydroxy 3,7 dimethoxy 6 methylanthraquinone', '1 Hydroxy 3,7 dimethoxy anthraquinone', '1,3 Dihydroxy 5,6 dimethoxy 2 methyl anthraquinone', 'Anhydropseudophlegmaci n 9,10 quinone 3 amino 8  O methyl ether', '3 Amino 1 hydroxy 2 methoxyanthraquinone', '2061 A 67', 'K 1115 A', 'Eleuthraquinone A', '3,5,6 Trihydroxy 2 methylanthraquinone', '1,6 Dihydroxy 8 propylanthraquinone', 'Demethylmacrosporin', '2 Hydroxynorjavanicin', '1 Hydroxy 2,7 dimethoxy anthraquinone', '5,7 Dihydroxy 2 [1 (4 methoxy 6 oxo 6H pyran 2 yl) 2 phenylethylamino]  [1,4]naphthoquinone', "(3'R,P) Anhydropseudophlegmacin 9,10 quinone 6',8' di O methyl ether", "4 Hydroxy 4' oxoanhydro flavomannin 9,10 quinone", '2,3 Dimethoxy 5,6  dimethyl 2 cyclohexene 1,4 dione', '1 Hydroxy 6 methoxy 8 methylanthraquinone', '1 Hydroxy 2,6 dimethoxyanthraquinone', "9' Hydroxyaloesaponarin II", '1 Hydroxy 6 methoxy 8 propylanthraquinone', '2 Acetyl 1,8 dihydroxy 3 methyl anthraquinone', '1,4 Dihydroxy 5,7 dimethoxyanthraquinone', 'O Demethylfusarubin', 'Antibiotic Zg', '1,3 Dihydroxy 6 methoxy 2 methoxymethylanthraquin one', '1,4,6 Trihydroxy 5 methoxy 2 or 3 methylanthraquinone', '3 Methyl 5,6(7),8 trihydroxy 2 aza (9,10) anthraquinone', '1,3 Dihydroxy 6,8 dimethoxy 2 methylanthraquinone', 'S 383 O', '1 Amino 5 hydroxy anthraquinone', '1,8 Dihydroxy 2 ethyl 3 methylanthraquinone', '1,8 Dihydroxy 5 methylanthraquinone', '1,8 Dihydroxy 2,7 dimethoxyanthraquinone', '2 Acetoxy 1,5 dihydroxyanthraquinone', 'Julichrome Q2,5', 'a2 Rhodomycin A', '1,6,8 Trihydroxy 3 (2 hydroxypentyl)anthraquinone', '3,7 Diacetoxy 1,5 dihydroxy anthraquinone', '1,4,5 Trimethoxy 8 methoxy anthraquinone', "4 Hydroxyanhydroflavoman nin 9,10 quinone 6,6' di O  methyl ether", 'Methyl 3,4,8 trihydroxy 1 methylanthraquinone 2 carboxylate 3 methyl ether', 'Averythrin 6 methyl ether', '2,3,6,7 Tetraacetoxy 1,5 dihydroxyanthraquinone', '3 Methoxychrysazin', '1,8 Dihydroxy 2 methoxy 6 methylanthraquinone', '4,5 Dihydroxy 2 methoxycarbonylmethyl 3 oxopentyl)anthraquinone', '1,4 Dihydroxy 6 methoxy anthraquinone', 'Dermoquinone', '1,3,6,8 Tetrahydroxyanthraquinon e', '1,8 Dihydroxy 3,6 dimethoxy anthraquinone', '1,5 Dihydroxy 3 methoxy anthraquinone', '2 n Butyl 1,4 dihydroxy anthraquinone', 'Julichrome Q5,5', '1,4 Dihydroxy 7 hydroxymethyl 2,5 dimethoxyanthraquinone', '9 Ethyl 4,6 dihydroxynaphthacene 5,12 quinone', '3,5,8 Trihydroxy 7 methoxy 2 methylanthraquinone', '2 Acetoxy 1,4,8 trihydroxyanthraquinone', '1,2,3,8 Tetrahydroxy 6 methylanthraquinone', 'Emodin 6 geranyl ether', '2 n Butyl 1,4 dihydroxy 3 methoxyanthraquinone', '1,8 Dihydroxy 3,6 dimethoxy 2 methyl 7 vinylanthraquinone', 'Julichrome Q5,6', '1,3,6,8 Tetrahydroxy 2 (1 methoxyhexyl)anthraquinone', '1,3,6,8 Tetrahydroxy 2 methoxyethylanthraquinon e IV', '1,3,5,8 Tetrahydroxy 6,7 dimethoxy 2 methylanthraquinone', '4,5,8 Trihydroxy 2 methoxycarbonylmethyl 3 oxopentyl)anthraquinone', '1,5 Dihydroxy 2,3,6,7 tetramethoxyanthraquinone', 'Julichrome Q2,2', '1,4,5 Trihydroxy 6 acetoxyanthraquinone', '1,3,6,7 8 Pentahydroxy 2 methoxyethylanthraquinon eV', 'A 80915 G', '1,4 Dihydroxy 2,5,8 trimethylanthraquinone', "(+) 3,3',7,7',8,8' Hexahydroxy 5,5' dimethyl bisanthraquinone", '1,3,5 Trihydroxy 8 methoxy 2 methyanthraquinone', '1,4,5 Trihydroxy 2 methoxy anthraquinone', 'Basidiodifferoquinone', 'Nanaomycin betaE', 'Nanaomycin aE', '1,3,4,5 Tetrahydroxy 2 methylanthraquinone', '1,4,5 Trihydroxy 7 methoxy 3 methylanthraquinone', '1,4,5,7 Tetrahydroxy 2 (1 hydroxypropyl)anthraquinone', '1,4,7,8 Tetrahydroxy 2 methylanthraquinone', '1,5 Dihydroxy 2 methoxy anthraquinone', 'Nanaomycin aB', '1,4,5 Trihydroxy 7 methoxyanthraquinone', '1,3,8 Trihydroxy 2 (1 hydroxyhexyl) 6 methoxyanthraquinone', '2 n Butyl 1,4,5,8 tetrahydroxy anthraquinone', '1,4,5,8 Tetrahydroxy 2,6 dimethylanthraquinone', '1,2,4,5,6 Pentahydroxy 7 hydroxymethylanthraquino ne', 'Julichrome Q3,3', 'Verdoskyrin', '1,3,5,6,7,8 Hexahydroxy 2 methylanthraquinone', '2,3 Dihydro,5,8 dihydroxy 6 methoxy 2 hydroxymethyl 3 (2  hydroxypropyl) 1,4 naphthoquinone', 'Quinone C', 'Quinone B', '(3R, 4aS, 5R, 10aR) 5 Hydroxydihydrofusarubin A', '3 O Ethyldihydrofusarubin B', '( ) Flavoskyrin', '(3R, 4aS, 5S, 10aR) 5 Hydroxydihydrofusarubin D', '3 O Ethyldihydrofusarubin A', 'Stemphyltoxin IV', '3 O Methyldihydrofusarubin A']


def get_smiles_inchi():
    df = pd.read_csv("quinones.csv")  # load data
    data = []
    fails = []
    for num, name in enumerate(df["Quinone names"]):  # iterate over names
        while name[0] == " ":  # remove spaces at beginning
            name = name[1:]
        while name[-1] == " ":  # ...and end of name
            name = name[:-1]
        name = name.replace(" ", "-")  # replace other spaces with -

        try:
            data.append(extraction_variations(df, name, num)) #try different extractions
        except: #try to find further mistakes in joining strings
            try:
                name = name[::-1].replace("-", " ", 1)[::-1] #replace last "-" with " "
                data.append(extraction_variations(df, name, num))
            except:
                try:
                    name = name.replace("-", " ") #replace all "-" with " "
                    data.append(extraction_variations(df, name, num))
                except:
                    didit = 0
                    for index in [m.start() for m in re.finditer("-", name)]: #remove all "-" in turn and replace with nothing
                        if didit == 0:
                            try:
                                name = name[0: index:] + name[index + 1::]
                                data.append(extraction_variations(df, name, num))
                                didit = 1
                            except:
                                pass
                    if didit == 0:
                        print("0(#" + str(num) + ") Quinone " + name + " could not be extracted.")
                        fails.append(name)


    print("Failed:")
    print(fails)
    write_input_sheet(data)

def extraction_variations(dataset, name, num): #try replacing brackets
    df = dataset

    try:
        name = name.replace("(", "%28")
        name = name.replace(")", "%29")
        name = name.replace("[", "%5B")
        name = name.replace("]", "%5D")
        name = name.replace("'", "%27")
        return extraction_variations2(df, name, num)
    except:
        try:
            name = name.replace("'", "%27")
            return extraction_variations2(df, name, num)
        except:
            name = name.replace("'", "")
            return extraction_variations2(df, name, num)


def extraction_variations2(dataset, name, num):
    df = dataset

    try:
        name_noComma = name.replace(",", "_")
        returns = extract_quinones_by_name(name_noComma)  # call database function

        inchi = returns[0].replace("InChI=", "")  # remove "InChI="
        smiles = returns[1]
        print("1(#" + str(num) + ") Quinone " + name + " was extracted from the URL.")
        return [name, inchi, smiles, df["E"][num], df["Gsolv"][num]]  #return iportant information

    except: # if link extraction failed, try pubchempy
        try:
            results = pcp.get_compounds(name, 'name')
            inchi = results[0].inchi
            smiles = results[0].canonical_smiles
            print("1(#" + str(num) + ") Quinone " + name + " was extracted with pubchempy.")
            return [name, inchi, smiles, df["E"][num], df["Gsolv"][num]]
        except:
            if name[-2] == "-":
                name = name[::-1].replace("-", " ", 1)[::-1]  # replace last occurence of "-" if only one letter follows
            results = pcp.get_compounds(name, 'name')
            inchi = results[0].inchi
            smiles = results[0].canonical_smiles
            print("1(#" + str(num) + ") Quinone " + name + " was extracted with pubchempy.")
            return [name, inchi, smiles, df["E"][num], df["Gsolv"][num]]

def extract_quinones_by_name(quinone_name):
    string1 = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/"
    string3 = "/property/"
    string4 = "/TXT"
    try:
        inchi_html = urllib.request.urlopen(string1 + quinone_name + string3 + "INCHI" + string4).read()
        inchi_html2 = inchi_html.decode('UTF-8')

        smiles_html = urllib.request.urlopen(string1 + quinone_name + string3 + "CanonicalSMILES" + string4).read()
        smiles_html2 = smiles_html.decode('UTF-8')

        return [inchi_html2, smiles_html2]
    except: #see if only one is available
        try:
            inchi_html = urllib.request.urlopen(string1 + quinone_name + string3 + "INCHI" + string4).read()
            inchi_html2 = inchi_html.decode('UTF-8')
            print("ONLY INCHI OBTAINED")
            return [inchi_html2, "nan"]
        except:
            smiles_html = urllib.request.urlopen(string1 + quinone_name + string3 + "CanonicalSMILES" + string4).read()
            smiles_html2 = smiles_html.decode('UTF-8')
            print("ONLY SMILES OBTAINED")
            return ["nan", smiles_html2]


def write_input_sheet(data):
    # one with all data
    with open("full_input_sheet_with_names.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(["name", "inchi", "smiles", "E", "Gsolv"])

    for num, row in enumerate(data):
        with open("full_input_sheet_with_names.csv", "a") as csv_file:  # create file and write header
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)

    # one with only dl-data inchi
    with open("full_input_sheet_inchi.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(["inchi", "E", "Gsolv"])

    for num, row in enumerate(data):
        with open("full_input_sheet_inchi.csv", "a") as csv_file:  # create file and write header
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow([row[1], row[3], row[4]])

    # one with only dl-data smiles
    with open("full_input_sheet_smiles.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(["smiles", "E", "Gsolv"])

    for num, row in enumerate(data):
        with open("full_input_sheet_smiles.csv", "a") as csv_file:  # create file and write header
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow([row[2], row[3], row[4]])

    return