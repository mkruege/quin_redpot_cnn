from tensorflow import keras
from tensorflow.keras import layers
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.optimizers import Adagrad
import numpy as np
import plot_results

def plot_my_model():
    model = create_model((120, 80, 1), 1, [128, 256, 512], [6,4,2], [1,1,1], [0,0,0],
                         [1,1,1], [3,4,5], [1,2,2], [0,0,0], 1, "linear", 0,
                         0.1)

    from tensorflow.keras.utils import plot_model
    plot_model(model, to_file='model.png', rankdir="LR")


    return

def run(data, filters, window_sizec, stridesizec, paddingc, pooling_type, window_sizep, stridesizep, paddingp, global_pooling, outlayer_act, batch_size, batch_norm, epochs, dropout, optimizer, learning_rate, decay, loss, output=0, picklemodel=1, picklename="pickledNN/", savename="current", outvals=["E"]):

    mses = []

    for [X_train, X_test, Y_train, Y_test, scaler_y] in data:
        try:
            outputs = Y_train.shape[1]
        except:
            outputs = 1
        model = create_model((X_train.shape[1], X_train.shape[2], 1), outputs, filters, window_sizec, stridesizec, paddingc, pooling_type, window_sizep, stridesizep, paddingp, global_pooling, outlayer_act, batch_norm, dropout)
        # keras.utils.plot_model(model, show_shapes=True)

        if optimizer == "Adam":
            if decay == 1:
                opt = Adam(lr=learning_rate, decay=learning_rate / epochs)
            else:
                opt = Adam(lr = learning_rate)
        if optimizer == "AdaGrad":
            if decay == 1:
                opt = Adagrad(lr=learning_rate, decay=learning_rate/epochs)
            else:
                opt = Adagrad(lr = learning_rate)
        model.compile(loss=loss, optimizer=opt)
        #model.summary()

        X_train = np.expand_dims(X_train, axis=3)
        X_test = np.expand_dims(X_test, axis=3)

        history = model.fit(X_train, Y_train, batch_size=batch_size, epochs=epochs, verbose=0, validation_data=(X_test, Y_test))


        if picklemodel == 1:
            keras.models.save_model(model, picklename)


        Y_pred = model.predict(X_test)

        # print("epochs: ", epochs)
        # print("batch_size: ", batch_size)

        Y_pred = MinMaxScaler.inverse_transform(scaler_y, Y_pred)

        Y_test = MinMaxScaler.inverse_transform(scaler_y, Y_test)

        mses.append(mean_squared_error(Y_test, Y_pred))


    mse = sum(mses)/len(mses)
    if output==1:
        print(str(filters) + ";" + str( window_sizec) + ";" + str(stridesizec) + ";" + str(paddingc) + ";" + str(pooling_type) + ";" + str(window_sizep) + ";" + str(stridesizep) + ";" + str(paddingp) + ";" + str(global_pooling) + ";" + str(outlayer_act) + ";" + str(batch_size) + ";" + str(batch_norm) + ";" + str(epochs) + ";" + str(dropout) + ";" + str(optimizer) + ";" + str(learning_rate) + ";" + str(decay) + ";" + str(loss) + ";" + str(mse))
    #print(mse)

    plot_results.prediction_scatter(Y_test, Y_pred, savename, outvals)



    return mse




def create_model(input_shape, outputs, filters, window_sizec, stridesizec, paddingc, pooling_type, window_sizep, stridesizep, paddingp, global_pooling, outlayer_act, batch_norm, dropout):

    input = keras.Input(shape=input_shape)

    x = input

    for num, filter in enumerate(filters):
        filter = int(filter)
        if int(paddingc[num])==0:
            padding = "valid"
        else:
            padding = "same"
        x = layers.Conv2D(filter, int(window_sizec[num]), strides=int(stridesizec[num]), padding=padding)(x)
        if batch_norm == 1:
            x = layers.BatchNormalization()(x)

        x = layers.Activation("relu")(x)

        if int(paddingp[num]) == 0:
            padding = "valid"
        else:
            padding = "same"

        if pooling_type == 1:
            x = layers.MaxPooling2D(int(window_sizep[num]), strides=int(stridesizep[num]), padding=padding)(x)
        elif pooling_type == 2:
            x = layers.AveragePooling2D(int(window_sizep[num]), strides=int(stridesizep[num]), padding=padding)(x)

    if global_pooling == 1:
        x = layers.GlobalMaxPooling2D()(x)
    if global_pooling == 2:
        x = layers.GlobalAveragePooling2D()(x)

    if dropout != 0:
        x = layers.Dropout(dropout)(x)

    outputs = layers.Dense(outputs, activation=outlayer_act)(x)

    return keras.Model(input, outputs)