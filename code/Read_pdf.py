import pdftotext
import csv



def extract_quin_names():
    # read pdf file
    with open("quinones.pdf", "rb") as f:
        pdf = pdftotext.PDF(f)


    quinones = []
    values = []
    # Iterate over all the pages
    for pagenum, page in enumerate(pdf):
        if pagenum > 7:  #Quinones start at page 9
            # split text to lines
            pageinlines = page.split("\n")
            pageinwords = []
            if pagenum == 8:
                pageinlines = pageinlines[5:]
            for line in pageinlines:
                #only consider first 30 symbols of any line
                line = line[:40]
                # split remaints of lines to words
                lineinwords = line.split()
                pageinwords.append(lineinwords)


            # now find criteria to identify all quinone names
            blockedlines=0 #make counter to skip lines, where names are joined
            for linenum, line in enumerate(pageinwords):
                if len(line)!=0:
                    if blockedlines == 0: #already part of previous quinone
                        [quin, blockedlines] = testnextword("", pageinwords, linenum-1)
                        quinones.append(quin)
                    else:
                        blockedlines = blockedlines-1 #already part of previous quinone

            pageinwords2 = []
            for line in pageinlines: #get full lines in words
                lineinwords = line.split()
                pageinwords2.append(lineinwords)

            for line in pageinwords2:
                if len(line) > 6: #filter lines with few words
                    print(line)
                    for num, word in enumerate(line):
                        if word[:2] == "1." or word[:2] == "0." or word[:3] == "-0." or word[:3]=="-1." or "#####" in word: #identify first value
                            if len(values)==0:
                                values.append([float(word), float(line[num+1])])
                            else:
                                try:
                                    if float(word) <= values[-1][0]: #all first numbers are smaller or equal previous one
                                        values.append([float(word), float(line[num+1])])
                                        print(word, line[num+1])
                                except:
                                    try:
                                        if "#####" in word:  # deal with #### in first val
                                            values.append([word, float(line[num + 1])])
                                            print(word, line[num + 1])

                                        elif "#####" in values[-1][0]: #deal with ### in second val
                                            values.append([float(word), float(line[num + 1])])
                                            print(word, line[num + 1])
                                    except:
                                        pass #don't add any other exceptions


    quinones = filter_quinones(quinones)

    #print_quinones(quinones, values)
    print_values(values)


def testnextword(quin, page, pospage):
    'checks if the following words are part of name'

    strike=0
    go = 1
    blockedlines=-1
    while go: #iterate over following lines
        pospage = pospage + 1
        posline = -1
        try: #catch exception: last word in line
            if len(page[pospage])==0: #go over max. 3 empty lines before starting new quinone name
                if strike==2:
                    break
                else:
                    strike=strike+1
                    continue
        except:
            break

        blockedlines = blockedlines+1
        go1 = 1

        while go1:
            posline = posline + 1

            if len(page[pospage])<=posline:
                go1 = 0
                continue

            next = page[pospage][posline]

            # convert letters to lower case, check if there are lower-case letters -> checks if there are any letters in word
            # bc quinone names always followed by ID (always >3 numbers)
            next = next.lower()
            if next.islower() or len(next) < 3:
                # merge with or without space seperator
                if page[pospage][posline][0] == "-":
                    quin = "".join([quin, page[pospage][posline]])
                elif len(quin) > 0 and quin[-1] == "-":
                    quin = "".join([quin, page[pospage][posline]])
                else:
                    quin = " ".join([quin, page[pospage][posline]])
            else:
                go1 = 0 #must be ID, finish loop

    return [quin, blockedlines]


def filter_quinones(quinones):
    newquinones = []
    for num, quinone in enumerate(quinones):
        if "derivative" in quinone or "IFO" in quinone or "HO (S) (S) OH OH O OH" in quinone: #reject certain words as names
            pass
        elif not quinones[num-1][-1]=="-" and len(quinone) > 3 and not quinone[0] == "-": #previous quinone name does not end with "-", current quin name does not start with "-" and name is longer than 3 letters -> append to list
            newquinones.append(quinone)
        else:
            newquinones[-1] = "".join([newquinones[-1], quinone]) #otherwise append to last name added to list
    return newquinones

def print_quinones(quinones, values):
    with open("quinones.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(["Quinone names", "E", "Gsolv"])

    for num, quinone in enumerate(quinones):
        with open("quinones.csv", "a") as csv_file:  # create file and write header
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow([quinone, values[num][0], values[num][1]])


def print_values(values):
    with open("values.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(["E", "Gsolv"])

    for num, value in enumerate(values):
        with open("values.csv", "a") as csv_file:  # create file and write header
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(value)