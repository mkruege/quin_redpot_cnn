try:
    from rdkit import Chem
except:
    pass

import numpy as np
import pandas as pd
import csv


def get_x_random_smiles_alternations_from_csv(smiles, x, rsmil_csv):
    "Uses csv file to obtain RandomSMILES augmentations."
    random_smiles = pd.read_csv(rsmil_csv) #read csv with saved randomSMILES
    rands = list(random_smiles.loc[random_smiles['smiles'] == smiles]["random_smiles"]) #get the ones associated to the given origin_SMILES
    return rands[:x] #return first x randomSMILES

def get_x_random_smiles_alternations(smiles, x):
    "Returns a list of augmentations for a canonical SMILES input string, using the RandomSmiles algorithm. Length is x or, if less, maximum number of augmentations possible."

    mol = Chem.MolFromSmiles(smiles)

    result = 0
    step = 0
    try:
        while result == 0: #while there are more different augmentations without obtaining one twice
            step = step+1
            alt = Chem.MolToRandomSmilesVect(mol, step) #call random smiles algorithm
            result = checkIfDuplicates_2(alt) #check if all elements are unique
            if step == x:
                return alt #return if x augmentations are collected

        #print("Only " + str(step) + " random smiles obtained.")
        return alt[:-1]
    except:
        print("No alternations obtained for molecule:")
        print(smiles)
        return []

def write_x_random_smiles_to_csv(filename, smilesfile, x):
    "Saves specified number of RandomSMILES to csv file to be used without RDKit."
    smiles = pd.read_csv(smilesfile)["smiles"]
    for smil in smiles:
        mol = Chem.MolFromSmiles(smil) #transform to RDKit molecule

        result = 0
        step = 0
        write = 0
        try:
            while result == 0:
                step = step+1
                alt = Chem.MolToRandomSmilesVect(mol, step) #get randomSMILES list
                result = checkIfDuplicates_2(alt) #see if new element is a duplicate
                if result == 1:
                    alt = alt[:-1] #remove last element where duplicate occured
                if step == x:
                    try:  # check if file is there
                        pd.read_csv(filename)
                    except:  # if not, make new file with header
                        header = ["smiles", "random_smiles"]
                        with open(filename, 'w+') as csv_file:
                            writer = csv.writer(csv_file)
                            writer.writerow(header)

                    with open(filename, "a") as csv_file:  # ... and write data when step is reached
                        writer = csv.writer(csv_file)
                        print(str(len(alt)))
                        #print("Obtained " + str(len(alt)) + " augmentations for molecule:")
                        #print(smil)
                        for rsmil in alt:
                            writer.writerow([smil, rsmil])
                    write = 1 # no write again after loop terminates
                    result = 1 # terminate loop
            if write == 0:
                with open(filename, "a") as csv_file:  # ... and write data when duplicate occurs
                    writer = csv.writer(csv_file)
                    #print("Obtained " + str(len(alt)) + " augmentations for molecule:")
                    #print(smil)
                    print(str(len(alt)))
                    for rsmil in alt:
                        writer.writerow([smil, rsmil])

        except:
            print("ERROR: No alternations obtained for molecule:")
            print(smil)

    return

def checkIfDuplicates_2(listOfElems):
    ''' Check if given list contains any duplicates '''
    setOfElems = set()
    for elem in listOfElems:
        if elem in setOfElems:
            return True
        else:
            setOfElems.add(elem)
    return False


def get_x_shifts(quin, shifts, specs):
    quins = []
    for i in range(shifts):  # make 20 iterations for each string if possible
        if quin[-1][specs[2]] == 1:  # empty character at last position
            quin=np.concatenate([[quin[-1]], quin[:-1]])  # bring last empty character to first position
            quins.append(quin)
        else:
            # print("String #" + str(quinnum) + " was too long for enough alternations")
            pass
    return quins