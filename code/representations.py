from openbabel import pybel
import pandas as pd
import coulomb_matrix_generator
from rdkit import Chem
from rdkit.Chem import MACCSkeys
from rdkit.Chem import AllChem
from rdkit import DataStructs
import numpy as np
from ase import Atoms
from dscribe.descriptors import MBTR


def read_ase_from_file(input_file):
    "Reads input file, computes atom coordinates and transforms into ase.Atoms structure. Returns list of molecules."
    smiles = read_smiles_from_file(input_file)
    xyzs = []
    mols = []
    uniques = []
    for num, smile in enumerate(smiles):
        if num%10 == 0:
            print("Obtained coordinates for " + str(num) + " of " + str(len(smiles)) + " molecules.")
        [mol, xyz] = coulomb_matrix_generator.generate_xyz(smile)
        xyzs.append(xyz)
        mols.append(mol)
        for element in mol:
            if element not in uniques:
                uniques.append(element)
    atoms = []
    for num, mol in enumerate(mols):
        atom = Atoms(mol, positions=xyzs[num])
        # atom.calc = NWChem(xc='PBE')
        # opt = BFGS(atom)
        # opt.run(fmax=0.02)
        atoms.append(atom)

    if "r" in uniques:
        uniques.remove("r")
        uniques.remove("B")
        uniques.append("Br")
    if "l" in uniques:
        uniques.remove("l")
        uniques.append("Cl")
    return [atoms, uniques]

def read_smiles_from_file(input_file):
    "Reads input file and return list of SMILES."
    df = pd.read_csv(input_file)
    smiles = df['smiles'].tolist()
    return smiles

def read_mols_from_file(input_file):
    "Reads input file and transforms to pybel molecules. Returns list of molecules."
    df = pd.read_csv(input_file)
    smiles = df['smiles'].tolist()
    mols = [pybel.readstring("smi", x) for x in smiles]
    return mols

def read_rdkit_mols_from_file(input_file):
    "Read input file and tranform to rdkit-molecules. Returns list of molecules."
    mols = []
    df = pd.read_csv(input_file)
    smiles = df['smiles'].tolist()
    for smil in smiles:
        mols.append(Chem.MolFromSmiles(smil))
    return mols

def transform_to_coulumb_matrix(input_file):
    "Returns coloumb matrixes for molecules in input file (pandas data frame)."
    smiles = read_smiles_from_file(input_file)
    [matrices, dfs] = coulomb_matrix_generator.generate_cm(smiles)
    return dfs


def transform_to_mbtr(input_file):
    "Returns many-body tensor representations for molecules in input file."
    [mols, uniques] = read_ase_from_file(input_file)
    mbtrs = []
    # Setup
    mbtr = MBTR(
        species=uniques,
        k1={
            "geometry": {"function": "atomic_number"},
            "grid": {"min": 0, "max": 8, "n": 100, "sigma": 0.1},
        },
        k2={
            "geometry": {"function": "inverse_distance"},
            "grid": {"min": 0, "max": 1, "n": 100, "sigma": 0.1},
            "weighting": {"function": "exp", "scale": 0.5, "threshold": 1e-3},
        },
        k3={
            "geometry": {"function": "cosine"},
            "grid": {"min": -1, "max": 1, "n": 100, "sigma": 0.1},
            "weighting": {"function": "exp", "scale": 0.5, "threshold": 1e-3},
        },
        periodic=False,
        normalization="l2_each",
        sparse=0,
    )
    for num, mol in enumerate(mols):
        if num%10 == 0:
            print("Obtained MBT representation for " + str(num) + " of " + str(len(mols)) + " molecules.")
        mbtrs.append(mbtr.create(mol))

    return mbtrs


def transform_to_fingerprint(input_file):
    "Returns fingerprints for molecules in input file."
    mols = read_mols_from_file(input_file)
    fps = [x.calcfp() for x in mols]
    return fps


def transform_to_maccs_keys(input_file):
    "Returns MACCS keys for molecules in input file."
    mols = read_rdkit_mols_from_file(input_file)
    fps = [DataStructs.cDataStructs.BitVectToText(MACCSkeys.GenMACCSKeys(x)) for x in mols]
    return fps


def transform_to_morgan_fingerprint(input_file, radius=2):
    "Returns morgan fingerprints for molecules in input file."
    mols = read_rdkit_mols_from_file(input_file)
    fps = [DataStructs.cDataStructs.BitVectToText(AllChem.GetMorganFingerprintAsBitVect(x, radius)) for x in mols]
    return fps

def transform_to_topological_fingerprint(input_file):
    "Returns topological fingerprints for molecules in input file."
    mols = read_rdkit_mols_from_file(input_file)
    fps = [DataStructs.cDataStructs.BitVectToText(Chem.RDKFingerprint(x)) for x in mols]
    return fps