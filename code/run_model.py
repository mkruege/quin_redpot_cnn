import prepare_2d_nn_input
import test_model
from sys import argv
import argparse
import pandas as pd
import csv
import timeit



model_dir = './'
if argv[0].find('/') >= 0:
	model_dir = argv[0][: - argv[0][::-1].find('/')]


#parse arguments
parser = argparse.ArgumentParser(description='Running defined CNN models for tuning.')
parser.add_argument('--smiles_data', '-d', type=str, default='all_new.csv', help='Path to bed file with SMILES and chem. property data,')
parser.add_argument('--outdir', '-od', type=str, default='./', help='Output directory. Default: "./"')
parser.add_argument('--outval', '-ov', type=str, default='E', help='List of output value(s)')
parser.add_argument('--filters', '-f', type=str, default='[16, 16]', help='List of filter numbers for each conv_layer. Length of list determines number of layers.')
parser.add_argument('--window_sizec', '-wsc', type=str, default='[3, 3]', help='List of window sizes for each conv layer')
parser.add_argument('--stridesizec', '-ssc', type=str, default='[3, 3]', help='List of stridesizes for each conv layer')
parser.add_argument('--paddingc', '-pc', type=str, default='[0, 0]', help='List of padding options for each conv layer')
parser.add_argument('--pooling_type', '-pt', type=str, default='[0, 0]', help='List of pooling types for each pooling layer, 0=none, 1=max, 2=avg')
parser.add_argument('--window_sizep', '-wsp', type=str, default='[3, 3]', help='List of window sizes for each pooling layer')
parser.add_argument('--stridesizep', '-ssp', type=str, default='[3, 3]', help='List of stridesizes for each pooling layer')
parser.add_argument('--paddingp', '-pp', type=str, default='[0, 0]', help='List of padding options for each pooling layer')
parser.add_argument('--global_pooling', '-gp', type=int, choices=[1, 2], default=1, help='No global pooling or 1=max_pooling or 2=avg_pooling.')
parser.add_argument('--outlayer_act', '-oa', type=str, default='linear', choices=['linear', 'relu'], help='Output layer activation')
parser.add_argument('--batch_size', '-bs', type=str, default='[16, 32, 64, 128]', help='batch_size')
parser.add_argument('--batch_norm', '-bn', type=int, default=0, choices=[0, 1], help='No batch normalization or after Conv layer')
parser.add_argument('--epochs', '-ep', type=str, default='[24, 32, 50]', help='Number of training epochs')
parser.add_argument('--dropout', '-do', type=float, default=0, help='No dropout or specified dropout before output')
parser.add_argument('--optimizer', '-op', type=str, default='Adam', choices=['Adam', 'AdaGrad'], help='Optimizer')
parser.add_argument('--learning_rate', '-lr', type=float, default=0.001, help='Learning rate')
parser.add_argument('--decay', '-dc', type=int, default=0, choices=[0, 1], help='decay or no decay for learnging rate')
parser.add_argument('--loss', '-lo', type=str, default='mse', choices=['mse', 'cross_entropy'], help='Loss function')
parser.add_argument('--k_rands', '-kr', type=int, default=5, help='k_randomSMILES for augmentation method')
parser.add_argument('--n_rands', '-nr', type=int, default=0, help='n_randomSMILES for augmentation method')
parser.add_argument('--k_shifts', '-ks', type=int, default=5, help='k_shiftSMILES for augmentation method')
parser.add_argument('--n_shifts', '-ns', type=int, default=0, help='n_shiftSMILES for augmentation method')
parser.add_argument('--extend_frame', '-ef', type=int, default=10, help='extend_frame for augmentation method')


# parse and pre-process command line arguments
args = parser.parse_args()

def parselist(input):
	return list(map(float, input.strip('[]').split(',')))

args.smiles_data = args.smiles_data.split(',')
args.outval = args.outval.split(',')
args.filters = parselist(args.filters)
args.window_sizec = parselist(args.window_sizec)
args.stridesizec = parselist(args.stridesizec)
args.paddingc = parselist(args.paddingc)
args.pooling_type = parselist(args.pooling_type)
args.window_sizep = parselist(args.window_sizep)
args.stridesizep = parselist(args.stridesizep)
args.paddingp = parselist(args.paddingp)
args.batch_size = parselist(args.batch_size)
args.epochs = parselist(args.epochs)


# make header for csv and list of params
header = ["outval", "filters", "window_sizeC", "stridesizeC", "paddingC", "poolingType", "window_sizeP", "stridesizeP", "paddingP", "global_pooling", "outlayer_act", "batch_norm", "dropout", "optimizer", "learning_rate", "decay", "loss", "k_rands", "n_rands", "k_shifts", "n_shifts", "extend_frame", "epochs", "batch_size", "mse", "trainingtime"]
row = [args.outval, args.filters, args.window_sizec, args.stridesizec, args.paddingc, args.pooling_type, args.window_sizep, args.stridesizep, args.paddingp, args.global_pooling, args.outlayer_act, args.batch_norm, args.dropout, args.optimizer, args.learning_rate, args.decay, args.loss, args.k_rands, args.n_rands, args.k_shifts, args.n_shifts, args.extend_frame]

stop = 0
try: #see if there is a model_runs file and check if identical run has been logged
	previous_runs = pd.read_csv("model_runs.csv")

except: #make new model_runs file
	with open(args.outdir + "model_runs.csv", "w+") as csv_file:  # create file and write header
		writer = csv.writer(csv_file, delimiter=",")
		writer.writerow(header)


#preprocess input data
nn_input = prepare_2d_nn_input.prepare_input_from_original_smiles(args.smiles_data, args.outval, outvalnames=[], n_random_smiles=args.n_rands, n_shifts=args.n_shifts, k_random_smiles=args.k_rands, k_shifts=args.k_shifts, extend_frame=args.extend_frame, rands_nums_histo=0)

mse = 100000000
for epoch in args.epochs:
	for bs in args.batch_size:


		#run_model

		start = timeit.timeit() #time model training time

		mse_new = test_model.run(nn_input, args.filters, args.window_sizec, args.stridesizec, args.paddingc, args.pooling_type, args.window_sizep, args.stridesizep, args.paddingp, args.global_pooling, args.outlayer_act, int(bs), args.batch_norm, int(epoch), args.dropout, args.optimizer, args.learning_rate, args.decay, args.loss)

		end = timeit.timeit()

		trainingtime = end-start

		if mse_new < mse:
			mse = mse_new
			traintime = trainingtime
			ep = epoch
			batch = bs

row.append(ep)
row.append(batch)
row.append(mse)
row.append(traintime) #write results into csv
with open(args.outdir + "model_runs.csv", "a") as csv_file:  # write new row
	writer = csv.writer(csv_file, delimiter=",")
	writer.writerow(row)



