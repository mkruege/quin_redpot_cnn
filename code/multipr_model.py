from multiprocessing import Pool
import pandas as pd
import prepare_2d_nn_input
import test_model
from functools import partial






def exec_multipr_hpt(input_data, outval, hpt_sheet, k_rands, n_rands, k_shifts, n_shifts, extend_frame, crossval, rsmilfile=[]):
    nn_input = prepare_2d_nn_input.prepare_input_from_original_smiles(input_data, outval, outvalnames=[],
                                                                      n_random_smiles=n_rands,
                                                                      n_shifts=n_shifts,
                                                                      k_random_smiles=k_rands,
                                                                      k_shifts=k_shifts,
                                                                      extend_frame=extend_frame,
                                                                      rands_nums_histo=0, crossval=crossval, rsmilfile=rsmilfile)
    multi_run_hpt_sheet(hpt_sheet, nn_input)


def multi_run_hpt_sheet(sheet, nn_input):

    hpt = pd.read_csv(sheet)


    procs = hpt.shape[0]  # Number of processes to create



    print("Begin multiprocessing")
    a_args = [i for i in range(procs)]
    second_arg = nn_input
    third_arg = hpt
    pool = Pool()
    pool.map(partial(pool_jobs, nn_input=second_arg, hpt=third_arg), a_args)

    # with poolcontext(processes=cores) as pool:
    #     ret = pool.map(pool_jobs_unpack, [(hpt, nn_input, i) for i in range(procs)])



    return

def pool_jobs(i, nn_input, hpt):
    test_model.run(nn_input, hpt['filters'][i].split(","), hpt['window_sizec'][i].split(","),
     hpt['stridesizec'][i].split(","), hpt['paddingc'][i].split(","),
     hpt['pooling_type'][i].split(","), hpt['window_sizep'][i].split(","),
     hpt['stridesizep'][i].split(","), hpt['paddingp'][i].split(","),
     hpt['global_pooling'][i], hpt['outlayer_act'][i],
     hpt['batch_size'][i], hpt['batch_norm'][i], hpt['epochs'][i],
     hpt['dropout'][i], hpt['optimizer'][i], hpt['learning_rate'][i],
     hpt['decay'][i], hpt['loss'][i], output=1)
    return



