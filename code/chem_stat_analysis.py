import pandas as pd
import numpy as np
import csv
from rdkit import Chem
from itertools import permutations
import scipy.stats
import plot_results

def perform_full_analysis_smiles(dataset, savename, properties, remove_by_fgroup=[]):
    "Performs chemical analysis for SMILES representations and statistical analysis for given properties."
    df = pd.read_csv(dataset)
    df = df.dropna(axis=0)

    for group in remove_by_fgroup:
        for smil in df.smiles:
            if Chem.MolFromSmiles(smil).HasSubstructMatch(Chem.MolFromSmarts(group)):
                df = df[df.smiles != smil]
    df = df.reset_index(drop=True)

    chems = chemical_analysis(df["smiles"]) #pass smiles representations
    write_chems_to_csv(df, chems, savename, properties)
    make_correlation_heatmap(savename)

    for prop in properties:
        df_chem = pd.read_csv(savename + "_chem.csv")
        df_chem = df_chem.dropna(axis=0)
        ret = statistical_analysis(df_chem, savename, prop)
        stats=ret[1]
        statsprops=ret[0]
        write_stats_to_csv(stats, statsprops, savename, prop)
    return

def make_correlation_heatmap(savename):
    df = pd.read_csv(savename + "_chem_heatmap.csv")
    corr = df.corr(method="pearson")
    plot_results.corr_heatmap(corr, savename, "pearson")

    corr2 = df.corr(method="spearman")
    plot_results.corr_heatmap(corr2, savename, "spearman")

    #corr3 = df.corr(method="kendall")
    #plot_results.corr_heatmap(corr3, savename, "kendall")

    #columns = ["n_atoms", "conj_C", "keto_dist", "n_keto", "n_single_bonds", "n_double_bonds", "n_triple_bonds", "n_all_bonds", "n_C", "n_O", "n_N", "n_S", "n_Si", "n_P", "n_F", "n_Cl", "n_Br", "E", "Gsolv"]
    columns = list(df)
    corr = df[columns].corr("pearson")
    corr = pd.melt(corr.reset_index(),
                   id_vars='index')  # Unpivot the dataframe, so we can get pair of arrays for x and y
    corr.columns = ['x', 'y', 'value']
    plot_results.heatmap2(x=corr['x'], y=corr['y'], size=corr['value'].abs(), color = corr["value"], savename=savename, corrtype="pearson")

    # columns = ["n_atoms", "conj_C", "keto_dist", "n_keto", "n_single_bonds", "n_double_bonds", "n_triple_bonds",
    #            "n_all_bonds", "n_C", "n_O", "n_N", "n_S", "n_Si", "n_P", "n_F", "n_Cl", "n_Br", "E", "Gsolv"]
    corr = df[columns].corr("spearman")
    corr = pd.melt(corr.reset_index(),
                   id_vars='index')  # Unpivot the dataframe, so we can get pair of arrays for x and y
    corr.columns = ['x', 'y', 'value']
    plot_results.heatmap2(x=corr['x'], y=corr['y'], size=corr['value'].abs(), color = corr["value"], savename=savename, corrtype="spearman")

    return

def write_stats_to_csv(stats, statprops, savename, prop):
    header = ["property", "pearsons_r", "pearson_p_val", "spearman_r", "spearman_p_val", "kendalltau", "kendalltau_p_val"]
    rows = []
    for num, property in enumerate(statprops):
        row = [property]
        for stat in stats[num]:
            for val in stat:
                row.append(val)
        rows.append(row)

    with open(savename + "_stat_" + prop + ".csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for row in rows:
        with open(savename + "_stat_" + prop + ".csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)

    return

def write_chems_to_csv(df, chems, savename, properties):
    "Collects all chemical analysis data and writes to csv file"
    header = ["smiles", "n_atoms", "n_rings", "ring_shape", "ring_strucs", "conjugated_atoms", "conjugated_bonds", "conj_C", "keto_dist", "n_keto", "n_s_bonds", "n_d_bonds", "n_t_bonds", "n_a_bonds", "n_all_bonds", "alkyl_g", "hydroxyl_g", "caboxyle_g", "amine_g", "amide_g", "nitrile_g", "sulfide_g", "sulfonic_g"]

    for i in chems[2]: #unique elements
        header.append("n_" + i)
    for i in properties:
        header.append(i)
    rows = []
    rows2 = []
    skipped = 0

    # header2 for correlation heat map
    header2 = ["n_atoms", "n_rings", "conj_C", "keto_dist", "n_keto", "n_s_bonds", "n_d_bonds", "n_t_bonds",
               "n_a_bonds", "n_all_bonds", "alkyl_g", "hydroxyl_g", "caboxyle_g", "amine_g", "amide_g", "nitrile_g", "sulfide_g", "sulfonic_g"]

    for num, smil in enumerate(df["smiles"]):
        #get all chemical properties for csv sheet
        row = [smil, chems[0][num], chems[1][num], chems[4][num], chems[5][num], chems[6][num], chems[7][num], chems[8][num], chems[9][num], chems[10][num], chems[11][num], chems[12][num], chems[13][num], chems[14][num], chems[15][num], chems[16][num], chems[17][num], chems[18][num], chems[19][num], chems[20][num], chems[21][num], chems[22][num], chems[23][num]]

        row2 = [] #for heat map - only simple values
        for head in header2:
            row2.append(row[header.index(head)])

        for i in range(len(chems[2])): # unique element counts
            row.append(chems[3][num][i])
            row2.append(chems[3][num][i])
        for prop in properties: #original properties
            try:
                row.append(df[prop][num+skipped])
                row2.append(df[prop][num+skipped])
            except:
                skipped = skipped+1
                row.append(df[prop][num+skipped])
                row2.append(df[prop][num+skipped])
        rows.append(row)
        rows2.append(row2)

    #append elements and properties to header2 too
    for i in chems[2]: #unique elements
        header2.append("n_" + i)
    for i in properties:
        header2.append(i)

    with open(savename + "_chem.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for row in rows:
        with open(savename + "_chem.csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)

    with open(savename + "_chem_heatmap.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header2)

    for row in rows2:
        with open(savename + "_chem_heatmap.csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)

    return

def chemical_analysis(smiles):
    "Performs chemical analysis for SMILES representations."

    #read smiles strings and transform to rdkit molecules
    mols = []
    for smil in smiles:
        mols.append(Chem.MolFromSmiles(smil))

    #ANALYSIS

    #init lists
    unique_elements = []
    n_atoms = []
    n_rings = []
    n_elements = []
    ring_shapes = []
    ringstrucs = []
    conj_b = []
    conj_a = []
    conj_C = []
    keto_dist = []
    n_keto = []
    sing_bond = []
    doub_bond = []
    trip_bond = []
    arom_bond = []
    all_bonds = []

    alkyl_g = []
    hydroxyl_g = []
    caboxyle_g = []
    amine_g = []
    amide_g = []
    nitrile_g = []
    sulfide_g = []
    sulfonic_g = []

    for mol in mols:

        #init sub-lists
        mol_elements = [0]*len(unique_elements)
        ringshape = []

        # count all atoms
        n_atoms.append(mol.GetNumAtoms())

        #count all elements
        for atom in mol.GetAtoms():
            elem = atom.GetSymbol()
            if elem in unique_elements: #already in element collection list
                mol_elements[unique_elements.index(elem)]+=1
            else:
                unique_elements.append(elem) #new element for element collection
                mol_elements.append(1) #count 1 for new element in this molecule
        n_elements.append(mol_elements)


        #ring information -> sizes of existing ring formations
        ri = mol.GetRingInfo()
        ringstruc = ri.AtomRings()
        ringstruc2 = ri.BondRings()
        ringstrucs.append(ringstruc)
        for ring in ringstruc:
            ringshape.append(len(ring))
        ring_shapes.append(ringshape)


        #conjugated structure
        cob = get_conjugated_system(mol, ringstruc2)
        conj_b.append(cob)
        coa = []
        for conjring in cob:
            coa.append(ri.AtomRings()[ri.BondRings().index(conjring)])
        conj_a.append(coa)

        #count C atoms in conjugated structure
        uniq_conjc = []
        for conjring in coa:
            for at in conjring:
                if at not in uniq_conjc:
                    uniq_conjc.append(at)
        coc = len(uniq_conjc)
        conj_C.append(coc)

        #get relative distance of keto groups (excluding keto-C)
        keto_dist.append(get_keto_distance(mol, coa))

        ketos = mol.GetSubstructMatches(Chem.MolFromSmiles("C(=O)"))  # get keto groups
        n_keto.append(len(ketos))

        n_rings.append(len(ringshape))

        #get other substructures (functional groups)
        alkyl_g.append(len(mol.GetSubstructMatches(Chem.MolFromSmarts("[CX4]"))))
        hydroxyl_g.append(len(mol.GetSubstructMatches(Chem.MolFromSmarts("[OX2H]"))))
        caboxyle_g.append(len(mol.GetSubstructMatches(Chem.MolFromSmarts("[CX3](=[OX1])O"))))
        amine_g.append(len(mol.GetSubstructMatches(Chem.MolFromSmarts("[NX3;H2,H1;!$(NC=O)]"))))
        amide_g.append(len(mol.GetSubstructMatches(Chem.MolFromSmarts("[OX1]=CN"))))
        nitrile_g.append(len(mol.GetSubstructMatches(Chem.MolFromSmarts("[NX1]#[CX2]"))))
        sulfide_g.append(len(mol.GetSubstructMatches(Chem.MolFromSmarts("[#16X2H0][!#16]"))))
        sulfonic_g.append(len(mol.GetSubstructMatches(Chem.MolFromSmarts("[$([#16X4](=[OX1])(=[OX1])([#6])[OX2H,OX1H0-]),$([#16X4+2]([OX1-])([OX1-])([#6])[OX2H,OX1H0-])]"))))

        #number of single/double/triple bonds
        one = 0
        two = 0
        three = 0
        arom = 0
        bonds = 0
        for bond in mol.GetBonds():
            bonds = bonds + 1
            if bond.GetBondType() == Chem.rdchem.BondType.SINGLE:
                one = one+1
            elif bond.GetBondType() == Chem.rdchem.BondType.DOUBLE:
                two = two+1
            elif bond.GetBondType() == Chem.rdchem.BondType.TRIPLE:
                three = three+1
            elif bond.GetBondType() == Chem.rdchem.BondType.AROMATIC:
                arom = arom+1
            else:
                print(bond.GetBondType())
        sing_bond.append(one)
        doub_bond.append(two)
        trip_bond.append(three)
        arom_bond.append(arom)
        all_bonds.append(bonds)



    #fill up lists of element numbers to equal length
    n_unique_elements = len(n_elements[-1])
    for molel in n_elements:
        while len(molel)<n_unique_elements:
            molel.append(0)




    funcgroups = [alkyl_g, hydroxyl_g, caboxyle_g, amine_g, amide_g, nitrile_g, sulfide_g, sulfonic_g]
    funcgroupnames = ["alkyl_g", "hydroxyl_g", "caboxyle_g", "amine_g", "amide_g", "nitrile_g", "sulfide_g", "sulfonic_g"]
    funcgroupnums = []
    for var, list in enumerate(funcgroups):
        funcgroupnums.append(0)
        for elem in list:
            funcgroupnums[-1] = funcgroupnums[-1]+elem
    for num, name in enumerate(funcgroupnames):
        print("Overall number of functional group " + name + " is " + str(funcgroupnums[num]))


    return [n_atoms, n_rings, unique_elements, n_elements, ring_shapes, ringstrucs, conj_a, conj_b, conj_C, keto_dist, n_keto, sing_bond, doub_bond, trip_bond, arom_bond, all_bonds, alkyl_g, hydroxyl_g, caboxyle_g, amine_g, amide_g, nitrile_g, sulfide_g, sulfonic_g]


def get_conjugated_system(quin, ringstruc):
    "Returns the conjugated ring structures of a given ringstruc of a quinone."
    conj_bond_system = []

    for ringnum, ring in enumerate(ringstruc):  # check if keto group atom is in any ring
        conj = 1
        for bond in ring:
            if quin.GetBondWithIdx(bond).GetIsConjugated() == 0:
                conj = 0
        if conj == 1:
            conj_bond_system.append(ring)

    return conj_bond_system

def get_keto_distance(mol, coa):
    "Returns the shortest distance between any two keto groups located in the conjugated system."
    keto_dist = []
    ketogroups = mol.GetSubstructMatches(Chem.MolFromSmiles("C(=O)"))  # get keto groups
    ketoslist = list(ketogroups)
    permut = [list(p) for p in permutations(ketoslist, 2)] #get permutations of keto groups of len(2)
    for ketos in permut: #now find the shortest distance for any two keto groups
        try:
            success = 0
            for ring in coa:
                if ketos[0][0] in ring and ketos[1][0] in ring:  #if both keto groups are in the same ring -> easy calculation by index
                    kdist1 = abs(ring.index(ketos[0][0]) - ring.index(ketos[1][0])) - 1
                    kdist2 = len(ring) - kdist1 - 2
                    keto_dist.append(min([kdist1, kdist2]))
                    success = 1

            if success == 0:  # keto groups spread across multiple rings - more difficult distance calculation
                kc = []  # keto carbon atoms
                for keto in ketos:
                    kc.append(keto[0])
                rings = []  # rings with keto carbon atoms
                otherrings = []  # further conjugated rings
                for ring in coa:
                    if kc[0] in ring or kc[1] in ring:
                        rings.append(ring)
                    else:
                        otherrings.append(ring)
                common = []  # atoms in both rings
                for atom in rings[0]:
                    if atom in rings[1]:
                        common.append(atom)
                kdistges = 100
                if len(common) > 0:  # there are common atoms in both rings with keto groups
                    for com in common:
                        kdist1 = abs(rings[0].index(kc[0]) - rings[0].index(com)) - 1
                        kdist2 = len(rings[0]) - kdist1 - 2
                        kdistring1 = min([kdist1, kdist2])

                        kdist1 = abs(rings[1].index(kc[1]) - rings[1].index(com)) - 1
                        kdist2 = len(rings[1]) - kdist1 - 2
                        kdistring2 = min([kdist1, kdist2])

                        if (kdistring1 + kdistring2 + 1) < kdistges:  # calculate distance over common atom
                            kdistges = (kdistring1 + kdistring2 + 1)

                    keto_dist.append(kdistges)

                else:  # rings with keto groups not adjected
                    seccommon0 = []  # find common atoms of both rings with one other ring
                    seccommon1 = []
                    relothrings = []
                    for atom in rings[0]:
                        for ori in otherrings:
                            if atom in ori:
                                seccommon0.append(atom)  # collect atoms that can be used as path
                                if ori not in relothrings:
                                    relothrings.append(ori)
                    for atom2 in rings[1]:
                        for ori in otherrings:
                            if atom2 in ori:
                                seccommon1.append(atom2)
                                if ori not in relothrings:
                                    relothrings.append(ori)

                    for com0 in seccommon0:  # try all different paths and get shortest one
                        for com1 in seccommon1:
                            kdist1 = abs(rings[0].index(kc[0]) - rings[0].index(com0)) - 1
                            kdist2 = len(rings[0]) - kdist1 - 2
                            kdistring1 = min([kdist1, kdist2])

                            kdist1 = abs(relothrings[0].index(com0) - relothrings[0].index(com1)) - 1
                            kdist2 = len(relothrings[0]) - kdist1 - 2
                            kdistring2 = min([kdist1, kdist2])

                            kdist1 = abs(rings[1].index(kc[1]) - rings[1].index(com1)) - 1
                            kdist2 = len(rings[1]) - kdist1 - 2
                            kdistring3 = min([kdist1, kdist2])

                            if (kdistring1 + kdistring2 + 1) < kdistges:
                                kdistges = (kdistring1 + kdistring2 + kdistring3 + 2)

                    keto_dist.append(kdistges)
        except:
            pass #skips this pair of keto groups, if e.g.keto group not in conjugated system
    if len(keto_dist) > 0:
        return min(keto_dist)
    else:
        print("No keto distance obtained for molecule " + Chem.MolToSmiles(mol))
        return 0


def statistical_analysis(chem_dataset, savename, property):
    "Returns relevant ratios of chemical properties."
    prop = chem_dataset[property]
    statprops = []
    stats = []
    for property1 in ["n_atoms", "n_rings", "conj_C", "keto_dist", "n_keto", "n_s_bonds", "n_d_bonds", "n_t_bonds", "n_a_bonds", "n_all_bonds", "n_O", "n_C"]:
        prop1 = chem_dataset[property1]
        statprops.append(property1)
        stats.append(simple_stat(savename, property1, prop1, property, prop))


    for ratio in [["n_C", "n_O"], ["n_d_bonds", "n_all_bonds"], ["conj_C", "n_C"]]:
        prop1 = []
        for num, i in enumerate(chem_dataset[ratio[0]]):
            if chem_dataset[ratio[1]][num]!=0:
                prop1.append(i/chem_dataset[ratio[1]][num])
            else:
                prop1.append(0)
        statprops.append(ratio[0] + "/" + ratio[1])
        stats.append(simple_stat(savename, ratio[0] + "/" + ratio[1], prop1, property, prop))


    return [statprops, stats]

def simple_stat(savename, property1, prop1, property, prop, threshold=0.05, threshold_type="lin"):
    "Tests all chemical properties regarding correlation with the specified property, plots and prints if threshold is not exceeded."
    cor_lin = scipy.stats.pearsonr(prop1, prop)
    cor_rank = scipy.stats.spearmanr(prop1, prop)
    cor_rank2 = scipy.stats.kendalltau(prop1, prop)

    if "rank" in threshold_type:
        thres = cor_rank
    elif "lin" in threshold_type:
        thres = cor_lin

    if thres[1] < threshold:
        # print("----------------------------------------------------------------")
        # print("Correlation between " + property1 + " and " + property + ":")
        # print("----------------------------------------------------------------")
        # print("pearson r: " + str(cor_lin[0]))
        # print("p: " + str(cor_lin[1]))
        # print("")
        # print("Spearman r: " + str(cor_rank[0]))
        # print("p: " + str(cor_rank[1]))
        # print("")
        # print("Kendall Tau: " + str(cor_rank2[0]))
        # print("p: " + str(cor_rank2[1]))
        # print("")
        if len(get_uniques(prop1)[0]) < 15:
            X = []
            for uni in get_uniques(prop1)[0]:
                x = []
                for num, val in enumerate(prop1):
                    if val == uni:
                        x.append(prop[num])
                X.append(x)
            plot_results.corr_boxplot(X, savename, property1, property, get_uniques(prop1)[0], get_uniques(prop1)[1])
        else:
            plot_results.corr_scatter(prop1, prop, savename, property1, property)

    return [cor_lin, cor_rank, cor_rank2]


def get_uniques(list):
    "Returns unique elements and number of such in a list."
    uniques = []
    unique_nums = []
    for element in list:
        if element not in uniques:
            uniques.append(element)
            unique_nums.append(1)
        else:
            unique_nums[uniques.index(element)] = unique_nums[uniques.index(element)] + 1

    unique_nums_s = [unique_n for _, unique_n in sorted(zip(uniques, unique_nums))]
    uniques_s = sorted(uniques)

    return [uniques_s, unique_nums_s]