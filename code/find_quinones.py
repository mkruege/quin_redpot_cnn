from rdkit import Chem
import pandas as pd
import csv

def make_new_quinone_csv_(file, header):

    df = pd.read_csv(file)
    [quindex, quinones] = find_SMILES_quinones(df["smiles"])

    with open("ramak_quin_smiles.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for index in quindex:
        row = df.iloc[index]
        with open("ramak_quin_smiles.csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)
    return


def find_SMILES_quinones(smiles):
    "Extracts all quinones from input list of SMILES-representations"
    quinones = []
    quindex = []
    m_quinones = []
    m_quindex = []
    for num, smil in enumerate(smiles):
        added = 0
        mol = Chem.MolFromSmiles(smil) #transform SMILES to rdkit.Chem-molecule
        ri = mol.GetRingInfo() #get aromatic ring information
        if len(ri.AtomRings())>0: #at least one ring structure in molecule
            ringstruc = ri.AtomRings()
            ketos = mol.GetSubstructMatches(Chem.MolFromSmiles("C(=O)")) #get keto groups
            one = 0
            for ring in ringstruc:
                for atom in ring: #iterate over all atoms in ring systems
                    for keto in ketos:
                        for atom2 in keto: #also iterate over all atoms in keto groups
                            if atom == atom2: #if atom in both -> keto group attached to ring system
                                if one == 1: #if two -> possibly quinone!
                                    if added == 0: #only append it one time
                                        m_quinones.append(mol)
                                        m_quindex.append(num)
                                        added = 1
                                else:
                                    one =1
    #now check on conjugation
    for num, quin in enumerate(m_quinones): #go over molecules collected in first step
        ketos = quin.GetSubstructMatches(Chem.MolFromSmiles("C(=O)")) #get keto groups
        ri = quin.GetRingInfo() #get ring strucs
        ringstruc = ri.AtomRings()
        ringstruc2 = ri.BondRings()
        conj = 1
        for ringnum, ring in enumerate(ringstruc): #check if keto group atom is in any ring
            rk = 0
            for atom in ring:
                for keto in ketos:
                    for atom2 in keto:
                        if atom == atom2:
                            rk = 1
            if rk == 1: #if keto group atom part of ring: check if bonds in ring are all conjugated
                for bond in ringstruc2[ringnum]:
                    if quin.GetBondWithIdx(bond).GetIsConjugated() == 0:
                        conj = 0

        if conj == 1: #if applies, must be quinone now
            quinones.append(smiles[m_quindex[num]])
            quindex.append(m_quindex[num])
    return [quindex, quinones]