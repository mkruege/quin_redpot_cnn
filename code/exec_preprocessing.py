import multipr_model
from sys import argv
import argparse
import prepare_2d_nn_input


model_dir = './'
if argv[0].find('/') >= 0:
	model_dir = argv[0][: - argv[0][::-1].find('/')]


#parse arguments
parser = argparse.ArgumentParser(description='Perform preprocessing and save training data.')
parser.add_argument('--smiles_data', '-d', type=str, default='all_new.csv', help='Path to bed file with SMILES and chem. property data,')
parser.add_argument('--outval', '-ov', type=str, default='E', help='List of output value(s)')
parser.add_argument('--k_rands', '-kr', type=int, default=5, help='k_randomSMILES for augmentation method')
parser.add_argument('--n_rands', '-nr', type=int, default=0, help='n_randomSMILES for augmentation method')
parser.add_argument('--k_shifts', '-ks', type=int, default=5, help='k_shiftSMILES for augmentation method')
parser.add_argument('--n_shifts', '-ns', type=int, default=0, help='n_shiftSMILES for augmentation method')
parser.add_argument('--extend_frame', '-ef', type=int, default=0, help='extend_frame for augmentation method')
parser.add_argument('--cross_val', '-cv', type=int, default=0, help='Define k for k-fold cross validation.')
parser.add_argument('--rsmilfile', '-rs', type=str, default='random_smiles_x10.csv', help='File to obtain RandomSMILES augmentations without RDKit')
parser.add_argument('--outfilename', '-of', type=str, default='inpt_data', help='Output file name')

# parse and pre-process command line arguments
args = parser.parse_args()


args.smiles_data = args.smiles_data.split(',')
args.outval = args.outval.split(',')

print("Arguments parsed")

nn_input = prepare_2d_nn_input.prepare_input_from_original_smiles(args.smiles_data, args.outval, outvalnames=[], asfile=1,
                                                                      n_random_smiles=args.n_rands,
                                                                      n_shifts=args.n_shifts,
                                                                      k_random_smiles=args.k_rands,
                                                                      k_shifts=args.k_shifts,
                                                                      extend_frame=args.extend_frame,
                                                                      rands_nums_histo=0, crossval=args.cross_val, rsmilfile=args.rsmilfile, outfilename=args.outfilename)



print("Training data prepared.")
