import pandas as pd
from rdkit import Chem
import csv
from sklearn.preprocessing import OneHotEncoder


def only_smiles(file):
    "removes inchi column from csv file"
    df = pd.read_csv(file)
    df = df.drop("inchi", axis=1)
    df.to_csv(file[:-4] + "_smiles.csv", index=0)
    return

def get_input(file, header, savefile, files=1):
    'reads smiles-input sheets and merges them in equal format'

    data = read_input_sheets(file, files)

    data_merged = merge_data(data, header)

    uniques = write_csv(data_merged, header)

    encode_smiles(uniques, header, savefile)

    return


def read_input_sheets(file, files):
    'read dat from csv'
    if files == 1:
        df0 = pd.read_csv(file[0])
        return [df0]
    elif files == 2:
        df0 = pd.read_csv(file[0])
        df1 = pd.read_csv(file[1])
        return [df0, df1]
    elif files == 3:
        df0 = pd.read_csv(file[0])
        df1 = pd.read_csv(file[1])
        df2 = pd.read_csv(file[2])
        return [df0, df1, df2]
    else:
        print("Not coded yet (prepare_2d_smiles_data.read_input_sheets)")

def merge_data(data, header):
    'transform data to equal format and join data'
    all_data = []
    for df in data:
        for num, smiles in enumerate(df["smiles"]):
            new_data = []
            smil = list((Chem.MolToSmiles(Chem.MolFromSmiles(smiles)))) #transform with rdkit to obtain equal forms
            for prop in header:
                new_data.append(df[prop][num])
            for i in smil: #also split strings into single chars
                new_data.append(i)
            all_data.append(new_data)


    for num1, dat in enumerate(all_data): #find chars that are only part of element names
        for num, char in enumerate(dat):
            if char == "r":
                dat[num-1] = dat[num-1] + char
                del dat[num]

    return all_data


def merge_multi_outputs(data, header):
    'transform data to equal format and join data'
    all_data = []
    for df in data:
        for num, smiles in enumerate(df["smiles"]):
            new_data = []
            smil = list((Chem.MolToSmiles(Chem.MolFromSmiles(smiles))))  # transform with rdkit to obtain equal forms
            for merges in header:
                for prop in merges:
                    try:
                        new_data.append(df[prop][num])
                    except:
                        pass
            for i in smil:  # also split strings into single chars
                new_data.append(i)
            all_data.append(new_data)

    for num1, dat in enumerate(all_data):  # find chars that are only part of element names
        for num, char in enumerate(dat):
            if char == "r":
                dat[num - 1] = dat[num - 1] + char
                del dat[num]

    return all_data




def write_csv(data, header):
    'write to one csv'
    lengths = []
    for dat in data: #find max string length
        lengths.append(len(dat)-len(header))
    maxlength = max(lengths)

    for dat in data: #fill up with a new character to get equal lengths
        for i in range(maxlength+10+len(header)):
            try:
                dat[i]
            except:
                dat.append("ö")

    uniques = [] #find all unique characters
    for dat in data:
        for char in dat[len(header):]:
            if char not in uniques:
                uniques.append(char)

    header0 = []
    for prop in header:
        header0.append(prop)
    for i in range(maxlength+10):
        header0.append(i)

    with open("final_input_smiles.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header0)

    for num, row in enumerate(data):
        with open("final_input_smiles.csv", "a") as csv_file:  # create file and write header
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)

    return uniques

def encode_smiles(uniques, header, savefile):

    'encodes the input via one hot encoding'

    data = pd.read_csv("final_input_smiles.csv")
    ohedata = data.drop(header, axis=1) #remove E and Gsolv before one hot encoding

    uniquesarray = []
    for i in range(ohedata.shape[1]):
        uniquesarray.append(uniques)

    ohe = OneHotEncoder(categories=uniquesarray) #apply one-hot-encoding on all chars
    X = ohedata
    data2 = ohe.fit_transform(X).toarray()

    alldata = []
    for num, dat in enumerate(data2): #merge with E and Gsolv from original dataframe
        newdata = []
        for prop in header:
            newdata.append(data[prop][num])
        for d in dat:
            newdata.append(d)
        alldata.append(newdata)

    for i in range(data2.shape[1]): #prepare csv header
        header.append(i)


    with open(savefile, "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for row in alldata:
        with open(savefile, "a") as csv_file:  # create file and write header
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)

    return