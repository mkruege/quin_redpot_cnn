import pandas as pd
import csv





def analyse_SMILES(Y_test, y_pred, savename, outval):
    "Makes csv sheet that contains all quinones in validation dataset sorted by absolute/relative error."
    #find associated input data
    if "tabor" in savename:
        df = pd.read_csv("smiles_tabor.csv")
    elif "ramak" in savename:
        df = pd.read_csv("smiles_ramak.csv")
    elif "pdf" in savename or "krist" in savename:
        df = pd.read_csv("smiles_pdf.csv")
    elif "er" in savename:
        df = pd.read_csv("smiles_er.csv")

    else:
        df = pd.read_csv("smiles_all.csv")

    if len(y_pred[0])>1: #more than one value predicted
        print("Analysis of NN results only working with one output so far.")
        return


    relative_err = []
    absolute_err = []

    for num, pred in enumerate(y_pred):
        #get relative error
        if Y_test[num]==0:
            relative_err.append(0)
        else:
            relative_err.append(abs(pred-Y_test[num]/Y_test[num]))

        #get absolute error
        absolute_err.append(abs(pred-Y_test[num]))

    #make list from
    y_test_sorted_rel = [y for _, y in sorted(zip(relative_err, Y_test))]
    relative_err_sorted = sorted(relative_err)
    y_test_sorted_abs = [y for _, y in sorted(zip(absolute_err, Y_test))]
    absolute_err_sorted = sorted(absolute_err)

    quin_sorted_rel = []
    quin_sorted_abs = []


    for y in y_test_sorted_abs:
        try:
            quin_sorted_abs.append(df.loc[df[outval] == y[0]]["smiles"].tolist()[0])
        except:
            dist = []
            act = []
            for actualY in df[outval]:
                dist.append(abs(actualY - y))
                act.append(actualY)
            quin_sorted_abs.append(df.loc[df[outval] == act[dist.index(min(dist))]]["smiles"].tolist()[0])


    for y in y_test_sorted_rel:
        try:
            quin_sorted_rel.append(df.loc[df[outval] == y[0]]["smiles"].tolist()[0])
        except:
            dist = []
            act = []
            for actualY in df[outval]:
                dist.append(abs(actualY - y))
                act.append(actualY)
            quin_sorted_rel.append(df.loc[df[outval] == act[dist.index(min(dist))]]["smiles"].tolist()[0])




    #print lists to csv
    header = ["smiles", "rel_error"]
    rows = []
    for num, quin in enumerate(quin_sorted_rel):
        try:
            rows.append([quin, relative_err_sorted[num][0]])
        except:
            rows.append([quin, relative_err_sorted[num]])

    with open(savename + "_rel_err_sorted.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for row in rows:
        with open(savename + "_rel_err_sorted.csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)



    header = ["smiles", "abs_error"]
    rows = []
    for num, quin in enumerate(quin_sorted_abs):
        rows.append([quin, absolute_err_sorted[num][0]])

    with open(savename + "_abs_err_sorted.csv", "w+") as csv_file:  # create file and write header
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow(header)

    for row in rows:
        with open(savename + "_abs_err_sorted.csv", "a") as csv_file:  # add row to file
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(row)


    return