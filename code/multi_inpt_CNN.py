from tensorflow import keras
from tensorflow.keras import layers
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
import plot_results
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import concatenate
import numpy as np



def run_model(data, epochs=200, batch_size=8, savename="default", outvals=["E"], analyse=1):

    [X_train, X_test, Y_train, Y_test, scaler_y, X_train_val, X_test_val] = data

    try:
        outputs = Y_train.shape[1]
    except:
        outputs = 1

    # create the MLP and CNN models
    mlp = create_mlp(1, regress=True)
    cnn = create_cnn((X_train.shape[1], X_train.shape[2], 1), regress=True)
    # create the input to our final set of layers as the *output* of both
    # the MLP and CNN
    combinedInput = concatenate([mlp.output, cnn.output])
    # our final FC layer head will have two dense layers, the final one
    # being our regression head
    x = Dense(4, activation="relu")(combinedInput)
    x = Dense(1, activation="linear")(x)
    # our final model will accept categorical/numerical data on the MLP
    # input and images on the CNN input, outputting a single value (the
    # predicted price of the house)
    model = Model(inputs=[mlp.input, cnn.input], outputs=x)

    # compile the model using mean absolute percentage error as our loss,
    # implying that we seek to minimize the absolute percentage difference
    # between our price *predictions* and the *actual prices*
    opt = Adam(lr=1e-3, decay=1e-3 / epochs)
    model.compile(loss="mean_squared_error", optimizer=opt)
    # train the model
    print("[INFO] training model...")
    history = model.fit(
        x=[X_train_val, X_train], y=Y_train,
        validation_data=([X_test_val, X_test], Y_test),
        epochs=epochs, batch_size=batch_size)



    # plot training history
    plot_results.plot_model_history(history, savename)

    Y_pred = model.predict([X_test_val, X_test])

    #print(model.evaluate(X_train, Y_train))

    # print("epochs: ", epochs)
    # print("batch_size: ", batch_size)
    print(savename)
    print("MSE_normalized: " + str(mean_squared_error(Y_test, Y_pred)))

    Y_pred = MinMaxScaler.inverse_transform(scaler_y, np.concatenate([np.zeros((Y_pred.shape[0], 1)), Y_pred.reshape(Y_pred.shape[0], 1)], axis=1))[:,1]

    Y_test = MinMaxScaler.inverse_transform(scaler_y, np.concatenate([np.zeros((Y_test.shape[0], 1)), Y_test.reshape(Y_test.shape[0], 1)], axis=1))[:,1]

    print("MSE_original: " + str(mean_squared_error(Y_test, Y_pred)))

    Y_pred0 = []
    Y_pred1 = []
    Y_test0 = []
    Y_test1 = []
    for num, elem in enumerate(X_test_val):
        if elem == 0:
            Y_pred0.append(Y_pred[num])
            Y_test0.append(Y_test[num])
        else:
            Y_pred1.append(Y_pred[num])
            Y_test1.append(Y_test[num])

    Y_pred0 = np.asarray(Y_pred0).reshape(len(Y_pred0), 1)
    Y_pred1 = np.asarray(Y_pred1).reshape(len(Y_pred1), 1)
    Y_test0 = np.asarray(Y_test0).reshape(len(Y_test0), 1)
    Y_test1 = np.asarray(Y_test1).reshape(len(Y_test1), 1)

    plot_results.prediction_scatter(Y_test0, Y_pred0, savename, outvals)
    plot_results.prediction_scatter(Y_test1, Y_pred1, savename, outvals)



    #if analyse == 1:
    #    analyse_NN_results.analyse_SMILES(Y_test, Y_pred, savename, outvals[0])

    #TODO: Implement analyse_results for multi_input

    return


def create_mlp(dim, regress=False):
    # define our MLP network
    model = Sequential()
    model.add(Dense(8, input_dim=dim, activation="relu"))
    model.add(Dense(4, activation="relu"))
    if regress:
        model.add(Dense(1, activation="linear"))
    return model

def create_cnn2(inputShape, outputs=1, filters=(16, 32, 64), regress=False):
    chanDim = -1
    # define the model input
    inputs = Input(shape=inputShape)
    # loop over the number of filters
    for (i, f) in enumerate(filters):
        # if this is the first CONV layer then set the input
        # appropriately
        if i == 0:
            x = inputs
        # CONV => RELU => BN => POOL
        x = Conv2D(f, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)

        # flatten the volume, then FC => RELU => BN => DROPOUT
        x = Flatten()(x)
        x = Dense(16)(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=chanDim)(x)
        x = Dropout(0.5)(x)
        # apply another FC layer, this one to match the number of nodes
        # coming out of the MLP
        x = Dense(4)(x)
        x = Activation("relu")(x)
        # check to see if the regression node should be added
        if regress:
            x = Dense(1, activation="linear")(x)

    # construct the CNN
    model = Model(inputs, x)
    # return the CNN
    return model

def create_cnn(inputShape, outputs=1, filters=(16, 32, 64), regress=False):
    chanDim = -1
    # define the model input
    inputs = keras.Input(shape=inputShape)

    x = layers.Conv2D(32, 3, strides=2, padding="same")(inputs)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    x = layers.Conv2D(64, 3, padding="same")(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    previous_block_activation = x  # Set aside residual

    for size in [128, 256, 512, 728]:
        x = layers.Activation("relu")(x)
        x = layers.SeparableConv2D(size, 3, padding="same")(x)
        x = layers.BatchNormalization()(x)

        x = layers.Activation("relu")(x)
        x = layers.SeparableConv2D(size, 3, padding="same")(x)
        x = layers.BatchNormalization()(x)

        x = layers.MaxPooling2D(3, strides=2, padding="same")(x)

        # Project residual
        residual = layers.Conv2D(size, 1, strides=2, padding="same")(
            previous_block_activation
        )
        x = layers.add([x, residual])  # Add back residual
        previous_block_activation = x  # Set aside next residual

    x = layers.SeparableConv2D(1024, 3, padding="same")(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    x = layers.GlobalAveragePooling2D()(x)
    #activation = "sigmoid"
    # activation = "softmax"
    activation = "linear"

    x = layers.Dropout(0.5)(x)
    x = layers.Dense(outputs, activation=activation)(x)

    # construct the CNN
    model = Model(inputs, x)
    # return the CNN
    return model

def create_model(input_shape, outputs):

    inputs = keras.Input(shape=input_shape)

    x = layers.Conv2D(32, 3, strides=2, padding="same")(inputs)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    x = layers.Conv2D(64, 3, padding="same")(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    previous_block_activation = x  # Set aside residual

    for size in [128, 256, 512, 728]:
        x = layers.Activation("relu")(x)
        x = layers.SeparableConv2D(size, 3, padding="same")(x)
        x = layers.BatchNormalization()(x)

        x = layers.Activation("relu")(x)
        x = layers.SeparableConv2D(size, 3, padding="same")(x)
        x = layers.BatchNormalization()(x)

        x = layers.MaxPooling2D(3, strides=2, padding="same")(x)

        # Project residual
        residual = layers.Conv2D(size, 1, strides=2, padding="same")(
            previous_block_activation
        )
        x = layers.add([x, residual])  # Add back residual
        previous_block_activation = x  # Set aside next residual

    x = layers.SeparableConv2D(1024, 3, padding="same")(x)
    x = layers.BatchNormalization()(x)
    x = layers.Activation("relu")(x)

    x = layers.GlobalAveragePooling2D()(x)
    activation = "sigmoid"
    # activation = "softmax"

    x = layers.Dropout(0.5)(x)
    outputs = layers.Dense(outputs, activation=activation)(x)

    return keras.Model(inputs, outputs)