import matplotlib.pyplot as plt
import numpy as np
#import seaborn as sns
from sklearn.metrics import r2_score

def corr_heatmap(df, savename, corrtype):
    plt.pcolor(df, cmap="binary", label="CNN Model Mean Squared Error")
    plt.clim(0.25, 0.45)
    plt.yticks(np.arange(0.5, len(df.index), 1), df.index, fontsize=13)
    plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns, rotation=-70, fontsize=13)
    plt.xlabel("RandomSMILES Augmentations", fontsize=13)
    plt.ylabel("ShiftSMILES Augmentations", fontsize=13)
    cbar = plt.colorbar()
    cbar.ax.tick_params(labelsize=13)
    cbar.ax.set_xlabel("Model \n RMSE [V]", fontsize=13)
    cbar.ax.xaxis.set_label_coords(1.4, -0.06)
    plt.tight_layout()
    plt.savefig(savename + "_heatmap_" + corrtype)
    plt.show()


def heatmap2(x, y, size, color, savename, corrtype): #requires seaborn
    # "From https://towardsdatascience.com/better-heatmaps-and-correlation-matrix-plots-in-python-41445d0f2bec"
    # fig, ax = plt.subplots()
    #
    # # Mapping from column names to integer coordinates
    # x_labels = [v for v in sorted(x.unique())]
    # y_labels = [v for v in sorted(y.unique())]
    # x_to_num = {p[1]: p[0] for p in enumerate(x_labels)}
    # y_to_num = {p[1]: p[0] for p in enumerate(y_labels)}
    #
    # size_scale = 100
    # ax.scatter(
    #     x=x.map(x_to_num),  # Use mapping for x
    #     y=y.map(y_to_num),  # Use mapping for y
    #     s=size * size_scale,  # Vector of square sizes, proportional to size parameter
    #     marker='s'  # Use square as scatterplot marker
    # )
    #
    # # Show column labels on the axes
    # ax.set_xticks([x_to_num[v] for v in x_labels])
    # ax.set_xticklabels(x_labels, rotation=45, horizontalalignment='right')
    # ax.set_yticks([y_to_num[v] for v in y_labels])
    # ax.set_yticklabels(y_labels)
    #
    # ax.grid(False, 'major')
    # ax.grid(True, 'minor')
    # ax.set_xticks([t + 0.5 for t in ax.get_xticks()], minor=True)
    # ax.set_yticks([t + 0.5 for t in ax.get_yticks()], minor=True)
    #
    # ax.set_xlim([-0.5, max([v for v in x_to_num.values()]) + 0.5])
    # ax.set_ylim([-0.5, max([v for v in y_to_num.values()]) + 0.5])
    #
    # n_colors = 256  # Use 256 colors for the diverging color palette
    # palette = sns.diverging_palette(20, 220, n=n_colors)  # Create the palette
    # color_min, color_max = [-1,1]  # Range of values that will be mapped to the palette, i.e. min and max possible correlation
    #
    #
    # plot_grid = plt.GridSpec(1, 15, hspace=0.2, wspace=0.1)  # Setup a 1x15 grid
    # ax = plt.subplot(plot_grid[:, :-1])  # Use the leftmost 14 columns of the grid for the main plot
    #
    # def value_to_color(val):
    #     val_position = float((val - color_min)) / (
    #                 color_max - color_min)  # position of value in the input range, relative to the length of the input range
    #     ind = int(val_position * (n_colors - 1))  # target index in the color palette
    #     return palette[ind]
    #
    # ax.scatter(
    #     x=x.map(x_to_num),  # Use mapping for x
    #     y=y.map(y_to_num),  # Use mapping for y
    #     s=size * size_scale,  # Vector of square sizes, proportional to size parameter
    #     c=color.apply(value_to_color),  # Vector of square colors, mapped to color palette
    #     marker='s'  # Use square as scatterplot marker
    # )
    # # ...
    #
    # # Add color legend on the right side of the plot
    # ax = plt.subplot(plot_grid[:, -1])  # Use the rightmost column of the plot
    #
    # col_x = [0] * len(palette)  # Fixed x coordinate for the bars
    # bar_y = np.linspace(color_min, color_max, n_colors)  # y coordinates for each of the n_colors bars
    #
    # bar_height = bar_y[1] - bar_y[0]
    # ax.barh(
    #     y=bar_y,
    #     width=[5] * len(palette),  # Make bars 5 units wide
    #     left=col_x,  # Make bars start at 0
    #     height=bar_height,
    #     color=palette,
    #     linewidth=0
    # )
    # ax.set_xlim(1, 2)  # Bars are going from 0 to 5, so lets crop the plot somewhere in the middle
    # ax.grid(False)  # Hide grid
    # ax.set_facecolor('white')  # Make background white
    # ax.set_xticks([])  # Remove horizontal ticks
    # ax.set_yticks(np.linspace(min(bar_y), max(bar_y), 3))  # Show vertical ticks for min, middle and max
    # ax.yaxis.tick_right()  # Show vertical ticks on the right
    #
    # plt.savefig(savename + "_heatmap_2_" + corrtype)
    # plt.show()
    return

def prediction_scatter(Y_test, Y_pred, savename, outvals):
    "Plots and saves common correlation scatter for each output"
    for num, outval in enumerate(outvals):
        plt.scatter(Y_test[:, num], Y_pred[:, num], s=1)
        plt.ylabel(outval + " Prediction [V]")
        plt.xlabel(outval + " Values [V]")
        plt.savefig(savename + outval + ".png")
        plt.show()

    return

def prediction_scatter2(Y_test, Y_pred, savename, outvals):
    "Plots and saves common correlation scatter for each output"
    plt.scatter(Y_test, Y_pred, s=1)
    plt.ylabel(outvals[0] + " Prediction [V]")
    plt.xlabel(outvals[0] + " Values [V]")
    plt.savefig(savename + outvals[0] + ".png")
    plt.show()

    return

def nums_histo(dat, type):
    plt.hist(dat, bins=50)
    if type==0:
        plt.xlabel("Number of RandomSMILES augmentations for molecule when k_rands = 200.")
    else:
        plt.xlabel("Number of ShiftSMILES augmentations for molecule when k_shifts = 100 and extend_frame = 0.")
    plt.ylabel("Number of Molecules.")
    plt.show()
    return


def corr_boxplot(X, savename, xlabel, ylabel, xticks, x_n):
    plt.boxplot(X)
    plt.xlabel(xlabel)
    plt.ylabel("E [V]")
    if xlabel not in "n_keto":
        plt.xticks(range(1, len(xticks)+1), [str(x)[:4] + "\nn=" + str(x_n[i]) for i, x in enumerate(xticks)])
    else:
        plt.xticks(range(1, len(xticks) + 1), [str(x)[:4] + "\n" + str(x_n[i]) for i, x in enumerate(xticks)])
    plt.tight_layout()
    plt.savefig("boxplot_" + savename + "_stat_" + xlabel.replace("/", ",") + "_" + ylabel + ".png")
    plt.show()
    return

def corr_scatter(X, Y, savename, xlabel, ylabel):
    plt.scatter(X, Y, marker=".", s=1)
    z = np.polyfit(X, Y, 1)
    p = np.poly1d(z)
    plt.plot(X, p(X), "r--")
    plt.xlabel(xlabel)
    plt.ylabel("E [V]")
    plt.savefig("scatter_" + savename + "_stat_" + xlabel.replace("/", ",") + "_" + ylabel + ".png")
    plt.show()
    print("X: " + xlabel)
    print("Y: " + ylabel)
    print("y=%.6fx+%.6f" % (z[0], z[1]))
    print("R2 = " + str(r2_score(X, Y)))
    return


def plot_model_history(history, params):

    try:
        print(history.history.keys())
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title(str(params['first_conv_layer']) + ", " +  str(params['second_dense_layer']) + " nr, " + str(params['lr']) + " lr")
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper left')
        #plt.savefig("model_loss" + str(len(X_train) + len(x_val)) + "-" + str(params['first_hidden_layer']) + "-" + str(params['lr']) + "-" + str(params['batch_size']) + ".png")
        plt.show()
    except:
        print(history.history.keys())
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title(params)
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper left')
        # plt.savefig("model_loss" + str(len(X_train) + len(x_val)) + "-" + str(params['first_hidden_layer']) + "-" + str(params['lr']) + "-" + str(params['batch_size']) + ".png")
        plt.show()