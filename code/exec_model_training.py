import multipr_model
from sys import argv
import argparse
import test_model
import pandas as pd
import pickle

model_dir = './'
if argv[0].find('/') >= 0:
	model_dir = argv[0][: - argv[0][::-1].find('/')]


#parse arguments
parser = argparse.ArgumentParser(description='Running CNN HPT following specified hpt-csv sheet.')
parser.add_argument('--input_data', '-d', type=str, default='inpt_data', help='Path to bed file with processed input data,')
parser.add_argument('--hpt_sheet', '-hp', type=str, default='hpt_sheet.csv', help='Path to bed file with hpt information')
parser.add_argument('--hpt_run', '-r', type=int, default=0, help='Which line in hpt-sheet is executed')
parser.add_argument('--outval', '-ov', type=str, default='E', help='List of output value(s)')
parser.add_argument('--cross_val', '-cv', type=int, default=0, help='Define k for k-fold cross validation.')


# parse and pre-process command line arguments
args = parser.parse_args()


#args.input_data = args.input_data.split(',')
args.outval = args.outval.split(',')

print("Arguments parsed")

hpt = pd.read_csv(args.hpt_sheet)
infile = open(args.input_data,'rb')
nn_input = pickle.load(infile)
infile.close()
i = args.hpt_run

test_model.run(nn_input, hpt['filters'][i].split(","), hpt['window_sizec'][i].split(","),
     hpt['stridesizec'][i].split(","), hpt['paddingc'][i].split(","),
     hpt['pooling_type'][i].split(","), hpt['window_sizep'][i].split(","),
     hpt['stridesizep'][i].split(","), hpt['paddingp'][i].split(","),
     hpt['global_pooling'][i], hpt['outlayer_act'][i],
     hpt['batch_size'][i], hpt['batch_norm'][i], hpt['epochs'][i],
     hpt['dropout'][i], hpt['optimizer'][i], hpt['learning_rate'][i],
     hpt['decay'][i], hpt['loss'][i], output=1)