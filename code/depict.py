import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Draw



def transform_smiles_data_to_picture(dataset, savename, size=(100, 100)):
    "Transforms smiles strings from data set into images of given size."
    df = pd.read_csv(dataset)
    depict_smiles(df["smiles"], savename, size) #pass smiles representations
    return


def depict_smiles(smiles, savename, size):
    # read smiles strings and transform to rdkit molecules
    mols = []
    for smil in smiles:
        mols.append(Chem.MolFromSmiles(smil))

    for num, mol in enumerate(mols):
        AllChem.Compute2DCoords(mol)
        Draw.MolToFile(mol, savename + str(num) + ".png", size=size)
    return

def depict_single_smile(smile, savename, size):
    # read smiles strings and transform to rdkit molecules
    mol = Chem.MolFromSmiles(smile)

    AllChem.Compute2DCoords(mol)
    Draw.MolToFile(mol, savename + ".png", size=size)

    return