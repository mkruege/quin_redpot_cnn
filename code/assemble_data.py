import pandas as pd
import numpy as np
import find_quinones

def merge_equal_sheets(input_files, savename, checkquin=1):
    "Merge data sets and remove all duplicates. Also, if checkquin==1 remove all non-quinones, checkquin==2 to remove all non-quinones"
    dataset1 = pd.read_csv(input_files[0])


    df1 = pd.DataFrame(dataset1)

    if len(input_files) > 1:
        dataset2 = pd.read_csv(input_files[1])
        df2 = pd.DataFrame(dataset2)

        #merge first two
        whole = pd.concat([df1, df2], ignore_index=1)

        #if more files, keep merging next one
        if len(input_files) > 2:
            dataset3 = pd.read_csv(input_files[2])
            df3 = pd.DataFrame(dataset3)
            whole = pd.concat([whole, df3], ignore_index=1)
            if len(input_files) > 3:
                dataset4 = pd.read_csv(input_files[3])
                df4 = pd.DataFrame(dataset4)
                whole = pd.concat([whole, df4], ignore_index=1)
                if len(input_files) > 4:
                    dataset5 = pd.read_csv(input_files[4])
                    df5 = pd.DataFrame(dataset5)
                    whole = pd.concat([whole, df5], ignore_index=1)

    else:
        whole = df1

    if checkquin == 1: #remove all non-quinones
        drops1 = []  # list of elements to remove
        smiles = whole["smiles"].tolist() #list of smiles strings
        quindex = find_quinones.find_SMILES_quinones(smiles)[0] #get index of all molecules that are quinones
        for i in range(len(smiles)):
            if i not in quindex:
                drops1.append(i)
                print("Found and removed molecule that is not quinone:")
                print(smiles[i])
        print(str(len(drops1)) + " molecules have been removed because they are not quinones.")
        whole = whole.drop(drops1, axis=0) #remove non-quinones from dataframe

    if checkquin == 2: #remove all quinones
        drops1 = []
        smiles = whole["smiles"].tolist()
        quindex = find_quinones.find_SMILES_quinones(smiles)[0]
        for i in range(len(smiles)):
            if i in quindex:
                drops1.append(i)
                print("Found and removed molecule that is quinone:")
                print(smiles[i])
        whole = whole.drop(drops1, axis=0) #remove all quinones from dataframe

    #also remove all duplicates
    print("Shape before duplicate filter:")
    print(whole.shape)
    whole = whole.drop_duplicates("smiles")
    print("Shape after duplicate filter:")
    print(whole.shape)

    if savename[-4:] in ".csv":
        whole.to_csv(savename, index=False)
    else:
        whole.to_csv(savename + ".csv", index=False)

    return