from tensorflow import keras
import pickle
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
from matplotlib import pyplot as plt
import random
import plot_results
try:
    import chem_stat_analysis
except:
    pass
import csv
#import depict
import numpy as np
from sklearn import linear_model
from sklearn import kernel_ridge
from tensorflow.keras import backend as K
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier





def check_NN_testdata(modelpath, datafile, testsetfile, makenew=1, testset_csv="testset.csv", out="E"):
    global colorvar

    # make chem- and statfile and get Y_test
    [Y_smiles, Y_test] = make_testset_chemfile(testsetfile, makenew=makenew, out=out)
    print("chemfile prepared")

    # load trained CNN model
    model = keras.models.load_model(modelpath)  # load trained model
    print("model loaded")

    # get model predictions for corresponding data
    Y_pred = get_predictions(model, datafile, testsetfile, makenew)
    print("Data predicted")

    # enable to restrict analysis to molecules containing certain functional groups
    restrict_testset = 0
    if restrict_testset == 1:
        testset_chem = pd.read_csv("testset_chem_heatmap.csv")
        amine = testset_chem.index[(testset_chem['amine_g'] == True)].tolist()
        amide = testset_chem.index[(testset_chem['amide_g'] == True)].tolist()
        nitrile = testset_chem.index[(testset_chem['nitrile_g'] == True)].tolist()
        sulfide = testset_chem.index[(testset_chem['sulfide_g'] == True)].tolist()

        all_groups = []
        for group in [amide, amine, nitrile]:
            for num in group:
                if num not in all_groups:
                    all_groups.append(num)
        all_groups.sort()
        Y_pred_l = Y_pred.tolist()
        for index in sorted(all_groups, reverse=True):
            del Y_pred_l[index]
            del Y_smiles[index]
            del Y_test[index]
        testset_chem.drop(all_groups, 0, inplace=True)

        Y_pred = np.asarray(Y_pred_l)
        testset_chem.to_csv("testset_chem_heatmap.csv")


    # fit linear model and compare:
    compare_with_linear_gc_model("tabor_for_publ_E_chem_heatmap.csv", testset_csv, Y_pred, out=out)

    # fit classification model and compare:
    compare_with_clustering("all_chem_heatmap.csv", testset_csv, Y_pred)

    #Sensitivity analysis:
    prediction_sensitivity(model, datafile, testsetfile, Y_pred)

    #predict example quinones from experiments
    make_quinone_exp_comparison_plot(datafile, model)
    
    # make predictions on generated quinones and compare
    investigate_generated_quinones(model, datafile, makenew=1)

    # for testset molecules instead
    investigate_testset_quinones(model, datafile, testsetfile, Y_pred, Y_test, makenew=0)

    return

    #additional plotting tools

    # look_into_NN(model, datafile)
    #
    # [struc_unique_groups, struc_unique_index_groups, el_unique_groups, el_unique_index_groups] = find_groups_from_smiles(Y_smiles)
    # print("groups obtained")
    #
    # chem = pd.read_csv("testset_chem.csv")
    #
    # colorvar = 0
    #
    # xys = prep_trackgroup_plot(struc_unique_index_groups, Y_pred, chem['n_rings'], chem['n_O'], chem['n_double_bonds'])
    # make_trackgroup_plot(xys, "n(rings)", "n(O)", "Keto group distance", "Molecules of similar structure", 0)
    #
    # colorvar = 0
    #
    # xys = prep_trackgroup_plot(struc_unique_index_groups, Y_test, chem['n_rings'], chem['n_O'], chem['n_double_bonds'])
    # make_trackgroup_plot(xys, "n(rings)", "n(O)", "Keto group distance", "Molecules of similar structure", 1)
    #
    # colorvar = 0
    #
    # xys = prep_trackgroup_plot(el_unique_index_groups, Y_pred, chem['n_rings'], chem['n_O'], chem['n_double_bonds'])
    # make_trackgroup_plot(xys, "n(rings)", "n(O)", "n(Double bonds)", "Molecules of equal sum formula", 0)
    #
    # colorvar = 0
    #
    # xys = prep_trackgroup_plot(el_unique_index_groups, Y_test, chem['n_rings'], chem['n_O'], chem['n_double_bonds'])
    # make_trackgroup_plot(xys, "n(rings)", "n(O)", "n(Double bonds)", "Molecules of equal sum formula", 1)
    #
    # return



def error_analysis(Y_pred, Y_test):
    "Performs full error analysis based on model predictions and dataset values"

    import analyse_NN_results

    analyse_NN_results.analyse_SMILES(Y_test, Y_pred, "testset_errors", "E")
    chem_stat_analysis.perform_full_analysis_smiles("testset_errors_abs_err_sorted.csv", "testset_error_chemstat", ["abs_error"])


    return

def prediction_sensitivity(model, datafile, testsetfile, Y_pred):
    "Performs sensitivity analysis for individual SMILES characters"
    # get test data set from training data to obtain predictions
    infile = open(datafile, 'rb')
    data = pickle.load(infile)
    infile.close()

    infile = open(testsetfile, 'rb')
    test = pickle.load(infile)
    infile.close()

    xtest = test[2]


    scaler_y = data[-1][4]


    #char_pos = ['O', '=', 'C', '(', ')', 'c', '1', '2', '3', 'ö', 'P', 'S', '-', '4', 'n', '[', '+', ']', 'l', 'N', 'H', 'F', 'i', 'o', '#', '5', 's', 'Br', '6']

    #feature_shift
    char_pos = ['5', '+', 'F', 'l', 's', 'i', 'Br', '6', 'c', '1', '2', 'O', '=', '(', 'C', ')', 'P', 'H', '-', 'ö', '3', 'S', '4', 'o', 'N', '#', '[', 'n', ']']

    # removal-sensitivity
    sensitivity_chars = ['O', '=', 'C', '1', '(', 'P', ')', 'c', '2', 'S', '3']
    remove_sensitivity = []

    for remove_this in sensitivity_chars:
        new_groups = []
        new_groups_values = []
        for molnum, mol in enumerate(xtest):
            for num, pos in enumerate(mol):
                if pos[char_pos.index(remove_this)] == 1:
                    new_mol = np.delete(mol, (num), axis=0)
                    new_mol = np.transpose(new_mol)
                    new_mol = np.hstack((new_mol, np.tile(new_mol[:, [-1]], 1)))
                    new_mol = np.transpose(new_mol)
                    new_groups.append(new_mol)
                    new_groups_values.append(Y_pred[molnum])
                    continue
        try:
            new_groups, new_groups_values = zip(*random.sample(list(zip(new_groups, new_groups_values)), 10000))
        except:
            try:
                new_groups, new_groups_values = zip(*random.sample(list(zip(new_groups, new_groups_values)), len(new_groups)))
            except:
                remove_sensitivity.append([])
                continue
        new_group_preds = model.predict(np.array(new_groups))
        new_group_preds = MinMaxScaler.inverse_transform(scaler_y, new_group_preds)

        sensitivity_values = []
        for num, pred in enumerate(new_group_preds):
            sensitivity_values.append((pred - new_groups_values[num])[0])
        print(len(list(filter(lambda x: (x < 0), sensitivity_values))))
        print(len(list(filter(lambda x: (x >= 0), sensitivity_values))))
        remove_sensitivity.append(sensitivity_values)
        print("Sensiticity analysis for " + remove_this + " terminated!")

    full_lengths = [len(remove_sensitivity[x]) for x in range(len(remove_sensitivity))]

    plt.boxplot(remove_sensitivity)
    plt.xticks(range(len(remove_sensitivity) + 1)[1:],
               labels=sensitivity_chars)  # labels=[x + "\nn=" + str(full_lengths[i]) for i, x in enumerate(char_pos)])
    plt.grid()
    plt.xlabel("Removed feature instance")
    plt.ylabel("Effect on E prediction [V]")
    plt.savefig("remove_sensitivity_box.png")
    plt.show()

    abs_sums = []
    for char in remove_sensitivity:
        abs_sums.append(np.sum(np.abs(char)))
    plt.bar(range(len(abs_sums) + 1)[1:], [x / sum(abs_sums) for x in abs_sums])
    plt.xticks(range(len(abs_sums) + 1)[1:], labels=sensitivity_chars)
    plt.xlabel("Removed feature instance")
    plt.ylabel("Average absolute effect on E prediction [V]")
    plt.savefig("remove_sensitivity_bar.png")
    plt.show()

    #swap-sensitivity
    swap_sensitivity = []
    swap_pairs = [["C", "c"], ["c", "C"], ["O", "C"], ["O", "P"], ["P", "O"], ["O", "S"], ["S", "O"], ["P", "S"], ["S", "P"], ["P", "C"], ["S", "C"], ["P", "c"], ["S", "c"]]
    swap_pair_strings = ["C>c", "c>C", "O>C", "O>P", "P>O", "O>S", "S>O", "P>S", "S>P", "P>C", "S>C", "P>c", "S>c"]
    new_swap_groups = []
    new_swap_group_values = []
    for pair in swap_pairs:
        pair1 = np.zeros((29, ), dtype=bool)
        pair2 = np.zeros((29, ), dtype=bool)
        pair1[char_pos.index(pair[0])] = True
        pair2[char_pos.index(pair[1])] = True
        for molnum, mol in enumerate(xtest):
            for num, pos in enumerate(mol):
                if pos[char_pos.index(pair[0])] == 1:
                    new_mol = np.concatenate([mol[:num], np.reshape(pair2, (1, 29)), mol[num+1:]], axis=0)
                    new_swap_groups.append(new_mol)
                    new_swap_group_values.append(Y_pred[molnum])
                    continue

        new_groups, new_groups_values = zip(*random.sample(list(zip(new_swap_groups, new_swap_group_values)), 10000))
        new_group_preds = model.predict(np.array(new_groups))
        new_group_preds = MinMaxScaler.inverse_transform(scaler_y, new_group_preds)


        sensitivity_values = []
        for num, pred in enumerate(new_group_preds):
            sensitivity_values.append((pred - new_groups_values[num])[0])
        swap_sensitivity.append(sensitivity_values)
        print("Sensiticity analysis for " + pair[0] + " and " + pair[1] + " terminated!")

    full_lengths = [len(swap_sensitivity[x]) for x in range(len(swap_sensitivity))]

    plt.boxplot(swap_sensitivity)
    plt.xticks(range(len(swap_sensitivity) + 1)[1:],
               labels=swap_pair_strings)  # labels=[x + "\nn=" + str(full_lengths[i]) for i, x in enumerate(char_pos)])
    plt.grid()
    plt.xlabel("Feature substitution")
    plt.ylabel("Effect on E prediction [V]")
    plt.savefig("subst_sensitivity_box.png")
    plt.show()

    swap_abs_sums = []
    for char in swap_sensitivity:
        swap_abs_sums.append(np.sum(np.abs(char)))
    plt.bar(range(len(swap_abs_sums) + 1)[1:], [x/sum(swap_abs_sums) for x in swap_abs_sums])
    plt.xticks(range(len(swap_abs_sums) + 1)[1:], labels=swap_pair_strings)
    plt.xlabel("Feature substitution")
    plt.ylabel("Average absolute effect on E prediction [V]")
    plt.savefig("rsubs_sensitivity_bar.png")
    plt.show()


    return

def investigate_testset_quinones(model, datafile, testsetfile, y_pred, y_test, insights=1, makenew=1):
    "Analyse Feature Map Representation method based on the testset quinones"
    infile = open(datafile, 'rb')
    data = pickle.load(infile)
    infile.close()

    infile = open(testsetfile, 'rb')
    testdata = pickle.load(infile)
    infile.close()

    scaler_y = data[-1][4]

    xtest = testdata[2][:5000]

    activations1 = []
    activations2 = []
    activations3 = []
    activations0 = []
    if makenew == 1:
        for i, x in enumerate(xtest):
            NN_plot_data = look_into_NN(model, datafile, [[x], testdata[0][i]], feature_operation=0, rem_none_ind=1)
            [pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita] = unpack_plot_data(NN_plot_data)
            activations0.append([pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita])

            NN_plot_data = look_into_NN(model, datafile, [[x], testdata[0][i]], feature_operation=1, rem_none_ind=1)
            [pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita] = unpack_plot_data(NN_plot_data)
            activations1.append([pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita])

            NN_plot_data = look_into_NN(model, datafile, [[x], testdata[0][i]], feature_operation=2, rem_none_ind=1)
            [pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita] = unpack_plot_data(NN_plot_data)
            activations2.append([pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita])

            NN_plot_data = look_into_NN(model, datafile, [[x], testdata[0][i]], feature_operation=3, rem_none_ind=1)
            [pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita] = unpack_plot_data(NN_plot_data)
            activations3.append([pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita])
        with open("testset_molecule_activations_coll", 'wb') as handle:
            pickle.dump([activations0, activations1, activations2, activations3], handle)

    else:
        infile = open("testset_molecule_activations_coll", 'rb')
        [activations0, activations1, activations2, activations3] = pickle.load(infile)
        infile.close()


    for num, activation in enumerate([activations0, activations1]):
        analyze_activations(activation, y_pred, y_test, num, equalize=1)
    for num, activation in enumerate([activations2, activations3]):
        analyze_activations(activation, y_pred, y_test, num+2, equalize=0)

    return


def analyze_activations(activations, y_pred, y_test, feature_operation, equalize=1):
    "Testing for various correlations with feature map representation"
    from scipy.stats import pearsonr, spearmanr

    #unpack inputs
    pos_act_1 = []
    pos_act_2 = []
    pos_act_3 = []
    mols = []
    for num, activation in enumerate(activations):
        pos_act_1.append(activation[0])
        pos_act_2.append(activation[1])
        pos_act_3.append(activation[2])
        mols.append(activation[4])
    preds = [x[0] for x in y_pred[:len(mols)]]
    vals = y_test[:len(mols)]

    # remove small molecules from data because equalizer function will fail
    small = []
    for i, mol in enumerate(mols):
        if len(mol) < 28:
            small.append(i)
    for i in small:
        mols.pop(i)
        preds.pop(i)
        vals.pop(i)
        pos_act_1.pop(i)
        pos_act_2.pop(i)
        pos_act_3.pop(i)

    #equalizer function to remove effect of non-encoding "background"
    if equalize == 1:
        new_pos_act_1 = equalizer(pos_act_1, 0)
        new_pos_act_2 = equalizer(pos_act_2, 1)
        new_pos_act_3 = equalizer(pos_act_3, 2)
    else:
        new_pos_act_1 = []
        new_pos_act_2 = []
        new_pos_act_3 = []
        for mol in pos_act_1:
            new_pos_act_1.append([x[0] for x in mol])
        for mol in pos_act_2:
            new_pos_act_2.append([x[0] for x in mol])
        for mol in pos_act_3:
            new_pos_act_3.append([x[0] for x in mol])

    #test for association of sum with model error or output
    act_1_sum = [np.average(x) for x in new_pos_act_1]
    act_2_sum = [np.average(x) for x in new_pos_act_2]
    act_3_sum = [np.average(x) for x in new_pos_act_3]
    mse = []
    mae = []
    mape = []
    for i, val in enumerate(vals):
        mse.append(metrics.mean_squared_error([val], [preds[i]]))
        mae.append(metrics.mean_absolute_error([val], [preds[i]]))
        mape.append(metrics.mean_absolute_percentage_error([val], [preds[i]]))
    logvals = list(np.log10(vals))
    logpreds = list(np.log10(preds))

    nans = []
    for i, pred in enumerate(logpreds):
        if np.isnan(pred) or np.isnan(logvals[i]) or np.isnan(act_1_sum[i]) or np.isnan(act_2_sum[i]) or np.isnan(act_3_sum[i]):
            nans.append(i)

    for i in sorted(nans, reverse=True):
        del mols[i]
        del preds[i]
        del vals[i]
        del logvals[i]
        del logpreds[i]
        del act_1_sum[i]
        del act_2_sum[i]
        del act_3_sum[i]
        del new_pos_act_1[i]
        del new_pos_act_2[i]
        del new_pos_act_3[i]
        del mse[i]
        del mae[i]
        del mape[i]




    mse_1_sum_pr = pearsonr(mse, act_1_sum)
    mse_2_sum_pr = pearsonr(mse, act_2_sum)
    mse_3_sum_pr = pearsonr(mse, act_3_sum)
    mae_1_sum_pr = pearsonr(mae, act_1_sum)
    mae_2_sum_pr = pearsonr(mae, act_2_sum)
    mae_3_sum_pr = pearsonr(mae, act_3_sum)
    mape_1_sum_pr = pearsonr(mape, act_1_sum)
    mape_2_sum_pr = pearsonr(mape, act_2_sum)
    mape_3_sum_pr = pearsonr(mape, act_3_sum)
    vals_1_sum_pr = pearsonr(vals, act_1_sum)
    vals_2_sum_pr = pearsonr(vals, act_2_sum)
    vals_3_sum_pr = pearsonr(vals, act_3_sum)
    logvals_1_sum_pr = pearsonr(logvals, act_1_sum)
    logvals_2_sum_pr = pearsonr(logvals, act_2_sum)
    logvals_3_sum_pr = pearsonr(logvals, act_3_sum)
    preds_1_sum_pr = pearsonr(preds, act_1_sum)
    preds_2_sum_pr = pearsonr(preds, act_2_sum)
    preds_3_sum_pr = pearsonr(preds, act_3_sum)
    logpreds_1_sum_pr = pearsonr(logpreds, act_1_sum)
    logpreds_2_sum_pr = pearsonr(logpreds, act_2_sum)
    logpreds_3_sum_pr = pearsonr(logpreds, act_3_sum)
    stat_names = ["mse_1_sum_pr", "mse_2_sum_pr", "mse_3_sum_pr", "mae_1_sum_pr", "mae_2_sum_pr", "mae_3_sum_pr", "mape_1_sum_pr", "mape_2_sum_pr", "mape_3_sum_pr", "vals_1_sum_pr", "vals_2_sum_pr", "vals_3_sum_pr", "logvals_1_sum_pr", "logvals_2_sum_pr", "logvals_3_sum_pr", "preds_1_sum_pr", "preds_2_sum_pr", "preds_3_sum_pr", "logpreds_1_sum_pr", "logpreds_2_sum_pr", "logpreds_3_sum_pr"]
    for i, stat in enumerate([mse_1_sum_pr, mse_2_sum_pr, mse_3_sum_pr, mae_1_sum_pr, mae_2_sum_pr, mae_3_sum_pr, mape_1_sum_pr, mape_2_sum_pr, mape_3_sum_pr, vals_1_sum_pr, vals_2_sum_pr, vals_3_sum_pr, logvals_1_sum_pr, logvals_2_sum_pr, logvals_3_sum_pr, preds_1_sum_pr, preds_2_sum_pr, preds_3_sum_pr, logpreds_1_sum_pr, logpreds_2_sum_pr, logpreds_3_sum_pr]):
        print(stat_names[i] + ": " + str(stat[0]) + "  p = " + str(stat[1]))



    # structural interpretation by comparing activation values for individual atoms
    uniquesarray = ['5', '+', 'F', 'l', 's', 'i', 'Br', '6', 'c', '1', '2', 'O', '=', '(', 'C', ')', 'P', 'H', '-', 'ö', '3', 'S', '4', 'o', 'N', '#', '7', '[', 'n', ']']
    elements = ["O", "C", "P", "c", "S", "N", "F", "H", "Br", "n", "o"]
    elements_en = [3.5, 2.5, 2.06, 2.5, 2.44, 3.07, 4.17, 2.2, 2.74, 3.07, 3.5]
    mols_atoms = []
    mols_atom_act_1 = []
    mols_atom_act_2 = []
    mols_atom_act_3 = []
    mols_en = []
    for molnum, mol in enumerate(mols):
        atoms = []
        atom_act_1 = []
        atom_act_2 = []
        atom_act_3 = []
        atom_en = []
        for charnum, char in enumerate(mol):
            if char in elements:
                atoms.append(char)
                atom_act_1.append(new_pos_act_1[molnum][charnum])
                atom_act_2.append(new_pos_act_2[molnum][charnum])
                atom_act_3.append(new_pos_act_3[molnum][charnum])
                atom_en.append(elements_en[elements.index(char)])
            elif char == "l": #correction for "Cl"
                atoms[-1] = "Cl"
                atom_act_1[-1] = (new_pos_act_1[molnum][charnum - 1] + new_pos_act_1[molnum][charnum]) / 2
                atom_act_2[-1] = (new_pos_act_2[molnum][charnum - 1] + new_pos_act_2[molnum][charnum]) / 2
                atom_act_3[-1] = (new_pos_act_3[molnum][charnum - 1] + new_pos_act_3[molnum][charnum]) / 2
                atom_en[-1] = 2.83
        mols_atoms.append(atoms)
        mols_atom_act_1.append(atom_act_1)
        mols_atom_act_2.append(atom_act_2)
        mols_atom_act_3.append(atom_act_3)
        mols_en.append(atom_en)

    pearson_en_act1 = []
    pearson_en_act2 = []
    pearson_en_act3 = []
    spearman_en_act1 = []
    spearman_en_act2 = []
    spearman_en_act3 = []


    pearson_en_act1_p = []
    pearson_en_act2_p = []
    pearson_en_act3_p = []
    spearman_en_act1_p = []
    spearman_en_act2_p = []
    spearman_en_act3_p = []

    for molnum, mol in enumerate(mols):
        pearson_en_act1.append(pearsonr(mols_en[molnum], mols_atom_act_1[molnum])[0])
        pearson_en_act2.append(pearsonr(mols_en[molnum], mols_atom_act_2[molnum])[0])
        pearson_en_act3.append(pearsonr(mols_en[molnum], mols_atom_act_3[molnum])[0])
        spearman_en_act1.append(spearmanr(mols_en[molnum], mols_atom_act_1[molnum])[0])
        spearman_en_act2.append(spearmanr(mols_en[molnum], mols_atom_act_2[molnum])[0])
        spearman_en_act3.append(spearmanr(mols_en[molnum], mols_atom_act_3[molnum])[0])
        pearson_en_act1_p.append(pearsonr(mols_en[molnum], mols_atom_act_1[molnum])[1])
        pearson_en_act2_p.append(pearsonr(mols_en[molnum], mols_atom_act_2[molnum])[1])
        pearson_en_act3_p.append(pearsonr(mols_en[molnum], mols_atom_act_3[molnum])[1])
        spearman_en_act1_p.append(spearmanr(mols_en[molnum], mols_atom_act_1[molnum])[1])
        spearman_en_act2_p.append(spearmanr(mols_en[molnum], mols_atom_act_2[molnum])[1])
        spearman_en_act3_p.append(spearmanr(mols_en[molnum], mols_atom_act_3[molnum])[1])
    print("Comparing values for each atom in molecule, averaged over molecule")
    print("Pearson_en_act1: " + str(np.average(pearson_en_act1)) + ", p = " + str(np.average(pearson_en_act1_p)))
    print("Pearson_en_act2: " + str(np.average(pearson_en_act2)) + ", p = " + str(np.average(pearson_en_act2_p)))
    print("Pearson_en_act3: " + str(np.average(pearson_en_act3)) + ", p = " + str(np.average(pearson_en_act3_p)))
    print("Spearman_en_act1: " + str(np.average(spearman_en_act1)) + ", p = " + str(np.average(spearman_en_act1_p)))
    print("Spearman_en_act2: " + str(np.average(spearman_en_act2)) + ", p = " + str(np.average(spearman_en_act2_p)))
    print("Spearman_en_act3: " + str(np.average(spearman_en_act3)) + ", p = " + str(np.average(spearman_en_act3_p)))
    print("Comparing values for atoms all over data set")
    global_pearson_en_act1 = pearsonr([j for i in mols_en for j in i], [j for i in mols_atom_act_1 for j in i])
    global_pearson_en_act2 = pearsonr([j for i in mols_en for j in i], [j for i in mols_atom_act_2 for j in i])
    global_pearson_en_act3 = pearsonr([j for i in mols_en for j in i], [j for i in mols_atom_act_3 for j in i])
    global_spearman_en_act1 = spearmanr([j for i in mols_en for j in i], [j for i in mols_atom_act_1 for j in i])
    global_spearman_en_act2 = spearmanr([j for i in mols_en for j in i], [j for i in mols_atom_act_2 for j in i])
    global_spearman_en_act3 = spearmanr([j for i in mols_en for j in i], [j for i in mols_atom_act_3 for j in i])
    stats2 = [global_pearson_en_act1, global_pearson_en_act2, global_pearson_en_act3, global_spearman_en_act1, global_spearman_en_act2, global_spearman_en_act3]
    stat_names2 = ["global_pearson_en_act1", "global_pearson_en_act2", "global_pearson_en_act3", "global_spearman_en_act1", "global_spearman_en_act2", "global_spearman_en_act3"]
    for i, stat in enumerate(stats2):
        print(stat_names2[i] + ": " + str(stat[0]) + "  p = " + str(stat[1]))

    keto_g = []
    alkyl_g = []
    hydroxyl_g = []
    caboxyle_g = []
    amino_g = []
    amide_g = []
    nitrile_g = []
    sulfide_g = []
    sulfonic_g = []

    #testing for functional groups
    from rdkit import Chem
    for molnum, mol in enumerate(mols):
        rdkmol = Chem.MolFromSmiles("".join(mol))

        keto_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmiles("C(=O)")))
        alkyl_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmarts("[CX4]")))
        hydroxyl_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmarts("[OX2H]")))
        caboxyle_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmarts("[CX3](=[OX1])O")))
        amino_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmarts("[NX3;H2,H1;!$(NC=O)]")))
        amide_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmarts("[OX1]=CN")))
        nitrile_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmarts("[NX1]#[CX2]")))
        #sulfide_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmarts("[#16X2H0][!#16]")))
        sulfonic_g.append(rdkmol.GetSubstructMatches(Chem.MolFromSmarts("[$([#16X4](=[OX1])(=[OX1])([#6])[OX2H,OX1H0-]),$([#16X4+2]([OX1-])([OX1-])([#6])[OX2H,OX1H0-])]")))

    # associations of groups with mesomeric/inductive effects
    func_group_names = ["keto", "alkyl", "hydroxyl", "carboxyle", "amino", "amide", "nitrile", "sulfonic"]
    func_groups = [keto_g, alkyl_g, hydroxyl_g, caboxyle_g, amino_g, amide_g, nitrile_g, sulfonic_g]
    inductive_rank = [-3, 1, 3, -5, -9, 2, -7, -8]

    group_ranks = []
    group_acts1 = []
    group_acts2 = []
    group_acts3 = []

    for groupnum, func_group in enumerate(func_groups):
        for molnum, groupmol in enumerate(func_group):
            for atoms in groupmol:
                acts1 = 0
                acts2 = 0
                acts3 = 0
                for atom in atoms:
                    acts1 = acts1 + mols_atom_act_1[molnum][atom]
                    acts2 = acts2 + mols_atom_act_2[molnum][atom]
                    acts3 = acts3 + mols_atom_act_3[molnum][atom]
                group_ranks.append(inductive_rank[groupnum])
                group_acts1.append(acts1/len(atoms))
                group_acts2.append(acts2 / len(atoms))
                group_acts3.append(acts3 / len(atoms))
    print("Testing for rank-based correlations based on inductive effects of groups")
    print("Spearman r: func_groups; act1: " + str(spearmanr(group_ranks, group_acts1)[0]) + "  p = " + str(spearmanr(group_ranks, group_acts1)[1]))
    print("Spearman r: func_groups; act2: " + str(spearmanr(group_ranks, group_acts2)[0]) + "  p = " + str(spearmanr(group_ranks, group_acts2)[1]))
    print("Spearman r: func_groups; act3: " + str(spearmanr(group_ranks, group_acts3)[0]) + "  p = " + str(spearmanr(group_ranks, group_acts3)[1]))



    if feature_operation == 0:
        plt.scatter(mse, act_2_sum, s=0.1, label="c = -0.2034  p = 1.1583e-45")
        plt.xlabel("Prediction MSE")
        plt.ylabel("Feature Map Activation Sum Type 0 - C-Layer 2")
        plt.xlim([0, 0.3])
        plt.legend()
        plt.savefig("fo0_mse_sum2.png")
        plt.show()

        plt.scatter(mae, act_1_sum, s=0.1, label="-0.1974  p = 4.5048e-43")
        plt.xlabel("Prediction MAE")
        plt.ylabel("Feature Map Activation Sum Type 0 - C-Layer 1")
        plt.xlim([0, 0.6])
        plt.legend()
        plt.savefig("fo0_mae_sum1.png")
        plt.show()

        plt.scatter(mae, act_2_sum, s=0.1, label="c = -0.2639  p = 1.0198e-76")
        plt.xlabel("Prediction MAE")
        plt.ylabel("Feature Map Activation Sum Type 0 - C-Layer 2")
        plt.xlim([0, 0.6])
        plt.legend()
        plt.savefig("fo0_mae_sum2.png")
        plt.show()

        plt.scatter(preds, act_3_sum, s=0.1, label="c = -0.1944  p = 8.2572e-42")
        plt.xlabel("Prediction")
        plt.ylabel("Feature Map Activation Sum Type 0 - C-Layer 3")
        # plt.xlim([0, 0.3])
        plt.legend()
        plt.savefig("fo0_pred_sum3.png")
        plt.show()

        plt.scatter([j for i in mols_en for j in i], [j for i in mols_atom_act_1 for j in i], s=0.1)
        plt.xlabel("Electronegativity of Atom")
        plt.ylabel("Feature Map Atom Activation Type 0 - C-Layer 1")
        # plt.xlim([0, 0.3])
        plt.savefig("fo0_en_sum1.png")
        plt.show()

        boxplotdata = []
        for n in sorted(elements_en):
            boxplotdata.append([mols_atom_act_1[ni][nj] for ni, i in enumerate(mols_en) for nj, j in enumerate(i) if j == n])
        plt.boxplot(boxplotdata, positions=sorted(elements_en), widths=0.05)
        #plt.text(0, 0, "c = 0.1899  p = 0.0", ha='center', va='center', color='navy')
        plt.xticks(sorted(elements_en), [str(x) for x in sorted(elements_en)])
        plt.xlabel("Electronegativity of Individual Atom")
        plt.ylabel("Feature Map Activation Type 0 of Individual Atom")
        plt.savefig("fo0_en_sum1_box.png")
        plt.show()

        boxplotdata2 = []
        for n in sorted(elements_en):
            boxplotdata2.append([mols_atom_act_1[ni][nj]-act_1_sum[ni] for ni, i in enumerate(mols_en) for nj, j in enumerate(i) if j == n])
        plt.boxplot(boxplotdata2, positions=sorted(elements_en), widths=0.05)
        #"0.2065, p = 0.3596371451494978"+
        plt.xticks(sorted(elements_en), [str(x) for x in sorted(elements_en)])
        plt.xlabel("Electronegativity of Individual Atom")
        plt.ylabel("Feature Map Activation Type 0 of Individual Atom - average activation in molecule")
        plt.savefig("fo0_en_sum1_box2.png")
        plt.show()

    if feature_operation == 1:
        plt.scatter(mse, act_2_sum, s=0.1, label="c = -0.2099  p = 1.4579e-48")
        plt.xlabel("Prediction MSE")
        plt.ylabel("Feature Map Activation Sum Type 1 - C-Layer 2")
        plt.xlim([0, 0.3])
        plt.legend()
        plt.savefig("fo1_mse_sum2.png")
        plt.show()

        plt.scatter(mae, act_1_sum, s=0.1, label="c = -0.2267  p = 1.4008e-56")
        plt.xlabel("Prediction MAE")
        plt.ylabel("Feature Map Activation Sum Type 1 - C-Layer 1")
        plt.xlim([0, 0.6])
        plt.legend()
        plt.savefig("fo1_mae_sum1.png")
        plt.show()

        plt.scatter(mae, act_2_sum, s=0.1, label="c = -0.2744  p = 5.0784e-83")
        plt.xlabel("Prediction MAE")
        plt.ylabel("Feature Map Activation Sum Type 1 - C-Layer 2")
        plt.xlim([0, 0.6])
        plt.legend()
        plt.savefig("fo1_mae_sum2.png")
        plt.show()

        plt.scatter(preds, act_3_sum, s=0.1, label="c = -0.2106  p = 6.6762e-49")
        plt.xlabel("Prediction")
        plt.ylabel("Feature Map Activation Sum Type 1 - C-Layer 3")
        # plt.xlim([0, 0.3])
        plt.legend()
        plt.savefig("fo1_pred_sum3.png")
        plt.show()

        plt.scatter([j for i in mols_en for j in i], [j for i in mols_atom_act_1 for j in i], s=0.1)
        plt.xlabel("Electronegativity of Atom")
        plt.ylabel("Feature Map Atom Activation Type 1 - C-Layer 1")
        # plt.xlim([0, 0.3])
        plt.savefig("fo1_en_sum1.png")
        plt.show()

        boxplotdata = []
        for n in sorted(elements_en):
            boxplotdata.append(
                [mols_atom_act_1[ni][nj] for ni, i in enumerate(mols_en) for nj, j in enumerate(i) if j == n])
        plt.boxplot(boxplotdata, positions=sorted(elements_en), widths=0.05)
        # plt.text(0, 0, "c = 0.1899  p = 0.0", ha='center', va='center', color='navy')
        plt.xticks(sorted(elements_en), [str(x) for x in sorted(elements_en)])
        plt.xlabel("Electronegativity of Individual Atom")
        plt.ylabel("Feature Map Activation Type 1 of Individual Atom")
        plt.legend()
        plt.savefig("fo1_en_sum1_box.png")
        plt.show()

        boxplotdata2 = []
        for n in sorted(elements_en):
            boxplotdata2.append(
                [mols_atom_act_1[ni][nj] - act_1_sum[ni] for ni, i in enumerate(mols_en) for nj, j in enumerate(i) if
                 j == n])
        plt.boxplot(boxplotdata2, positions=sorted(elements_en), widths=0.05)
        # "0.2065, p = 0.3596371451494978"+
        plt.xticks(sorted(elements_en), [str(x) for x in sorted(elements_en)])
        plt.xlabel("Electronegativity of Individual Atom")
        plt.ylabel("Feature Map Activation Type 1 of Individual Atom - average activation in molecule")
        plt.legend()
        plt.savefig("fo1_en_sum1_box2.png")
        plt.show()

    if feature_operation == 3:

        plt.scatter(preds, act_2_sum, s=0.1, label="c = -0.2243  p = 2.2059e-55")
        plt.xlabel("Prediction")
        plt.ylabel("Feature Map Activation Sum Type 3 - C-Layer 2")
        # plt.xlim([0, 0.3])
        plt.legend()
        plt.savefig("fo3_pred_sum2.png")
        plt.show()


    return

def equalizer(pos_act, layernum):
    "Function to eliminate the effect of the non-encoding parts at the molecule ends"
    new_pos_act = []
    for act in pos_act:
        if layernum == 0:
            avg_center = np.average(act[12:-7])
            front_eq = [x*avg_center for x in [1, 1, 1, 1, 1, 5/6, 5/7, 5/8, 4/8, 3/8, 2/8, 1/8]]
            end_eq = [x*avg_center for x in [1/8, 2/8, 3/8, 4/8, 5/8, 6/8, 7/8]]
            new_act = []
            for i, val in enumerate(act):
                if i < 12:
                    new_act.append(val[0] + front_eq[i])
                elif i >= (len(act) - 7):
                    new_act.append(val[0] + end_eq[i - (len(act) - 7)])
                else:
                    new_act.append(val[0])
            new_pos_act.append(new_act[5:])
        elif layernum == 1:
            avg_center = np.average(act[15:-10])
            front_eq = [x * avg_center for x in [1, 1, 1, 1, 1, 5/6, 5/7, 5/8, 5/9, 5/10, 5/11, 4/11, 3/11, 2/11, 1/11]]
            end_eq = [x * avg_center for x in [1/11, 2/11, 3/11, 4/11, 5/11, 6/11, 7/11, 8/11, 9/11, 10/11]]
            new_act = []
            for i, val in enumerate(act):
                if i < 12:
                    new_act.append(val[0] + front_eq[i])
                elif i >= (len(act) - 10):
                    new_act.append(val[0] + end_eq[i - (len(act) - 10)])
                else:
                    new_act.append(val[0])
            new_pos_act.append(new_act[5:])
        else:
            avg_center = np.average(act[16:-11])
            front_eq = [x * avg_center for x in [1, 1, 1, 1, 1, 5/6, 5/7, 5/8, 5/9, 5/10, 5/11, 5/12, 4/12, 3/12, 2/12, 1/12]]
            end_eq = [x * avg_center for x in [1/12, 2/12, 3/12, 4/12, 5/12, 6/12, 7/12, 8/12, 9/12, 10/12, 11/12]]
            new_act = []
            for i, val in enumerate(act):
                if i < 12:
                    new_act.append(val[0] + front_eq[i])
                elif i >= (len(act) - 11):
                    new_act.append(val[0] + end_eq[i - (len(act) - 11)])
                else:
                    new_act.append(val[0])
            new_pos_act.append(new_act[5:])

    return new_pos_act

def unpack_plot_data(data):
    try:
        [testa, layer_avgs1a, [pos_activation1a, test_string_splita], testb, layer_avgs1b, [pos_activation1b, test_string_splitb], layer_avgs2a, [pos_activation2a, test_string_splita], layer_avgs2b, [pos_activation2b, test_string_splitb], layer_avgs3a, [pos_activation3a, test_string_splita], layer_avgs3b, [pos_activation3b, test_string_splitb]] = data
        return [pos_activation1a, pos_activation1b, pos_activation2a, pos_activation2b, pos_activation3a,
                pos_activation3b, 0, 0, test_string_splita, test_string_splitb]

    except: #only one molecule as input (not generated quinones)
        [testa, layer_avgs1a, [pos_activation1a, test_string_splita], layer_avgs2a, [pos_activation2a, test_string_splita],
         layer_avgs3a, [pos_activation3a, test_string_splita]] = data
        return [pos_activation1a, pos_activation2a, pos_activation3a, 0, test_string_splita]

def investigate_generated_quinones(model, datafile, insights=1, makenew=0):
    "Synthetically generate quinone structures fpr investigation"
    import altern_smiles
    from sklearn.preprocessing import OneHotEncoder
    import prepare_2d_nn_input

    if makenew==0:
        infile = open("testset_molecule_activations", 'rb')
        [activations, preds, keys, fgroups] = pickle.load(infile)
        infile.close()
        make_molecule_activation_plot(activations, preds)

        make_generated_prediction_plot(keys, fgroups, preds)

        make_molecule_activation_plot(activations, preds)

        return



    infile = open(datafile, 'rb')
    data = pickle.load(infile)
    infile.close()

    xtest = data[-1][1]
    scaler_y = data[-1][4]

    smiles_group1 = ["O=c2ccc1ccccc1c2=O", "Cc2cc(=O)c1ccccc1c2=O", "Cc2ccc1c(=O)ccc(=O)c1c2", "Cc1cccc2c(=O)ccc(=O)c12"]
    smiles_group2 = ["O=c1ccc(=O)c2ccccc12", "O=c1cc(O)c(=O)c2ccccc12", "O=c1ccc(=O)c2cc(O)ccc12", "O=c1ccc(=O)c2c(O)cccc12"]

    import quinone_generator
    fgroups = ["(O)", "(C)", "(N)", "(N=O)", "(N(O)O)"]
    #ortho- /para (keto group influence
    # extra keto gruppe
    # NO2
    # darstellung relativ zum original
    smiles_groups = []
    for group in fgroups:
        smiles_groups.append(quinone_generator.generate_exp1(group)[0])

    keys = []
    keys.append(quinone_generator.generate_exp1(fgroups[0])[1])


    extended_group1 = [smiles_group1]
    extended_group2 = [smiles_group2]

    for smil in smiles_group1:
        extended_group1.append(altern_smiles.get_x_random_smiles_alternations(smil, 10))
    for smil in smiles_group2:
        extended_group2.append(altern_smiles.get_x_random_smiles_alternations(smil, 10))

    #uniquesarray = ['O', '=', 'C', '1', '(', 'P', ')', 'c', '2', 'ö', 'S', '3', '-', '[', 'n', '+', ']', '4', 'N', 'F', 'H', '5', '#', 's', 'l', 'o', 'i', 'Br']


    #feature_shift
    uniquesarray = ['5', '+', 'F', 'l', 's', 'i', 'Br', '6', 'c', '1', '2', 'O', '=', '(', 'C', ')', 'P', 'H', '-', 'ö', '3', 'S', '4', 'o', 'N', '#', '7', '[', 'n', ']']
    activations = []
    changes = []
    preds = []
    for groupnum, subgroup in enumerate(smiles_groups):
        alldata = []
        alldata_comp = []
        for group in subgroup:
            for smil in group:
                smildata = ['ö', 'ö', 'ö', 'ö', 'ö']
                for letter in smil:
                    smildata.append(letter)
                while len(smildata) < 145:
                    smildata.append("ö")
                alldata_comp.append(smil)
                alldata.append(smildata)

        [smiles_group1, smiles_group2] = subgroup

        ohe = OneHotEncoder(categories=[uniquesarray for x in range(145)], dtype=bool, sparse=False)  # apply one-hot-encoding on all chars
        all_ohe = ohe.fit_transform(alldata)

        # reshape to 2- or 3-dim input
        X = prepare_2d_nn_input.reshape_dimensions(all_ohe, [len(all_ohe), 145, len(uniquesarray)], 2)

        new_pred = model.predict(X)
        new_pred = MinMaxScaler.inverse_transform(scaler_y, new_pred)

        trueX = np.array(X)
        truesmil = smiles_group1+smiles_group2

        # newtruesmil = []
        # for smile in truesmil:
        #     newtruesmil.append("ööööö" + smile)
        # truesmil = newtruesmil

        or_pred = new_pred[:len(smiles_group1)]
        alt_pred = new_pred[len(smiles_group1):]
        for i in range(len(or_pred)):
            changes.append(alt_pred[i][0] - or_pred[i][0])
            preds.append([or_pred[i][0], alt_pred[i][0]])

        for ind in range(int(len(truesmil)/2)):
            x = np.array([trueX[ind], trueX[ind+int(len(truesmil)/2)]])
            smil = [truesmil[ind], truesmil[ind+int(len(truesmil)/2)]]

            if insights == 1:
                NN_plot_data = look_into_NN(model, datafile, [x, smil])

            if insights == 1:
                [pos_activation1a, pos_activation1b, pos_activation2a, pos_activation2b, pos_activation3a, pos_activation3b, m, m2, test_string_splita, test_string_splitb] = plot_NN_insights(NN_plot_data, groupnum)
                activations.append([pos_activation1a, pos_activation1b, pos_activation2a, pos_activation2b, pos_activation3a, pos_activation3b, m, m2, test_string_splita, test_string_splitb])


    if insights == 1:
        if makenew == 1:
            with open("testset_molecule_activations", 'wb') as handle:
                pickle.dump([activations, preds, keys, fgroups], handle)

    make_generated_prediction_plot(keys, fgroups, preds)

    make_molecule_activation_plot(activations, preds)


    return

def make_quinone_exp_comparison_plot(datafile, model):
    "Compare CNN predictions with literature data from experiments."
    from sklearn.preprocessing import OneHotEncoder
    import prepare_2d_nn_input
    from rdkit import Chem



    smiles_H2O = ["C1=CC(=O)C=CC1=O", "C1=CC=C2C(=C1)C=CC(=O)C2=O", "OC1=CC=CC2=C1C(=O)C=CC2=O", "C1=CC=C2C(=C1)C3=CC=CC=C3C(=O)C2=O",
                  "C1=CC=C2C(=O)C=CC(=O)C2=C1", "CC1=CC(=O)C2=CC=CC=C2C1=O", "C1=CC=C2C(=C1)C(=CC(=O)C2=O)O",
                  "O=C1c2ccccc2C(=O)c3ccccc13"]

    smiles_DMF = ["OC1=CC=CC2=C1C(=O)C=CC2=O", "C1=CC(=O)C=CC1=O", "OC1=CC=CC2=C1C(=O)C=CC2=O", "C1=CC=C2C(=C1)C3=CC=CC=C3C(=O)C2=O",
                  "C1=CC=C2C(=O)C=CC(=O)C2=C1", "CC1=CC(=O)C2=CC=CC=C2C1=O", "O=c2c1ccccc1c(=O)c4c2ccc3ccccc34",
                  "O=c1c(=O)c3cccc2cccc1c23", "CC1=CC2=C(C=C1)C(=O)C3=CC=CC=C3C2=O", "O=C1c2ccccc2C(=O)c3ccccc13"]

    # normal exp. comparison experiment
    #smiles_all = ["C1=CC(=O)C=CC1=O", "C1=CC=C2C(=C1)C=CC(=O)C2=O", "OC1=CC=CC2=C1C(=O)C=CC2=O", "C1=CC=C2C(=C1)C3=CC=CC=C3C(=O)C2=O", "C1=CC=C2C(=O)C=CC(=O)C2=C1", "CC1=CC(=O)C2=CC=CC=C2C1=O", "O=C1c2ccccc2C(=O)c3ccccc13", "C1=CC=C2C(=C1)C(=CC(=O)C2=O)O", "O=c2c1ccccc1c(=O)c4c2ccc3ccccc34", "O=c1c(=O)c3cccc2cccc1c23", "CC1=CC2=C(C=C1)C(=O)C3=CC=CC=C3C2=O"]
    #smiles_all = ["C1=CC(=O)C=CC1=O", "C1=CC=C2C(=C1)C=C3C=CC=C4C3=C2C(=O)C4=O", "C1=CC2=C3C(=C1)C(=O)C(=O)C3=CC=C2", "C1=CC=C2C(=C1)C=CC(=O)C2=O", "C1=CC=C2C=C3C(=O)C=CC(=O)C3=CC2=C1", "C1=CC=C2C(=C1)C=CC3=C2C=CC4=C3C(=O)C=CC4=O", "C1=CC=C2C(=O)C=CC(=O)C2=C1", "C1=CC=C2C(=C1)C=CC3=C2C(=O)C=CC3=O", "C1=CC=C2C(=C1)C3=CC=C4C(=O)C=CC5=C4C3=C(C2=O)C=C5", "CC1=CC(=O)C(=CC1=O)C", "CC1=CC(=O)C=C(C1=O)C", "CC1=CC(=O)C=CC1=O", "Cc2cc(=O)c1ccccc1c2=O", "CC1=CC2=C(C=C1)C(=O)C3=CC=CC=C3C2=O", "C1=CC=C2C(=C1)C(=CC(=O)C2=O)O", "C1=CC=C2C3=C4C(=CC2=C1)C(=O)C(=O)C5=CC=CC(=C54)C=C3", "C1=CC2=C3C(=C1)C(=O)C(=O)C4=CC=CC(=C43)C=C2", "C1=CC=C2C=C3C(=CC2=C1)C(=O)C4=CC=CC=C4C3=O", "C1=CC=C2C(=C1)C=CC3=C2C(=O)C(=O)C4=CC=CC=C34", "Oc1cccc2C(=O)C=CC(=O)c12", "C1=CC=C2C(=C1)C3=CC(=O)C4=CC=CC5=C4C3=C(C2=O)C=C5", "C1=CC=C2C(=C1)C=CC3=C2C(=O)C4=CC=CC=C4C3=O", "C1=CC=C2C(=C1)C(=O)C3=CC=CC=C3C2=O", "C1=CC=C2C(=C1)C3=CC=CC=C3C(=O)C2=O", "CC1=C(C)C(=O)C(C)=C(C)C1=O", "C1=CC=C2C(=C1)C3=CC=CC4=C3C(=CC=C4)C2=O", "C1=CC=C2C(=C1)C=CC3=C2C(=O)C4=CC=CC=C34", "O=C1c2ccccc2-c2cc3ccccc3cc21", "C1C2=CC=CC3=C2C(=CC=C3)C1=O", "O=C1C=Cc2cccc3cccc1c23", "C1=CC2=C3C(=C1)C(=O)C4=CC=CC5=C4C3=C(C=C2)C=C5", "C1=CC=C2C(=C1)C3=CC=CC=C3C2=O", "C1=CC=C2C(=C1)C=C(C=C2[N+](=O)[O-])[N+](=O)[O-]", "C1=CC2=C3C(=C1)C=CC4=C(C=C(C(=C43)C=C2)[N+](=O)[O-])[N+](=O)[O-]", "C1=CC=C2C(=C1)C=CC=C2[N+](=O)[O-]", "C1=CC2=C3C(=C1)C=CC4=C(C=CC(=C43)C=C2)[N+](=O)[O-]", "C1C2=C(C=CC(=C2)[N+](=O)[O-])C3=C1C=C(C=C3)[N+](=O)[O-]", "C1=CC=C2C=C(C=CC2=C1)[N+](=O)[O-]", "C1=CC=C2C(=C1)C3=C4C2=CC=CC4=C(C=C3)[N+](=O)[O-]", "C1=CC=C2C(=C1)C=CC3=C2C=C(C4=CC=CC=C34)[N+](=O)[O-]", "C1=CC=C2C(=C1)C=C(C3=CC=CC=C23)[N+](=O)[O-]", "C1=CC=C2C(=C1)C3=C4C(=C(C=C3)[N+](=O)[O-])C=CC=C4C2=O", "C1=CC=C2C(=C1)C3=C(C2=O)C=C(C=C3)[N+](=O)[O-]", "C1=CC2=C(C=C1[N+](=O)[O-])C(=O)C3=C2C=CC(=C3)[N+](=O)[O-]", "C1=CC=C2C(=C1)C(=CC=C2O)O", "C1=CC(=CC=C1O)O", "C1=CC2=C3C(=C1)C(=O)OC(=O)C3=CC=C2", "C1=CC=C(C=C1)C(=O)C2=CC=CC=C2"]
    # smiles_all = ["c1ccc2c(c1)cc3cccc4c3c2C(=O)C4=O", "C1=CC2=C3C(=C1)C(=O)C(=O)C3=CC=C2", "C1=CC=C2C(=C1)C=CC3=C2C=CC4=C3C=CC(=O)C4=O", "C1=CC=C2C(=C1)C=CC(=O)C2=O", "C1=CC=C2C=C3C(=O)C=CC(=O)C3=CC2=C1", "C1=CC(=O)C=CC1=O", "C1=CC=C2C(=C1)C=CC3=C2C=CC4=C3C(=O)C=CC4=O", "C1=CC=C2C(=O)C=CC(=O)C2=C1", "C1=CC=C2C(=C1)C=CC3=C2C(=O)C=CC3=O", "C1=CC2=C3C(=CC=C4C3=C1C=CC4=O)C=CC2=O",
    #               "C1=CC2=C3C(=CC=C4C3=C1C=CC4=O)C(=O)C=C2", "C1=CC=C2C(=C1)C(=O)C3=C(C2=O)C(=CC=C3)O", "CC1=CC2=C(C=C1C)C(=O)C3=CC=CC=C3C2=O", "CC1=CC(=O)C(=CC1=O)C", "CC1=CC(=O)C=C(C1=O)C", "CCC1=CC2=C(C=C1)C(=O)C3=CC=CC=C3C2=O", "C1=CC=C2C(=C1)C(=CC(=O)C2=O)O", "CC1=CC(=O)C=CC1=O", "CC1=CC(=O)C2=CC=CC=C2C1=O", "CC1=CC2=C(C=C1)C(=O)C3=CC=CC=C3C2=O",
    #               "C1=CC2=C3C(=C1)C(=O)C(=O)C4=CC=CC(=C43)C=C2", "C1=CC=C2C=C3C(=CC2=C1)C(=O)C4=CC=CC=C4C3=O", "C1=CC=C2C(=C1)C=CC3=C2C(=O)C(=O)C4=CC=CC=C34", "COc2ccc1c(=O)c(=O)cc(O)c1c2OC", "OC1=CC=CC2=C1C(=O)C=CC2=O", "C1=CC=C2C(=C1)C=CC3=C2C(=O)C4=CC=CC=C4C3=O", "O=C1C2=CC=CC=C2C(=O)C2=CC=CC=C12", "C1=CC=C2C(=C1)C3=CC=CC=C3C(=O)C2=O", "C1=CC=C2C(=C1)C3=CC=C4C(=O)C=CC5=C4C3=C(C2=O)C=C5", "C1=CC=C2C(=C1)C=C3C=CC4=C5C3=C2C(=O)C(=O)C5=CC=C4",
    #               "C1=CC=C2C(=C1)C3=C4C(=CC=C5C4=C(C=C3)C=CC5=O)C2=O", "C1=CC=C2C3=C4C(=CC2=C1)C(=O)C(=O)C5=CC=CC(=C54)C=C3", "C1=CC=C2C(=C1)C3=CC(=O)C4=CC=CC5=C4C3=C(C2=O)C=C5", "O=c1ccc(=O)c2c1cc3ccc4cccc5ccc2c3c45", "C1=CC2=C3C(=C1)C=CC4=C3C(=CC5=C4C=CC(=O)C5=O)C=C2", "C1=CC=C2C(=C1)C=CC3=C2C4=C(C=C3)C(=O)C=CC4=O", "C1=CC=C2C(=C1)C=CC3=C2C4=CC=CC=C4C(=O)C3=O", "O=c2c(=O)c4cccc5c1ccccc1c3cccc2c3c45", "C1=CC=C2C(=C1)C=CC3=CC4=C(C=C32)C(=O)C(=O)C5=CC=CC=C54",
    #               "O=c3c2ccc1ccccc1c2c(=O)c5c3ccc4ccccc45", "C1=CC=C2C=C3C(=CC2=C1)C4=C5C3=CC(=O)C(=O)C5=CC=C4", "CC1=C(C(=O)C(=C(C1=O)C)C)C", "C1=CC2=C3C(=C1)C(=O)C4=C5C3=C(C=C2)C(=O)C6=CC=CC(=C65)C=C4", "C1=CC=C2C=C3C(=CC2=C1)C(=O)C4=CC5=CC=CC=C5C=C4C3=O", "O=c2c(=O)c1c(O)cccc1c3cccc(O)c23", "C1C(=O)C2=CC=CC=C2C1=O", "O=c1c(=O)c3ccc4ccc5cc2cccc1c2c3c45", "O=c2c(O)cc3ccc4c(=O)c1ccccc1c5ccc2c3c45", "C1=CC2=C3C(=C1)C=CC4=C3C(=CC5=C4C=CC(=O)C5=O)C=C2", "C1=CC=C2C(=C1)C=CC(=O)C2=O", "C1=CC=C2C(=O)C=CC(=O)C2=C1", "C1=CC=C2C(=C1)C=CC3=C2C=CC(=O)C3=O",
    #               "C1=CC=C2C(=C1)C3=CC=C4C(=O)C=CC5=C4C3=C(C2=O)C=C5", "C1=CC=C2C(=C1)C3=C4C(=CC=C5C4=C(C=C3)C=CC5=O)C2=O", "C1=CC=C2C(=C1)C3=CC(=O)C4=CC=CC5=C4C3=C(C2=O)C=C5", "C1=CC(=O)C=CC1=O", "C1=CC(=O)C(=CC1=O)O", "C1=CC=C2C=C3C(=CC2=C1)C=CC4=C3C=CC(=O)C4=O", "Cc3cc1ccccc1c4ccc2c(=O)c(=O)ccc2c34", "Cc3cc1c(=O)c(=O)ccc1c4ccc2ccccc2c34"]
    smiles_all = ["C1=CC(=O)C=CC1=O", "C1=CC=C2C(=C1)C=CC(=O)C2=O", "OC1=CC=CC2=C1C(=O)C=CC2=O", "C1=CC=C2C(=C1)C3=CC=CC=C3C(=O)C2=O",
                  "C1=CC=C2C(=O)C=CC(=O)C2=C1", "CC1=CC(=O)C2=CC=CC=C2C1=O", "O=C1c2ccccc2C(=O)c3ccccc13",
                  "C1=CC=C2C(=C1)C(=CC(=O)C2=O)O", "O=c2c1ccccc1c(=O)c4c2ccc3ccccc34", "O=c1c(=O)c3cccc2cccc1c23", "CC1=CC2=C(C=C1)C(=O)C3=CC=CC=C3C2=O"]

    smiles_new = []



    for quin in smiles_all:
        mol = Chem.MolFromSmiles(quin)
        new_quin1 = Chem.MolToSmiles(mol)
        smiles_new.append(new_quin1)

    infile = open(datafile, 'rb')
    data = pickle.load(infile)
    infile.close()

    scaler_y = data[-1][4]

    # feature_shift
    uniquesarray = ['5', '+', 'F', 'l', 's', 'i', 'Br', '6', 'c', '1', '2', 'O', '=', '(', 'C', ')', 'P', 'H', '-', 'ö',
                    '3', 'S', '4', 'o', 'N', '#', '[', 'n', ']']

    preds = []
    alldata = []
    alldata_comp = []
    for smil in smiles_new:
        smildata = ['ö', 'ö', 'ö', 'ö', 'ö']
        for letter in smil:
            smildata.append(letter)
        while len(smildata) < 145:
            smildata.append("ö")
        alldata_comp.append(smil)
        alldata.append(smildata)


    ohe = OneHotEncoder(categories=[uniquesarray for x in range(145)], dtype=bool,
                            sparse=False)  # apply one-hot-encoding on all chars
    all_ohe = ohe.fit_transform(alldata)

    # reshape to 2- or 3-dim input
    X = prepare_2d_nn_input.reshape_dimensions(all_ohe, [len(all_ohe), 145, len(uniquesarray)], 2)

    new_pred = model.predict(X)
    new_pred = MinMaxScaler.inverse_transform(scaler_y, new_pred)


    # exp. data comparison plot
    quins_H2O = ["1,4-Benzoquinone", "1,2-Naphthoquinone", "5-Hydroxy-1,4-naphthoquinone", "9,10-Phenanthrenequinone",
                 "1,4-Naphthoquinone", "2-Methyl-1,4-naphthoquinone", "2-Hydroxy-1,4-naphthoquinone",
                 "9,10-Anthraquinone"]
    #preds_H2O = [groups_preds[1][0], groups_preds[5][0], groups_preds[6][1], groups_preds[21][0], groups_preds[6][0],
    #             groups_preds[6][2], groups_preds[6][1], groups_preds[8][0], groups_preds[26][5]]
    exp_H2O = [88.5, -89, -93.3, -122, -139, -196.3, -392, -445]
    uncert_H2O = [9.5, 0, 2.2, 2.2, 10.3, 14.6, 45.9, 0]

    quins_DMF = ["1,4-Benzoquinone", "1,2-Naphthoquinone", "5-Hydroxy-1,4-naphthoquinone", "9,10-Phenanthrenequinone",
                 "1,4-Naphthoquinone", "2-Methyl-1,4-naphthoquinone", "7,12-Benz(a)anthracenequinone",
                 "1,2-Acenaphthenequinone", "2-Methyl-9,10-Anthraquinone", "9,10-Anthraquinone"]
    # preds_DMF = [groups_preds[6][1], groups_preds[1][0], groups_preds[5][0], groups_preds[21][0], groups_preds[6][2],
    #              groups_preds[25][0], groups_preds[18][0], groups_preds[8][2], groups_preds[8][0], groups_preds[26][5]]
    exp_DMF = [-426.6, -473.0, -390, -579.0, -600.3, -672.0, -673.0, -841.0, -869.0, -904.7]
    uncert_DMF = [37.2, 0, 0, 0, 37.2, 31.4, 0, 0, 46.7, 98.1]

    allmols = ["1,4-Benzoquinone", "1,2-Naphthoquinone", "5-Hydroxy-1,4-naphthoquinone", "9,10-Phenanthrenequinone",
                 "1,4-Naphthoquinone", "2-Methyl-1,4-naphthoquinone", "9,10-Anthraquinone",
               "2-Hydroxy-1,4-naphthoquinone", "7,12-Benz(a)anthracenequinone", "1,2-Acenaphthenequinone", "2-Methyl-9,10-Anthraquinone"]
    allmols_short = ["1,4-BQ", "1,2-NQ", "5-Hyd-1.4-NQ", "9,10-PhaQ", "1,4-NQ *", "2-Meth-1,4-NQ", "9,10-AQ", "2-Hyd-1,4-NQ", "7,12-BaQ", "1,2-AcnQ", "2-Meth-9,10-AQ"]
    for num, molname in enumerate(allmols):
        try:
            if smiles_all[num] != smiles_H2O[quins_H2O.index(molname)]:
                print("Mols don't match.")
        except:
            if smiles_all[num] != smiles_DMF[quins_DMF.index(molname)]:
                print("Mols don't match")

    preds_v = []
    for pred in new_pred:
        preds_v.append(pred[0] * 1000)

    preds_fit_1 = []
    preds_fit_2 = []
    for pred in preds_v:
        preds_fit_1.append(pred-1037.08)
        preds_fit_2.append(pred-575.78)

    correlation_matrix_DMF = np.corrcoef(exp_DMF, preds_fit_1[:7] + preds_fit_1[8:])
    correlation_DMF = correlation_matrix_DMF[0,1]
    r_squared_DMF = correlation_DMF**2
    r_DMF = np.sqrt(r_squared_DMF)

    correlation_matrix_H2O = np.corrcoef(exp_H2O, preds_fit_2[:8])
    correlation_H2O = correlation_matrix_H2O[0, 1]
    r_squared_H2O = correlation_H2O ** 2
    r_H2O = np.sqrt(r_squared_H2O)

    print("r, r2 DMF: " + str(r_DMF) + "; " + str(r_squared_DMF))
    print("r, r2 H2O: " + str(r_H2O) + "; " + str(r_squared_H2O))

    from matplotlib.font_manager import FontProperties
    fontP = FontProperties()
    fontP.set_size('xx-small')

    plt.errorbar(range(len(allmols)), preds_fit_1, 115, label=None, fmt=".", color=(1, 0.5, 0.5), capsize=5, marker="x", markersize=7)
    plt.errorbar(range(len(allmols)), preds_fit_2, 115, label=None, fmt=".", color=(0.5, 0.5, 1), capsize=5, marker="x", markersize=7)
    plt.errorbar([allmols.index(x) for x in quins_DMF], exp_DMF, uncert_DMF, label=None, fmt='.', color=(1, 0, 0), capsize=5, marker=".", markersize=10)
    plt.errorbar([allmols.index(x) for x in quins_H2O], exp_H2O, uncert_H2O, label=None, fmt='.', color=(0, 0, 1), capsize=5, marker=".", markersize=10)
    plt.scatter([], [], label="Exp. values, DMF/SCE", color=(1, 0, 0), marker=".", s=100)
    plt.scatter([], [], label="Exp. values, Water/NHE", color=(0, 0, 1), marker=".", s=100)
    plt.scatter([], [], label="CNN predictions, DMF/SCE", color=(1, 0.5, 0.5), marker="x", s=70)
    plt.scatter([], [], label="CNN predictions, Water/NHE", color=(0.5, 0.5, 1), marker="x", s=70)
    plt.xticks(range(len(allmols)), allmols_short, rotation=-60, ha="left")
    plt.ylabel("Reduction Potentials E, mV")
    plt.legend(bbox_to_anchor=(0.52, 1), loc='upper left', fontsize=9)
    plt.savefig("quinone_experimental_data_fits.eps", format="eps", bbox_inches='tight')
    plt.show()

    return

def make_generated_prediction_plot(keys, groups, predictions):
    "Plot results from quinone generation experiment"
    from matplotlib.font_manager import FontProperties

    key_classes = [0, 1, 0, 2, 2, 0, 1, 2, 1, 2, 1, 0, 2, 0, 2, 2, 2, 0, 0, 1, 1, 0, 2, 0, 2, 1, 0] #0: ortho, 1: para, 2: multi

    fgroups = ["--"]
    for group in groups:
        fgroups.append(group)
    groups_preds = [[] for x in range(27)]
    #make 2d grid of predictions according to groups and templates
    for num, predpair in enumerate(predictions):
        if len(groups_preds[num%27]) == 0: #append original molecule data first
            groups_preds[num%27].append(predictions[num][0])
        #then append alternated molecule data
        groups_preds[num%27].append(predictions[num][1])

    fontP = FontProperties()
    fontP.set_size('xx-small')
    for gnum, group_preds in enumerate(groups_preds):
        plt.plot(range(len(group_preds)), group_preds, label=keys[0][gnum])
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', prop=fontP)
    plt.xticks(range(len(group_preds)), [group for group in fgroups])
    plt.ylabel("Predicted E [V]")
    plt.xlabel("Functional groups attached")
    plt.tight_layout()
    plt.savefig("generated_quinones.png")
    plt.show()


    plt.imshow(groups_preds, cmap='coolwarm', interpolation='nearest')
    plt.yticks(range(len(groups_preds)), keys[0])
    plt.xticks(range(len(groups_preds[0])), fgroups, fontsize=8, rotation=45)
    cbar = plt.colorbar()
    cbar.set_label('Predicted E [V]')
    for num, kclass in enumerate(key_classes):
        if kclass == 0:
            plt.gca().get_yticklabels()[num].set_color('r')
        elif kclass == 1:
            plt.gca().get_yticklabels()[num].set_color('b')
        else:
            plt.gca().get_yticklabels()[num].set_color('black')
    plt.tight_layout()
    plt.savefig("heatmap_generated_quin_exp.png")
    plt.show()

    groups_diffs = []
    for group in groups_preds:
        groups_diffs.append([(x - group[0]) for x in group[1:]])

    plt.imshow(groups_diffs, cmap='coolwarm', interpolation='nearest')
    plt.yticks(range(len(groups_diffs)), keys[0])
    plt.xticks(range(len(groups_diffs[0])), fgroups[1:], fontsize=8, rotation=45)
    cbar = plt.colorbar()
    cbar.set_label('Change in E prediction [V]')
    for num, kclass in enumerate(key_classes):
        if kclass == 0:
            plt.gca().get_yticklabels()[num].set_color('r')
        elif kclass == 1:
            plt.gca().get_yticklabels()[num].set_color('b')
        else:
            plt.gca().get_yticklabels()[num].set_color('black')
    plt.tight_layout()
    plt.savefig("heatmap_generated_quin_diff_exp.png")
    plt.show()



    return

def make_molecule_activation_plot(activations, preds):
    "Function to map feature map representation schemes onto the 2D molecular structure."
    from rdkit import Chem
    from rdkit.Chem.Draw import rdMolDraw2D
    from IPython.display import Image
    from ColorHelper import MplColorHelper
    from rdkit.Chem.Draw import IPythonConsole
    from rdkit.Chem import Draw
    from rdkit.Chem import AllChem
    draws = []
    groups_mol = [[] for x in range(27)]
    groups_string = [[] for x in range(27)]
    groups_activations = [[] for x in range(27)]
    groups_preds = [[] for x in range(27)]
    minimum = 1000
    maximum = -1000
    for num, [pos_activation1a, pos_activation1b, pos_activation2a, pos_activation2b, pos_activation3a, pos_activation3b, m, m2, test_string_splita, test_string_splitb] in enumerate(activations):
        if len(groups_mol[num%27]) == 0: #append original molecule data first
            groups_mol[num%27].append(m)
            groups_string[num%27].append(test_string_splita)
            groups_activations[num%27].append(pos_activation1a)
            if minimum > min(pos_activation1a):
                minimum = min(pos_activation1a)
            if maximum < max(pos_activation1a):
                maximum = max(pos_activation1a)
            groups_preds[num%27].append(preds[num][0])
        #then append alternated molecule data
        groups_mol[num % 27].append(m2)
        groups_string[num % 27].append(test_string_splitb)
        groups_activations[num % 27].append(pos_activation1b)
        if minimum > min(pos_activation1b):
            minimum = min(pos_activation1b)
        if maximum < max(pos_activation1b):
            maximum = max(pos_activation1b)
        groups_preds[num%27].append(preds[num][1])


    for num in range(27):
        mols = []
        hit_atss = []
        atom_colss = []
        hit_bondss = []
        bond_colss = []
        atom_acts = []
        predictions = []

        d = rdMolDraw2D.MolDraw2DCairo(500, 500)
        for num2 in range(len(groups_mol[num])):
            test_string_split = groups_string[num][num2]
            pos_act = groups_activations[num][num2]
            mol = groups_mol[num][num2]
            prediction = groups_preds[num][num2]

            hit_ats = []
            atom_cols = {}
            hit_bonds = []
            bond_cols = {}
            atom_act = []


            COL = MplColorHelper('coolwarm', minimum, maximum)
            currentind = -1
            for atomnum, atom in enumerate(mol.GetAtoms()):
                currentind = currentind + 1
                while test_string_split[currentind] not in ["O", "C", "P", "c", "S", "N", "F", "H", "Br", "n", "o"]:
                    currentind = currentind + 1
                atom_act.append(pos_act[currentind])
                hit_ats.append(atomnum)
                atom_cols[atomnum] = (COL.get_rgb(pos_act[currentind])[0][0], COL.get_rgb(pos_act[currentind])[0][1], COL.get_rgb(pos_act[currentind])[0][2])
            for bondnum, bond in enumerate(mol.GetBonds()):
                hit_bonds.append(bondnum)
                prev_act = atom_act[mol.GetBondWithIdx(bondnum).GetBeginAtomIdx()]
                next_act = atom_act[mol.GetBondWithIdx(bondnum).GetEndAtomIdx()]
                avg_act = np.average([prev_act, next_act])
                avg_act_col = COL.get_rgb(avg_act)
                bond_cols[bondnum] = (avg_act_col[0], avg_act_col[1], avg_act_col[2])
                #bond_cols[bondnum] = (np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][0], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][0]]), np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][1], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][1]]), np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][2], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][2]]))
            mols.append(mol)
            hit_atss.append(hit_ats)
            atom_colss.append(atom_cols)
            hit_bondss.append(hit_bonds)
            bond_colss.append(bond_cols)
            atom_acts.append(atom_act)
            predictions.append(str(prediction) + " V")
            tmp = rdMolDraw2D.PrepareMolForDrawing(mol)
        AllChem.Compute2DCoords(mols[0])
        for m in mols[1:]:
            _ = AllChem.GenerateDepictionMatching2DStructure(m, mols[0])
        d = Draw.MolsToGridImage(mols, molsPerRow=len(groups_mol[0]), subImgSize=(300, 300), highlightAtomLists=hit_atss, highlightAtomColors = atom_colss, highlightBondLists=hit_bondss, highlightBondColors=bond_colss, legends=predictions, returnPNG=False)
        #draws.append([mols, hit_atss, atom_colss, hit_bondss, bond_colss])
        #d.FinishDrawing()
        #d.WriteDrawingText(str(num) + "_" + str(num2) + "".join(test_string_split) + '.png')
        #Image(filename="".join(test_string_split) + '.png', width=250)
        d.save("".join(groups_string[num][0]) + '.png')

    return


def save_svg(svg, svg_file, dpi=300):
    png_file = svg_file.replace('.svg', '.png')
    with open(svg_file, 'w') as afile:
        afile.write(svg)
    a_str = svg.encode('utf-8')
    #cairosvg.svg2png(bytestring=a_str, write_to=png_file, dpi=dpi)
    return

def plot_NN_insights(data, groupnum, plot=1):
    "Make feature map representations for molecules"
    import matplotlib.gridspec as gridspec
    from rdkit.Chem import AllChem

    try:
        [testa, layer_avgs1a, [pos_activation1a, test_string_splita], testb, layer_avgs1b, [pos_activation1b, test_string_splitb], layer_avgs2a, [pos_activation2a, test_string_splita], layer_avgs2b, [pos_activation2b, test_string_splitb], layer_avgs3a, [pos_activation3a, test_string_splita], layer_avgs3b, [pos_activation3b, test_string_splitb]] = data

    except: #only one molecule as input (not generated quinones)
        [testa, layer_avgs1a, [pos_activation1a, test_string_splita], layer_avgs2a, [pos_activation2a, test_string_splita],
         layer_avgs3a, [pos_activation3a, test_string_splita]] = data

        from rdkit import Chem
        from rdkit.Chem.Draw import rdMolDraw2D
        from IPython.display import Image
        from ColorHelper import MplColorHelper

        mol1 = "".join(test_string_splita)
        m = Chem.MolFromSmiles(mol1)
        draws = []
        for test_string_split, pos_act, mol in [[test_string_splita, pos_activation1a, m]]:
            hit_ats = []
            atom_cols = {}
            hit_bonds = []
            bond_cols = {}
            atom_act = []
            COL = MplColorHelper('coolwarm', min(pos_act), max(pos_act))
            currentind = -1
            for atomnum, atom in enumerate(mol.GetAtoms()):
                currentind = currentind + 1
                while test_string_split[currentind] not in ["O", "C", "P", "c", "S", "N", "F", "H", "Br", "n", "o"]:
                    currentind = currentind + 1
                atom_act.append(pos_act[currentind])
                hit_ats.append(atomnum)
                atom_cols[atomnum] = (COL.get_rgb(pos_act[currentind])[0][0], COL.get_rgb(pos_act[currentind])[0][1],
                                      COL.get_rgb(pos_act[currentind])[0][2])
            for bondnum, bond in enumerate(mol.GetBonds()):
                hit_bonds.append(bondnum)
                prev_act = atom_act[mol.GetBondWithIdx(bondnum).GetBeginAtomIdx()]
                next_act = atom_act[mol.GetBondWithIdx(bondnum).GetEndAtomIdx()]
                avg_act = np.average([prev_act, next_act])
                avg_act_col = COL.get_rgb(avg_act)
                bond_cols[bondnum] = (avg_act_col[0], avg_act_col[1], avg_act_col[2])
                # bond_cols[bondnum] = (np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][0], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][0]]), np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][1], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][1]]), np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][2], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][2]]))
            d = rdMolDraw2D.MolDraw2DCairo(500, 500)
            tmp = rdMolDraw2D.PrepareMolForDrawing(mol)
            d.DrawMolecule(mol, highlightAtoms=hit_ats, highlightAtomColors=atom_cols, highlightBonds=hit_bonds,
                           highlightBondColors=bond_cols)
            draws.append([mol, hit_ats, atom_cols, hit_bonds, bond_cols])
            d.FinishDrawing()
            d.WriteDrawingText("".join(test_string_split) + '.png')
            Image(filename="".join(test_string_split) + '.png', width=250)

        return [pos_activation1a, pos_activation2a, pos_activation3a, m, test_string_splita]

    if plot==1:
        fig = plt.figure(figsize=(10, 8))
        outer = gridspec.GridSpec(6, 6, wspace=0.2, hspace=0.2)

        inner = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[1, 0], wspace=0.1, hspace=0.1)

        ax = plt.Subplot(fig, inner[0])
        ax.imshow(testa, cmap="Greys")
        fig.add_subplot(ax)
        plt.xticks([8, 11, 14, 19], ["c", "O", "C", "none"], fontsize=6)
        plt.xlabel("encoded input")


        for jnum, j in enumerate([layer_avgs1a, layer_avgs2a, layer_avgs3a]):
            inner = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[1, 1+jnum], wspace=0.1, hspace=0.1)
            ax = plt.Subplot(fig, inner[0])
            ax.imshow(j, cmap="coolwarm")
            fig.add_subplot(ax)
            plt.gca().yaxis.set_visible(False)
            #plt.gca().xaxis.set_visible(False)
            plt.xticks([], [])
            plt.xlabel("conv layer " + str(jnum+1))

        inner = gridspec.GridSpecFromSubplotSpec(3, 1, subplot_spec=outer[1, 4:], wspace=0.1, hspace=0.1)
        for jnum, j in enumerate([pos_activation3a, pos_activation2a, pos_activation1a]):
            ax = plt.Subplot(fig, inner[jnum])
            ax.imshow(j[5:].reshape(1, -1), cmap="coolwarm", vmax=max(j[5:]),
                       vmin=min(j[5:]))
            fig.add_subplot(ax)
            if jnum != 2:
                plt.gca().xaxis.set_visible(False)
            else:
                plt.xticks(range(len(test_string_splita)), labels=test_string_splita)
            plt.yticks([0], labels=[str(3-jnum)])


        inner = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[3, 0], wspace=0.1, hspace=0.1)
        ax = plt.Subplot(fig, inner[0])
        ax.imshow(testb, cmap="Greys")
        fig.add_subplot(ax)
        plt.xticks([8, 11, 14, 19], ["c", "O", "C", "none"], fontsize=6)
        plt.xlabel("encoded input")

        for jnum, j in enumerate([layer_avgs1b, layer_avgs2b, layer_avgs3b]):
            inner = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[3, 1 + jnum], wspace=0.1, hspace=0.1)
            ax = plt.Subplot(fig, inner[0])
            ax.imshow(j, cmap="coolwarm")
            fig.add_subplot(ax)
            plt.gca().yaxis.set_visible(False)
            #plt.gca().xaxis.set_visible(False)
            plt.xticks([], [])
            plt.xlabel("conv layer " + str(jnum+1))

        inner = gridspec.GridSpecFromSubplotSpec(3, 1, subplot_spec=outer[3, 4:], wspace=0.1, hspace=0.1)
        for jnum, j in enumerate([pos_activation3b, pos_activation2b, pos_activation1b]):
            ax = plt.Subplot(fig, inner[jnum])
            ax.imshow(j[5:].reshape(1, -1), cmap="coolwarm", vmax=max(j[5:]),
                       vmin=min(j[5:]))
            fig.add_subplot(ax)
            if jnum != 2:
                plt.gca().xaxis.set_visible(False)
            else:
                plt.xticks(range(len(test_string_splitb)), labels=test_string_splitb)
            plt.yticks([0], labels=[str(3-jnum)])
        fig.savefig("full_insight_" + "".join(test_string_splita) + "_" + str(groupnum) + '.png')
        #fig.show()

    from rdkit import Chem
    from rdkit.Chem.Draw import rdMolDraw2D
    from IPython.display import Image
    from ColorHelper import MplColorHelper

    mol1 = "".join(test_string_splita)
    mol2 = "".join(test_string_splitb)
    m = Chem.MolFromSmiles(mol1)
    m2 = Chem.MolFromSmiles(mol2)
    draws = []
    for test_string_split, pos_act, mol in [[test_string_splita, pos_activation1a, m], [test_string_splitb, pos_activation1b, m2]]:
        hit_ats = []
        atom_cols = {}
        hit_bonds = []
        bond_cols = {}
        atom_act = []
        COL = MplColorHelper('coolwarm', min(pos_act), max(pos_act))
        currentind = -1
        for atomnum, atom in enumerate(mol.GetAtoms()):
            currentind = currentind + 1
            while test_string_split[currentind] not in ["O", "C", "P", "c", "S", "N", "F", "H", "Br", "n", "o"]:
                currentind = currentind + 1
            atom_act.append(pos_act[currentind])
            hit_ats.append(atomnum)
            atom_cols[atomnum] = (COL.get_rgb(pos_act[currentind])[0][0], COL.get_rgb(pos_act[currentind])[0][1], COL.get_rgb(pos_act[currentind])[0][2])
        for bondnum, bond in enumerate(mol.GetBonds()):
            hit_bonds.append(bondnum)
            prev_act = atom_act[mol.GetBondWithIdx(bondnum).GetBeginAtomIdx()]
            next_act = atom_act[mol.GetBondWithIdx(bondnum).GetEndAtomIdx()]
            avg_act = np.average([prev_act, next_act])
            avg_act_col = COL.get_rgb(avg_act)
            bond_cols[bondnum] = (avg_act_col[0], avg_act_col[1], avg_act_col[2])
            #bond_cols[bondnum] = (np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][0], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][0]]), np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][1], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][1]]), np.average([atom_cols[m.GetBondWithIdx(bondnum).GetBeginAtomIdx()][2], atom_cols[m.GetBondWithIdx(bondnum).GetEndAtomIdx()][2]]))
        d = rdMolDraw2D.MolDraw2DCairo(500, 500)
        tmp = rdMolDraw2D.PrepareMolForDrawing(mol)
        d.DrawMolecule(mol, highlightAtoms=hit_ats, highlightAtomColors = atom_cols, highlightBonds=hit_bonds, highlightBondColors=bond_cols)
        draws.append([mol, hit_ats, atom_cols, hit_bonds, bond_cols])
        d.FinishDrawing( )
        d.WriteDrawingText("".join(test_string_split) + '.png')
        Image(filename="".join(test_string_split) + '.png', width=250)

    return [pos_activation1a, pos_activation1b, pos_activation2a, pos_activation2b, pos_activation3a, pos_activation3b, m, m2, test_string_splita, test_string_splitb]

def look_into_NN(model, datafile, moldata=[], plotfilters=0, feature_operation=0, ambient_flattening=1, rem_none_ind=1):
    "Extract data for feature map representations"

    if len(moldata) != 0:
        NN_plot_data = []
        # get test data set from training data to obtain predictions
        infile = open(datafile, 'rb')
        data = pickle.load(infile)
        infile.close()

        scaler_y = data[-1][4]

        # summarize filter shapes
        for layer_num, layer in enumerate(model.layers):
            # check for convolutional layer
            if 'conv' not in layer.name:
                continue

            # get filter weights
            filters, biases = layer.get_weights()
            print(layer.name, filters.shape)

            # retrieve weights from the second hidden layer
            filters, biases = model.layers[1].get_weights()

            # normalize filter values to 0-1 so we can visualize them
            f_min, f_max = filters.min(), filters.max()
            filters = (filters - f_min) / (f_max - f_min)

            if layer_num == 1 and plotfilters==1:
                # plot first few filters
                n_filters, ix = 9, 1
                for i in range(n_filters):
                    # get the filter
                    f = filters[:, :, :, i]
                    # plot each channel separately
                    for j in range(1):
                        # specify subplot and turn of axis
                        ax = plt.subplot(n_filters, 3, ix)
                        ax.set_xticks([])
                        ax.set_yticks([])
                        # plot filter channel in grayscale
                        plt.imshow(f[:, :, j], cmap='coolwarm')
                        ix += 1
                # show the figure
                plt.show()

            # redefine model to output right after the first hidden layer
            # model = model(inputs=model.inputs, output=model.layers[1].output)

            inp = model.input  # input placeholder
            outputs = model.layers[layer_num].output  # all layer outputs
            functor = K.function([inp], [outputs])  # evaluation functions



            for xnum, test in enumerate(moldata[0]):
                if rem_none_ind == 1:
                    test[:, 19] = False
                if layer_num == 1:
                    #plt.imshow(test[:30], cmap='coolwarm')
                    #plt.show()
                    if rem_none_ind == 0:
                        for num, pos in enumerate(test):
                            if pos[19] == True and num > 5:
                                last = num
                                break
                    else:
                        for num, pos in enumerate(test):
                            if True not in pos and num > 5:
                                last = num
                                break

                    NN_plot_data.append(test[:last+4])

                test_string = moldata[1][xnum]

                if len(test_string) == 1:
                    test_string = test_string[0]

                test = np.asarray([test])
                # Testing
                layer_outs = functor(test)

                layer_avgs = []
                layer_sums = []
                # get average_feature map
                for first_order in layer_outs[0]:
                    first_sums = []
                    first_avgs = []
                    for second_order in first_order:
                        second_sums = []
                        second_avgs = []
                        for third_order in second_order:
                            avg = np.average(third_order)
                            # sum = np.average(np.abs(third_order))
                            # second_sums.append(sum)
                            second_avgs.append(avg)
                        # first_sums.append(second_sums)
                        first_avgs.append(second_avgs)
                    # layer_sums.append(first_sums)
                    layer_avgs.append(first_avgs)

                #plt.imshow(layer_avgs[0][:30], cmap='coolwarm')
                #plt.show()
                NN_plot_data.append(layer_avgs[0][:last+4])
                # plt.imshow(layer_sums[0], cmap='coolwarm')
                # plt.show()

                #algorithm to reduce effect of non-encoding section of feature map
                if ambient_flattening == 1:
                    #find "background"-value
                    background = layer_avgs[0][-1][-1]
                    new0 = []
                    for layer_avg in layer_avgs:
                        new1 = []
                        for line in layer_avg:
                            new2 = []
                            for val in line:
                                new2.append(val-background)
                            new1.append(new2)
                        new0.append(new1)
                    layer_avgs = new0

                pos_activation = []
                for num in range(len(test_string)+5):

                    if layer_num == 1:

                        if num < 8:
                            relevant = layer_avgs[0][:num+1]
                        else:
                            relevant = layer_avgs[0][(num-7):(num+1)]


                        if feature_operation == 0:
                            act = np.sum(sum(k ** 2 for k in j) for j in relevant)
                        elif feature_operation == 1:
                            act = np.sum(np.sqrt(sum(k ** 2 for k in j)) for j in relevant)
                        elif feature_operation == 2:
                            act = min([min(j) for j in relevant])
                        else:
                            act = max([max(j) for j in relevant])
                        pos_activation.append(act)


                    elif layer_num == 3:
                        if num < 11:
                            relevant = layer_avgs[0][:num+1]
                        else:
                            relevant = layer_avgs[0][(num-10):num+1]


                        if feature_operation == 0:
                            act = np.sum(sum(k ** 2 for k in j) for j in relevant)
                        elif feature_operation == 1:
                            act = np.sum(np.sqrt(sum(k ** 2 for k in j)) for j in relevant)
                        elif feature_operation == 3:
                            act = min([min(j) for j in relevant])
                        else:
                            act = max([max(j) for j in relevant])
                        pos_activation.append(act)

                    else:
                        if num < 12:
                            relevant = layer_avgs[0][:num+1]
                        else:
                            relevant = layer_avgs[0][(num-11):num+1]
                        if feature_operation == 0:
                            act = np.sum(sum(k ** 2 for k in j) for j in relevant)
                        elif feature_operation == 1:
                            act = np.sum(np.sqrt(sum(k ** 2 for k in j)) for j in relevant)
                        elif feature_operation == 2:
                            act = min([min(j) for j in relevant])
                        else:
                            act = max([max(j) for j in relevant])
                        pos_activation.append(act)


                # for i in range(5):
                #     relevant = []
                #     for j in range(5-i):
                #         relevant.append(layer_avgs[0][-(5 -i -j)])
                #     act = np.sum(np.sum(sum(k ** 2 for k in j) for j in relevant))
                #     pos_activation.append(act)

                pos_activation = np.asarray(pos_activation)
                pos_activation = np.expand_dims(pos_activation, 1)
                test_string_split = [char for char in test_string]

                # cmap = plt.cm.get_cmap('coolwarm')
                # plt.imshow(pos_activation.reshape(1, -1), cmap=cmap, vmax=max(pos_activation[:-5]),
                #            vmin=min(pos_activation[:-5]))
                # # for i in range(len(pos_activation)):
                # #    plt.text(i, 0, test_string[i], ha='center', va='center', color='navy')
                # plt.gca().yaxis.set_visible(False)
                # plt.xticks(range(len(pos_activation)), labels=test_string_split)
                # plt.show()

                NN_plot_data.append([pos_activation, test_string_split])

                # # 5 feature maps in one plot
                # square = 5
                # ix = 1

                # for _ in range(square):
                #     # specify subplot and turn of axis
                #     ax = plt.subplot(1, square, ix)
                #     ax.set_xticks([])
                #     ax.set_yticks([])
                #     # plot filter channel in coolwarm
                #     plt.imshow(np.transpose(layer_outs[0][0][:][ix - 1]), cmap='coolwarm')
                #     ix += 1
                # # show the figure
                # plt.show()
        return NN_plot_data


    else:
        # get test data set from training data to obtain predictions
        infile = open(datafile, 'rb')
        data = pickle.load(infile)
        infile.close()

        xtest = data[-1][1]
        scaler_y = data[-1][4]

        # summarize filter shapes
        for layer_num, layer in enumerate(model.layers):
            # check for convolutional layer
            if 'conv' not in layer.name:
                continue

            # get filter weights
            filters, biases = layer.get_weights()
            print(layer.name, filters.shape)

            # retrieve weights from the second hidden layer
            filters, biases = model.layers[1].get_weights()

            # normalize filter values to 0-1 so we can visualize them
            f_min, f_max = filters.min(), filters.max()
            filters = (filters - f_min) / (f_max - f_min)

            if layer_num == 0:
                # plot first few filters
                n_filters, ix = 5, 1
                for i in range(n_filters):
                    # get the filter
                    f = filters[:, :, :, i]
                    # plot each channel separately
                    for j in range(1):
                        # specify subplot and turn of axis
                        ax = plt.subplot(n_filters, 3, ix)
                        ax.set_xticks([])
                        ax.set_yticks([])
                        # plot filter channel in grayscale
                        plt.imshow(f[:, :, j], cmap='coolwarm')
                        ix += 1
                # show the figure
                plt.show()

            # redefine model to output right after the first hidden layer
            #model = model(inputs=model.inputs, output=model.layers[1].output)


            inp = model.input  # input placeholder
            outputs = model.layers[layer_num].output  # all layer outputs
            functor = K.function([inp], [outputs])  # evaluation functions


            test_df = pd.read_csv("testset.csv")

            test = xtest[7027] #largest E(0)
            #test = xtest[15718] #lowest E(0)

            if layer_num == 0:
                plt.imshow(test[0], cmap='coolwarm')
                plt.show()

            test_string = test_df.iloc[7027][0]
            #test_string = test_df.iloc[15718][0]

            test = np.asarray([test])
            # Testing
            layer_outs = functor(test)

            layer_avgs = []
            layer_sums = []
            #get average_feature map
            for first_order in layer_outs[0]:
                first_sums = []
                first_avgs = []
                for second_order in first_order:
                    second_sums = []
                    second_avgs = []
                    for third_order in second_order:
                        avg = np.average(third_order)
                        #sum = np.average(np.abs(third_order))
                        #second_sums.append(sum)
                        second_avgs.append(avg)
                    #first_sums.append(second_sums)
                    first_avgs.append(second_avgs)
                #layer_sums.append(first_sums)
                layer_avgs.append(first_avgs)

            plt.imshow(layer_avgs[0], cmap='coolwarm')
            plt.show()
            #plt.imshow(layer_sums[0], cmap='coolwarm')
            #plt.show()

            pos_activation = []
            for num, char in enumerate(test_string):
                relevant = []
                for i in range(6):
                    relevant.append(layer_avgs[0][num+i])
                if 0:
                    act = np.sum(np.sum(sum(k**2 for k in j) for j in relevant))
                else:
                    act = np.sum(np.sum(sum(j) for j in relevant))
                pos_activation.append(act)

            # for i in range(5):
            #     relevant = []
            #     for j in range(5-i):
            #         relevant.append(layer_avgs[0][-(5 -i -j)])
            #     act = np.sum(np.sum(sum(k ** 2 for k in j) for j in relevant))
            #     pos_activation.append(act)


            pos_activation = np.asarray(pos_activation)
            pos_activation = np.expand_dims(pos_activation, 1)
            test_string_split = [char for char in test_string]

            cmap = plt.cm.get_cmap('coolwarm')
            plt.imshow(pos_activation.reshape(1, -1), cmap=cmap, vmax=max(pos_activation[:-5]), vmin=min(pos_activation[:-5]))
            #for i in range(len(pos_activation)):
            #    plt.text(i, 0, test_string[i], ha='center', va='center', color='navy')
            plt.gca().yaxis.set_visible(False)
            plt.xticks(range(len(pos_activation)), labels=test_string_split)
            plt.show()

            # # 5 feature maps in one plot
            # square = 5
            # ix = 1

            # for _ in range(square):
            #     # specify subplot and turn of axis
            #     ax = plt.subplot(1, square, ix)
            #     ax.set_xticks([])
            #     ax.set_yticks([])
            #     # plot filter channel in coolwarm
            #     plt.imshow(np.transpose(layer_outs[0][0][:][ix - 1]), cmap='coolwarm')
            #     ix += 1
            # # show the figure
            # plt.show()

    return


def get_pairplot_from_heatmap_file(heatmap_file, intervals, interval_names, error=0):
    import seaborn as sns
    if error==1:
        df = pd.read_csv(heatmap_file)
        ints = []
        for num, red in enumerate(df["abs_error"]):
            done = 0
            for inum, int in enumerate(intervals):
                if red < int and done == 0:
                    ints.append(interval_names[inum])
                    done = 1
                if done == 0 and inum == (len(intervals)-1):
                    ints.append(interval_names[-1])
        plt.hist(ints, bins=len(intervals))
        #for error hist:
        #plt.xticks([(x*10) for x in range(6)], labels=[interval_names[10*x] for x in range(6)])
        #for error bin hist:
        plt.xticks([(x +0.5) for x in range(len(intervals))], labels=[interval_names[x] for x in range(len(intervals))])
        plt.xlabel("Absolute Error of E prediction [V]")
        plt.ylabel("Samples in Test Data Set")
        plt.show()

        df = df.drop(["abs_error", "n_t_bonds", "n_a_bonds", "n_S", "n_P", "n_F", "n_Si"], axis=1)
        df['abs_error'] = ints
        #from sklearn.utils import shuffle
        #df = shuffle(df)




        df = pd.melt(df, df.columns[-1], df.columns[:-1])

        g = sns.FacetGrid(df, col="variable", hue="abs_error", legend_out=True, sharex=False, sharey=False,
                          palette=sns.color_palette("coolwarm", n_colors=len(intervals)), col_wrap=4)

        g.map(sns.histplot, "value")
        # g.map(plt.hist, "value", stacked=True)
        plt.legend()
        g.savefig("facetgridDIST_" + str(len(interval_names)) + ".png")
        plt.show()

        # logdf = np.log10(df)
        # g = sns.pairplot(logdf, hue="E")
        # plt.show()
        col = sns.color_palette("coolwarm", n_colors=len(intervals))
        h = sns.pairplot(df, hue="abs_error", palette=col, plot_kws={"s": 12})
        h.savefig("output" + str(len(interval_names)) + ".png")
        plt.show()

        g = sns.FacetGrid(df, col="variable", hue="abs_error", legend_out=True, sharex=False, sharey=False, palette=sns.color_palette("coolwarm", n_colors=len(intervals)), col_wrap=4)

        g.map(sns.kdeplot, "value", fill=None)
        #g.map(plt.hist, "value", stacked=True)
        plt.legend()
        g.savefig("facetgridDIST_" + str(len(interval_names)) + ".png")
        plt.show()

        # logdf = np.log10(df)
        # g = sns.pairplot(logdf, hue="E")
        # plt.show()
        col = sns.color_palette("coolwarm", n_colors=len(intervals))
        h = sns.pairplot(df, hue="abs_error", palette=col, plot_kws={"s": 12})
        h.savefig("output" + str(len(interval_names)) + ".png")
        plt.show()


    else:
        df = pd.read_csv(heatmap_file)
        ints = []
        for num, red in enumerate(df["E"]):
            done = 0
            for inum, int in enumerate(intervals):
                if red < int and done == 0:
                    ints.append(interval_names[inum])
                    done = 1
        df = df.drop(
            ["E", "n_t_bonds", "n_a_bonds", "n_S", "n_Si", "n_F", "n_Br", "n_Cl", "n_N", "n_P", "n_rings", "conj_C",
             "keto_dist", "n_keto"], axis=1)
        df['E'] = ints

        # logdf = np.log10(df)
        # g = sns.pairplot(logdf, hue="E")
        # plt.show()
        col = sns.color_palette("coolwarm", n_colors=len(intervals))
        h = sns.pairplot(df, hue="E", palette=col, plot_kws={"s": 12})
        h.savefig("output" + str(len(interval_names)) + ".png")
        plt.show()
    return


def compare_with_linear_gc_model(heatmap_file, testsetfile, NN_pred, out="E"):
    "Fits linear model on tabular data to benchmark CNN model accuracy"

    try:
        [lm_pred, Y_test] = fit_linear_gc_model(heatmap_file, testsetfile, out)
        lm = 1
    except:
        print("Linear Model not computed - rearrange testset heatmap file (remove empty columns and ensure that columns in testset_heatmap and the data used to fit linear model are identical).")
        test = pd.read_csv("testset_chem_heatmap.csv")
        Y_test = test[out]
        lm = 0

    NN_pred_l = []
    for pred in NN_pred:
        NN_pred_l.append(pred[0])

    Y_test_l = Y_test.tolist()
    #new_Y_test = np.concatenate([Y_test[:10069], Y_test[10070:]])

    if lm == 1:
        lm_pred_l = lm_pred.tolist()



        #new_lm_pred = np.concatenate([lm_pred[:10069], lm_pred[10070:]])




        print("MSE LM:")
        print(metrics.mean_squared_error(Y_test_l, lm_pred_l))
    print("MSE CNN:")
    print(metrics.mean_squared_error(Y_test, NN_pred))

    oneplot = 0
    if oneplot == 1:
        cm=1/2.54
        fig, axs = plt.subplots(2, figsize=(5, 8))
        axs[0].scatter(Y_test_l, NN_pred_l, s=0.01, color="r", label="CNN")
        axs[0].plot([-0.75, 1.75], [-0.75, 1.75], color="grey")
        plt.ylim([-0.75, 1.75])
        axs[0].set_ylabel("Predicted Reduction Potentials E, V", fontsize=10)
        axs[1].set_ylabel("Predicted Reduction Potentials E, V", fontsize=10)
        #fig.text(0, 0.5, 'E Predictions [V]', va='center', rotation='vertical', fontsize=10)
        plt.xlabel("Reference Reduction Potentials E, V", fontsize=10)
        axs[1].scatter(Y_test_l, lm_pred_l, s=0.01, color="b", label="Linear Model")
        axs[1].plot([-0.75, 1.75], [-0.75, 1.75], color="grey")
        for ax in axs:
            ax.tick_params(labelsize=10)
            ax.legend(fontsize=10, markerscale=10)
        axs[0].annotate('RMSE = 0.115 V    \n', xy=(1, 0), xycoords='axes fraction', fontsize=10,
                        horizontalalignment='right', verticalalignment='bottom')
        axs[0].annotate('a) ', xy=(0, 1), xycoords='axes fraction', fontsize=15,
                        horizontalalignment='right', verticalalignment='top')

        axs[1].annotate('RMSE = 0.220 V    \n', xy=(1, 0), xycoords='axes fraction', fontsize=10,
                 horizontalalignment='right', verticalalignment='bottom')
        axs[1].annotate('b) ', xy=(0, 1), xycoords='axes fraction', fontsize=15,
                        horizontalalignment='right', verticalalignment='top')
        fig.savefig("lm_corr_svg.eps", format="eps", bbox_inches='tight')
        fig.show()
        return

    # print(np.argmax(lm_pred))
    # print(Y_test[np.argmax(lm_pred)])
    if lm == 1:
        plt.scatter(Y_test_l, lm_pred_l, marker="x" , s=10, color="b", label="Linear Model")
        if "E" in out:
            plt.plot([-0.75, 1.75], [-0.75, 1.75], color="grey")
            plt.ylim([-0.75, 1.75])
            plt.ylabel("E Predictions [V]", fontsize=13)
            plt.xlabel("E Values [V]", fontsize=13)
        if "Gsolv" in out:
            plt.plot([-300, 0], [-300, 0], color="grey")
            plt.ylim([-300, 0])
            plt.xlim([-300, 0])

        # plt.xticks(fontsize=13)
        # plt.yticks(fontsize=13)
        # plt.legend(fontsize=13)
        # plt.savefig("lm_corr_svg.svg", bbox_inches='tight')
        # plt.show()

    #plt.scatter(Y_test_l, lm_pred_l, marker="x", s=10, color="b", label="Linear Model")
    plt.scatter(Y_test_l, NN_pred_l, marker=6, s=10, color="r", label="CNN")
    if "E" in out:
        plt.plot([-0.75, 1.75], [-0.75, 1.75], color="grey")
        plt.ylim([-0.4, 1])
        plt.xlim([-0.6, 1])
        plt.ylabel("Predicted Reduction Potentials E, V", fontsize=13)
        plt.xlabel("Reference Reduction Potentials E, V", fontsize=13)
    if "Gsolv" in out:
        plt.plot([-300, 0], [-300, 0], color="grey")
        plt.ylim([-300, 0])
        plt.xlim([-300, 0])

    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.legend(fontsize=13)
    #plt.text(0.5, 0.5, 'RMSE(CNN) = 0.278 V', horizontalalignment='center', verticalalignment='center')
    #plt.annotate('RMSE(CNN) = 0.278 V    \n', xy=(1, 0),xycoords='axes fraction', fontsize=13, horizontalalignment='right', verticalalignment='bottom')
    plt.savefig("nn_corr_svg.eps", format="eps", bbox_inches='tight')
    plt.show()

    return

def compare_with_clustering(heatmap_file, testsetfile, NN_pred):


    [lm_pred, Y_test] = fit_clustering_model(heatmap_file, testsetfile)

    intervals = [0.25, 0.5, 0.75, 1, 1.75]

    ints = []
    for num, red in enumerate(NN_pred):
        done = 0
        for inum, int in enumerate(intervals):
            if red < int and done == 0:
                ints.append(inum)
                done = 1
    NN_pred = ints

    NN_conf = metrics.confusion_matrix(Y_test, NN_pred)
    class_conf = metrics.confusion_matrix(Y_test, lm_pred)

    print("CNNaccuracy:")
    print(metrics.accuracy_score(Y_test, NN_pred))

    print(NN_conf)

    print("RFaccuracy:")
    print(metrics.accuracy_score(Y_test, lm_pred))

    print(class_conf)

    return


def get_conf_matrix_heatmap():
    import seaborn as sns
    import matplotlib.pyplot as plt

    CNNarray = [[568, 113, 16, 0, 1],
 [73, 1052, 204, 12, 0],
 [11, 261, 1213, 74, 4],
 [0, 28, 250, 565, 44],
 [0, 2, 22, 123, 364]]
    rfarray = [[435, 117, 81, 51, 14],
 [195, 364, 446, 280, 56],
 [143, 135, 742, 415, 128],
 [22, 38, 246, 438, 143],
 [  8, 3, 76, 158, 266]]

    sums = [698, 1341, 1563, 887, 511]

    for array in [CNNarray, rfarray]:
        normalized = []
        for linnum, line in enumerate(array):
            newline = []
            for value in line:
                newline.append(value/sums[linnum])
            normalized.append(newline)

        df_cm = pd.DataFrame(normalized, index=["E < 0.25 V", "0.25 V < E < 0.5 V", "0.5 V < E < 0.75 V", "0.75 V < E < 1 V", "1 V < E"],
                             columns=["E < 0.25 V", "0.25 V < E < 0.5 V", "0.5 V < E < 0.75 V", "0.75 V < E < 1 V", "1 V < E"])
        plt.figure(figsize=(10, 7))
        col = sns.color_palette("coolwarm")
        sns.heatmap(df_cm, annot=True, cmap=col)
        plt.xlabel("Predicted classes")
        plt.xticks([0.5, 1.5, 2.5, 3.5, 4.5], ["E < 0.25 V", "0.25 V < E < 0.5 V", "0.5 V < E < 0.75 V", "0.75 V < E < 1 V", "1 V < E"], rotation=30)
        plt.ylabel("Actual classes")
        plt.show()
    return

def fit_clustering_model(heatmap_file, testsetfile):
    X = pd.read_csv(heatmap_file)
    df = pd.read_csv(testsetfile)
    rem = df["E"]

    X = X[~X.E.isin(rem)]
    X = X.dropna()

    Y = X["E"]
    X = X.drop(["E"], axis=1)

    intervals = [0.25, 0.5, 0.75, 1, 1.75]

    ints = []
    for num, red in enumerate(Y):
        done = 0
        for inum, int in enumerate(intervals):
            if red < int and done == 0:
                ints.append(inum)
                done = 1
    Y = ints



    test = pd.read_csv("testset_chem_heatmap.csv")
    Y_test = test["E"]
    X_test = test.drop(["E"], axis=1)

    ints = []
    for num, red in enumerate(Y_test):
        done = 0
        for inum, int in enumerate(intervals):
            if red < int and done == 0:
                ints.append(inum)
                done = 1
    Y_test = ints

    # kne = KNeighborsClassifier(6)
    # kne.fit(X, Y)
    # kne_pred = kne.predict(X_test)
    # kne_acc = metrics.accuracy_score(Y_test, kne_pred)

    # dt = DecisionTreeClassifier()
    # dt.fit(X, Y)
    # dt_pred = dt.predict(X_test)
    # dt_acc = metrics.accuracy_score(Y_test, dt_pred)

    rf = RandomForestClassifier()
    rf.fit(X, Y)
    rf_pred = rf.predict(X_test)
    rf_acc = metrics.accuracy_score(Y_test, rf_pred)

    return [rf_pred, Y_test]

def fit_linear_gc_model(heatmap_file, testsetfile, out):
    X = pd.read_csv(heatmap_file)

    df = pd.read_csv(testsetfile)
    rem = df[out]

    try:
        X = X[~X.E.isin(rem)]
    except:
        X = X[~X.Gsolv.isin(rem)]

    X = X.dropna()

    Y = X[out]
    X = X.drop([out], axis=1)

    #fit different linear models

    reg = linear_model.LinearRegression().fit(X, Y)

    test = pd.read_csv("testset_chem_heatmap.csv")
    Y_test = test[out]
    X_test = test.drop([out], axis=1)

    regpred = reg.predict(X_test)

    #sgd = linear_model.SGDRegressor().fit(X, Y)
    #sgd_score = sgd.score(X, Y)

    #kernel = kernel_ridge.KernelRidge().fit(X, Y)
    #kernel_score = kernel.score(X, Y)

    #elast = linear_model.ElasticNet().fit(X, Y)
    #elast_score = elast.score(X, Y)




    return regpred, Y_test



def prep_trackgroup_plot(groups, preds, gplot, gcol, gax):

    xg = []
    yg = []
    xg2 = []
    xg3 = []

    for group in groups:
        tempxg = []
        tempyg = []
        tempx2g = []
        tempx3g = []
        for num, ind in enumerate(group):
            try:
                tempyg.append(preds[ind][0])
            except:
                tempyg.append(preds[ind])
            tempxg.append(gax[ind])
            tempx2g.append(gcol[ind])
            tempx3g.append(gplot[ind])

        xg.append(tempxg)
        yg.append(tempyg)
        xg2.append(tempx2g)
        xg3.append(tempx3g)

    return [yg, xg, xg2, xg3]


def make_trackgroup_plot(xys, gplot, gcol, gax, title, ytest, boxplot=0):
    [yg, xg, xg2, xg3] = xys
    global colorvar

    uniquesg = []
    uniquest = []
    for group in xg3:
        if group[0] not in uniquesg:
            uniquesg.append(group[0])
    for group in xg3:
        if group[0] not in uniquest:
            uniquest.append(group[0])

    for unique in uniquesg:
        leg=[]
        for num in range(len(xg3)):
            if xg3[num][0] == unique:
                if xg2[num][0] not in leg:
                    tempcol = get_color_var(xg2[num][0])
                    colorvar = colorvar + 1
                    if len(xg[num]) < 40:
                        plt.plot(xg[num], yg[num], ":", color=tempcol)
                        if len(xg[num]) > 2:
                            plt.plot([xg[num][0], xg[num][-1]], [yg[num][0], yg[num][-1]], color=tempcol)
                        if len(xg[num]) > 3:
                            plt.plot([xg[num][0], xg[num][-2]], [yg[num][0], yg[num][-2]], color=tempcol)
                            plt.plot([xg[num][1], xg[num][-1]], [yg[num][1], yg[num][-1]], color=tempcol)
                    else:
                        z = np.polyfit(xg[num], yg[num], 1)
                        p = np.poly1d(z)
                        plt.plot(xg[num], p(xg[num]), color=tempcol)
                    if boxplot==0:
                        plt.scatter(xg[num], yg[num], color=get_color(xg2[num][0]), label=gcol + ": " + str(xg2[num][0]), s=1)
                    else:
                        xbox = []
                        ybox = []
                        for n, x in enumerate(xg[num]):
                            if x not in xbox:
                                xbox.append(x)
                                ybox.append([yg[num][n]])
                            else:
                                ybox[xbox.index(x)].append(yg[num][n])
                        c = get_color(xg2[num][0])
                        plt.boxplot(ybox, xbox, patch_artist=True, boxprops=dict(facecolor=c, color=c), capprops=dict(color=c), whiskerprops=dict(color=c),flierprops=dict(color=c, markeredgecolor=c), medianprops=dict(color=c))
                    leg.append(xg2[num][0])
                else:
                    if len(xg[num]) < 40:
                        plt.plot(xg[num], yg[num], ":", color=get_color_var(xg2[num][0]))
                    else:
                        z = np.polyfit(xg[num], yg[num], 1)
                        p = np.poly1d(z)
                        plt.plot(xg[num], p(xg[num]), color=get_color_var(xg2[num][0]))
                    colorvar = colorvar + 1
                    if boxplot==0:
                        plt.scatter(xg[num], yg[num], color=get_color(xg2[num][0]), s=1)
                    else:
                        xbox = []
                        ybox = []
                        for n, x in enumerate(xg[num]):
                            if x not in xbox:
                                xbox.append(x)
                                ybox.append([yg[num][n]])
                            else:
                                ybox[xbox.index(x)].append(yg[num][n])
                        c = get_color(xg2[num][0])
                        plt.boxplot(ybox, xbox, patch_artist=True,boxprops=dict(facecolor=c, color=c),capprops=dict(color=c),whiskerprops=dict(color=c),flierprops=dict(color=c, markeredgecolor=c),medianprops=dict(color=c))
        if ytest == 0:
            plt.ylabel("E0 Predictions")
        else:
            plt.ylabel("E0 Dataset values")
        plt.xlabel(gax)
        plt.title(title + " -- " + gplot + ": " + str(unique))
        plt.legend()
        plt.show()

    return


def find_groups_from_smiles(smiles, min_struc_gs=3, min_el_gs=600):


    returns = []

    # unique eqsum groups
    struc_uniques = []
    el_uniques = []
    struc_unique_groups = []
    el_unique_groups = []
    struc_unique_index_groups = []
    el_unique_index_groups = []
    for num, smil in enumerate(smiles):
        newsmil = []
        for char in smil:
            if char in ["C", "c", "N", "n", "O", "o", "P", "p", "S", "s", "F", "f", "l"]:
                newsmil.append(char.lower())

        if newsmil in struc_uniques: #smilstruc in uniques -> append to corresponding group
            struc_unique_groups[struc_uniques.index(newsmil)].append(smil)
            struc_unique_index_groups[struc_uniques.index(newsmil)].append(num)
        else: #not in uniques -> new unique
            struc_uniques.append(newsmil)
            struc_unique_index_groups.append([num])
            struc_unique_groups.append([smil])

        #repeat for sorted -> element count relevant
        newsmil_sort = sorted(newsmil)
        if newsmil_sort in el_uniques: #smilstruc in uniques -> append to corresponding group
            el_unique_groups[el_uniques.index(newsmil_sort)].append(smil)
            el_unique_index_groups[el_uniques.index(newsmil_sort)].append(num)
        else: #not in uniques -> new unique
            el_uniques.append(newsmil_sort)
            el_unique_index_groups.append([num])
            el_unique_groups.append([smil])


    # filter all groups with fewer elements then specified
    fil_struc_unique_groups = []
    fil_struc_unique_index_groups = []

    fil_el_unique_groups = []
    fil_el_unique_index_groups = []
    for num, smil in enumerate(struc_uniques):
        if len(struc_unique_groups[num]) >= min_struc_gs:
            fil_struc_unique_groups.append(struc_unique_groups[num])
            fil_struc_unique_index_groups.append(struc_unique_index_groups[num])

    for num, smil in enumerate(el_uniques):
        if len(el_unique_groups[num]) >= min_el_gs:
            fil_el_unique_groups.append(el_unique_groups[num])
            fil_el_unique_index_groups.append(el_unique_index_groups[num])


    returns.append(fil_struc_unique_groups)
    returns.append(fil_struc_unique_index_groups)
    returns.append(fil_el_unique_groups)
    returns.append(fil_el_unique_index_groups)

    return returns

def get_predictions(model, datafile, testsetfile, makenew):



    if makenew == 1:

        # get test data set from training data to obtain predictions
        infile = open(datafile, 'rb')
        data = pickle.load(infile)
        infile.close()

        infile = open(testsetfile, 'rb')
        testdata = pickle.load(infile)
        infile.close()

        scaler_y = data[-1][4]

        Y_pred = model.predict(testdata[2])

        Y_pred = MinMaxScaler.inverse_transform(scaler_y, Y_pred)

        #plot_test_corr(data[-1], model)

        with open("testset_predictions", 'wb') as handle:
            pickle.dump(Y_pred, handle)

    else:

        infile = open("testset_predictions", 'rb')
        Y_pred = pickle.load(infile)
        infile.close()



    return Y_pred


def make_testset_chemfile(testsetfile, makenew, out):


    infile = open(testsetfile, 'rb')
    testset = pickle.load(infile)
    infile.close()

    if makenew == 1:
        with open("testset.csv", "w+") as csv_file:  # create file and write header
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(["smiles", out])

        for num in range(len(testset[0])):

            with open("testset.csv", "a") as csv_file:  # add row to file
                writer = csv.writer(csv_file, delimiter=",")
                writer.writerow([testset[0][num][0], testset[1][num][0]])


        chem_stat_analysis.perform_full_analysis_smiles("testset.csv", "testset", [out])


    #find_groups_from_smiles([i[0] for i in testset[0]])


    return [[i[0] for i in testset[0]], [j[0] for j in testset[1]]]


def plot_test_corr(data, model):

    Y_pred = model.predict(data[1])
    Y_pred = MinMaxScaler.inverse_transform(data[4], Y_pred)

    Y_val = MinMaxScaler.inverse_transform(data[4], data[3])

    plot_results.prediction_scatter(Y_val, Y_pred, "test", out)

    return




#USING RAMAK - DATA!
def check_NN(modelpath, cmprfile, change_file, chemfile):

    #load one hot encoder
    infile = open(cmprfile, 'rb')
    cmp_input = pickle.load(infile)
    infile.close()

    model = keras.models.load_model(modelpath) #load trained model

    [cmpr_data, scaler_y] = cmp_input

    Y_pred = model.predict(cmpr_data)

    Y_pred = MinMaxScaler.inverse_transform(scaler_y, Y_pred)

    [header, pairs] = get_numbers(change_file)

    chem = pd.read_csv(chemfile)

    plot_changes(Y_pred, header, pairs, chem)

    return


def get_numbers(change_file):
    "Extracts index pairs for comparable molecules from file specified."

    df = pd.read_csv(change_file)

    desc = [] #description
    pairs = [] #index pairs
    for col in df.columns: #extract and transform data...
        desc_pairs = []
        for pair in df[col]:
            try:
                cur = []
                stringpair = pair.split(" – ")
                for stri in stringpair:
                    cur.append(int(stri))
                desc_pairs.append(cur)
            except:
                pass
        pairs.append(desc_pairs)
        desc.append(col)
    return [desc, pairs]

def plot_changes(preds, header, pairs, chem):


    rgroups = get_groups_from_pairs(pairs[header.index('pos (r)')])

    C_Ntracks = get_tracks(pairs[header.index('C → N (r)')])

    NH_Otracks = get_tracks(pairs[header.index('NH → O (r)')])

    C_tracks = get_tracks(pairs[header.index('+ C (g)')])




    title = "Groups of equal sum formula"
    gplot = "conj_C"
    gcol = "n_O"
    gax = "keto_dist"

    xysg = prep_trackgroup_plot(rgroups, preds, chem, gplot, gcol, gax)

    make_trackgroup_plot(xysg, gplot, gcol, gax, title)




    title = "C->N replacement in equal structure"
    tplot = "conj_C"
    tcol = "keto_dist"
    tax = "n_N"

    xyst = prep_trackgroup_plot(C_Ntracks, preds, chem, tplot, tcol, tax)

    make_trackgroup_plot(xyst, tplot, tcol, tax, title)




    title = "NH->O replacement in equal structure"
    tplot = "conj_C"
    tcol = "keto_dist"
    tax = "n_O"

    xyst = prep_trackgroup_plot(NH_Otracks, preds, chem, tplot, tcol, tax)

    make_trackgroup_plot(xyst, tplot, tcol, tax, title)




    title = "Appending of C-groups"
    tplot = "conj_C"
    tcol = "keto_dist"
    tax = "n_C"

    xyst = prep_trackgroup_plot(C_tracks, preds, chem, tplot, tcol, tax)

    make_trackgroup_plot(xyst, tplot, tcol, tax, title)

    return



def legend_without_duplicate_labels(ax):
    handles, labels = ax.get_legend_handles_labels()
    unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
    ax.legend(*zip(*unique))


def get_color(num):
    cols = [(0, 0, 1), (0, 1, 0), (1, 0, 0), (1, 1, 0), (1, 0, 1), (0, 1, 1), (1, 0.5, 0.5)]
    if num > 6:
        return cols[num%7]
    else:
        return cols[num]

def get_color_var(num):
    global colorvar
    cols = [(0, 0, 1), (0, 1, 0), (1, 0, 0), (1, 1, 0), (1, 0, 1), (0, 1, 1), (1, 0.5, 0.5)]


    r1 = 0
    r2 = 0
    r3 = 0

    if colorvar % 6 == 0:
        r1 = 50*(colorvar%20)
    elif colorvar % 6 == 1:
        r2 = 50*(colorvar%20)
    elif colorvar % 6 == 2:
        r3 = 50*(colorvar%20)
    elif colorvar % 6 == 3:
        r1 = 50 * (colorvar%20)
        r2 = 50 * (colorvar%20)
    elif colorvar % 6 == 4:
        r1 = 50 * (colorvar%20)
        r3 = 50 * (colorvar%20)
    elif colorvar % 6 == 5:
        r2 = 50 * (colorvar%20)
        r3 = 50 * (colorvar%20)


    r1 = r1 / 2000
    r2 = r2 / 2000
    r3 = r3 / 2000
    rs = [r1, r2, r3]

    newcol = []
    for n, rgb in enumerate(cols[num%7]):
        if rgb == 1:
            newcol.append(rgb - rs[n])
        else:
            newcol.append(rgb + rs[n])

    return tuple(newcol)



def get_groups_from_pairs(pairs):

    groups = [pairs[0]]
    for pair in pairs[1:]:
        done = 0
        for group in groups:
            if (pair[0] in group) and (pair[1] not in group):
                group.append(pair[1])
                done = 1
            if (pair[1] in group) and (pair[0] not in group):
                group.append(pair[0])
                done = 1
            if (pair[0] in group) and (pair[1] in group):
                done = 1
        if done == 0:
            groups.append(pair)
    return groups


def get_tracks(pairs):
    groups = [pairs[0]]
    for pair in pairs[1:]:
        done = 0
        for group in groups:
            if pair[0] == group[-1]:
                group.append(pair[1])
                done = 1

        if done == 0:
            groups.append(pair)
    return groups