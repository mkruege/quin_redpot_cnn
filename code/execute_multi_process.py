import multipr_model
from sys import argv
import argparse


model_dir = './'
if argv[0].find('/') >= 0:
	model_dir = argv[0][: - argv[0][::-1].find('/')]


#parse arguments
parser = argparse.ArgumentParser(description='Running CNN HPT following specified hpt-csv sheet.')
parser.add_argument('--smiles_data', '-d', type=str, default='all_new.csv', help='Path to bed file with SMILES and chem. property data,')
parser.add_argument('--hpt_sheet', '-hp', type=str, default='hpt_sheet.csv', help='Path to bed file with hpt information')
parser.add_argument('--outval', '-ov', type=str, default='E', help='List of output value(s)')
parser.add_argument('--k_rands', '-kr', type=int, default=5, help='k_randomSMILES for augmentation method')
parser.add_argument('--n_rands', '-nr', type=int, default=0, help='n_randomSMILES for augmentation method')
parser.add_argument('--k_shifts', '-ks', type=int, default=5, help='k_shiftSMILES for augmentation method')
parser.add_argument('--n_shifts', '-ns', type=int, default=0, help='n_shiftSMILES for augmentation method')
parser.add_argument('--extend_frame', '-ef', type=int, default=10, help='extend_frame for augmentation method')
parser.add_argument('--cross_val', '-cv', type=int, default=0, help='Define k for k-fold cross validation.')


# parse and pre-process command line arguments
args = parser.parse_args()


args.smiles_data = args.smiles_data.split(',')
args.outval = args.outval.split(',')

print("Arguments parsed")

multipr_model.exec_multipr_hpt(args.smiles_data, args.outval, args.hpt_sheet, args.k_rands, args.n_rands, args.k_shifts, args.n_shifts, args.extend_frame, args.cross_val)