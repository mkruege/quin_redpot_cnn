# quin_redpot_cnn

Materials for the work "Convolutional neural network prediction of quinone reduction potentials". Contains scripts for pre-processing, model training and analysis, and data.

Uploaded data origiates from:

Tabor, D.P., Gomez-Bombarelli, R., Tong, L., Gordon, R.G., Aziz, M.J., Aspuru-Guzik, A.: Mapping the frontiers of quinone stability in aqueous media: Implications for organic aqueous redox flow batteries. J. Mater. Chem. A 7(20), 12833{12841 (2019). doi:10.1039/c9ta03219c

Kristensen, S.B., van Mourik, T., Pedersen, T.B., S�rensen, J.L., Muff, J.: Simulation of electrochemicalproperties of naturally occurring quinones. Sci. Rep.10(1) (2020). doi:10.1038/s41598-020-70522-z